$(document).ready(function () {
	$("#loadMoreData").click(function () {
		console.log("Requesting more data...");
		$.ajax({
			url: Routing.generate('system_more', {id: $(this).attr('systemId')}),
		type: 'POST',
			dataType: 'json',
			data: {
				"lastDate": $(this).attr('lastData')
			},
		async: true,
			success: function (data) {
			$("#moreData").append(data);
		},
		fail: function () {
			console.log('FAILED');
		}

		});
		return false;
	});
});

function loadMoreData() {
	console.log("Requesting more data...");
	var systemId = $("#systemId").text();
	var lastDate = $("#lastDate").text();
	console.log(systemId);
	console.log(lastDate);

	$("#newData").load(Routing.generate('system_more', {id: systemId}), {
		"lastDate": lastDate
	}, function (x) {
		$(".rawdata").append(x);
		$("#lastDate").text( $(".rawdata tr:last td:first").text() );
	});



	// var newData = $("#newData");
	// console.log(data);
	// $(".rawdata").append(newData);


	// $.ajax({
	// 	url: Routing.generate('system_more', {id: $(this).attr('systemId')}),
	// 	type: 'POST',
	// 	dataType: 'json',
	// 	data: {
	// 		"lastDate": $(this).attr('lastData')
	// 	},
	// 	async: true,
	// 	success: function (data) {
	// 		$("#moreData").append(data);
	// 	},
	// 	fail: function () {
	// 		console.log('FAILED');
	// 	}
	//
	// });
	// return false;
}