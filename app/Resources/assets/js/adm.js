/**
 * Created by Justin on 12/12/2016.
 */

$(document).ready(function () {
	try {
		$("#usersEnabled")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});

		$("#usersNotEnabled")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});

		$("#usersNotRegistered")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});
	}
	catch (e) {
		// console.error(e);
	}

	try {
		$("#customersEnabled")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});

		$("#customersNotEnabled")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});
	}
	catch (e) {
		// console.error(e);
	}

	try {
		$("#systemsEnabledList")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});
		$("#systemsNotEnabledList")
			.tablesorter({
				theme: 'bootstrap',
				widthFixed: true,
				widgets: ['zebra', 'filter', 'pager']
			})
			.appendTablesorterPagerControls({
				sizes: [10, 50, 100],
				initialSize: 10
			});
	}
	catch (e) {
		// console.error(e);
	}

	$('.clickable-row').click(function () {
		window.location = $(this).data("href")
	});

	$('.number-input').on('change', function(){
		$(this).val(parseFloat($(this).val()).toFixed(1));
	});

	$("#systems_new_edit_form_systemId").click(function () {
		console.log("VAL: " + $(".js-regesteredFind").val());
	});


	$('#irdButton').click(function () {
		var lastResponseLength = false;

		$('#ird-stream-started').show();
		$('#ird-stream-output').empty();
		$('#ird-stream-output').show();

		setTimeout(startStreamingIRD(lastResponseLength), 1500);
	});

	$('#irdButtonClose').click(function () {
		$('#ird-stream-started').hide();
		$('#ird-stream-output').empty();
		$('#ird-stream-output').hide();
		$('#ird-stream-ended').hide();
		$(this).hide();
	});

	function startStreamingIRD(lastResponseLength) {
		xhr = new XMLHttpRequest();
		xhr.open('GET', Routing.generate('getRemoteData'), true);
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

		xhr.onprogress = function(e) {
			var response = e.currentTarget.response;
			var output = lastResponseLength === false
				? response
				: response.substring(lastResponseLength);

			lastResponseLength = response.length;

			$('#ird-stream-output').append('<p>'+output+'</p>');
		};

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				// $('#stream-output').append('<p>Completed!</p>');
				$('#ird-stream-ended').show();
				$('#irdButtonClose').show();
			}
		};

		xhr.send();
	}


	$('#irsButton').click(function () {
		var lastResponseLength = false;

		$('#irs-stream-started').show();
		$('#irs-stream-output').empty();
		$('#irs-stream-output').show();

		setTimeout(startStreamingIRS(lastResponseLength), 1500);
	});

	$('#irsButtonClose').click(function () {
		$('#irs-stream-started').hide();
		$('#irs-stream-output').empty();
		$('#irs-stream-output').hide();
		$('#irs-stream-ended').hide();
		$(this).hide();
	});

	function startStreamingIRS(lastResponseLength) {
		xhr = new XMLHttpRequest();
		xhr.open('GET', Routing.generate('getRemoteSystems'), true);
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

		xhr.onprogress = function(e) {
			var response = e.currentTarget.response;
			var output = lastResponseLength === false
				? response
				: response.substring(lastResponseLength);

			lastResponseLength = response.length;

			$('#irs-stream-output').append('<p>'+output+'</p>');
		};

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				$('#irs-stream-ended').show();
				$('#irsButtonClose').show();
			}
		};

		xhr.send();
	}

});

