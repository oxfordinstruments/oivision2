<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/18/2017
 * Time: 11:39 AM
 */

namespace AppBundle\Api;



use AppBundle\Entity\ApiKeyEntity;
use AppBundle\Entity\CustomerEntity;
use AppBundle\Entity\HelloEntity;
use AppBundle\Entity\OivErrorsEntity;
use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Moment\Moment;

class ApiController extends Controller
{

	/**
	 * Returns the current utc date and time.
	 * @Route("/api/datetime", name="api_datetime")
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Moment\MomentException
	 */
	public function getDatetime(Request $request)
	{
		$this->get('monolog.logger.oivision')
			->info('Datetime Request: IP='.$request->getClientIp().' HOST='.$request->getHost().' CONTENT='.$request->getContent(),
				['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__]);

		$odata = array(
			'code' => 200,
			'request' => null
		);
		if(!$this->checkRequest($request,  'get')){
			$odata['code'] = 405;
			$odata['error'] = 'Method not allowed';
			$this->get('monolog.logger.oivision')
				->info('Datetime Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__]);
			return new JsonResponse($odata, $odata['code']);
		}

		$datetime = new Moment('now', 'UTC');
		return new JsonResponse($datetime);
	}

	/**
	 * @Route("/api/hello", name="api_hello")
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Moment\MomentException
	 */
	public function getHello(Request $request)
	{
		$utils = $this->container->get('app.utils_common');

		$this->get('monolog.logger.oivision')
			->info('Hello Request: IP='.$request->getClientIp().' HOST='.$request->getHost().' CONTENT='.$request->getContent(),
				['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
		$odata = array(
			'code' => 202,
			'error' => false,
			'datetime' => null,
			'name' => 'NOT REGISTERED',
			'sysid' => 0,
			'enabled' => false,
			'settings' => [],
		);
		if(!$this->checkRequest($request,  'post', 'json')){
			$odata['code'] = 405;
			$odata['error'] = 'Method not allowed';
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		$idata = json_decode($request->getContent(), true);

		if(is_null($idata)){
			$odata['code'] = 400;
			$odata['error'] = json_last_error_msg();
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		if(!isset($idata['macoiv'])
			or !preg_match('/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/',$idata['macoiv'])
			or $idata['macoiv'] == '00-00-00-00-00-01')
		{
			$odata['code'] = 405;
			$odata['error'] = 'Missing or invalid oiv mac address (macoiv)';
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}
		if(!isset($idata['macrdc'])
			or !preg_match('/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/',$idata['macrdc'])
			or $idata['macrdc'] == '00-00-00-00-00-01')
		{
			$odata['code'] = 405;
			$odata['error'] = 'Missing or invalid rdc mac address (macrdc)';
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		$auth = $this->checkAuth($request, $idata['macrdc']);
		if($auth === false){
			$odata['code'] = 401;
			$odata['error'] = 'Unauthorized';
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}else if($auth !== true){
			$odata['new_token'] = $auth;
		}

		if(!isset($idata['ver'])){
			$odata['code'] = 405;
			$odata['error'] = 'Missing version (ver)';
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		if(!isset($idata['rev'])){
			$odata['code'] = 405;
			$odata['error'] = 'Missing revision (rev)';
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		$datetime = new Moment('now', 'UTC');
		$odata['datetime'] = $datetime->format();

		$em = $this->getDoctrine()->getManager();
		$sysRepo = $em->getRepository(SystemsEntity::class);
		$val = $sysRepo->findByMacRdc($idata['macrdc']);
		if($val){
			/**
			 * @var SystemsEntity[] $val
			 */
			$val = $val[0];
			$odata['name'] = $utils->cleanString($val->getSystemName());
			$odata['enabled'] = $val->getEnabled();
			$system_id_num = $val->getId();
			$odata['sysid'] = $val->getSystemId();

			$this->get('monolog.logger.oivision')
				->info('Hello Response: IP='.$request->getClientIp().' MACRDC='.$idata['macrdc'].'MACOIV='.$idata['macoiv'].' SYSTEM='.$odata['name'].' SYSID='.$odata['sysid'].' ENABLED='.var_export($odata['enabled'],true),
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);

		}else{
			$this->get('monolog.logger.oivision')
				->info('Hello Response: IP='.$request->getClientIp().' MACRDC='.$idata['macrdc'].'MACOIV='.$idata['macoiv'].' SYSTEM='.$odata['name'].' SYSID='.$odata['sysid'].' ENABLED='.var_export($odata['enabled'],true),
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);

			$lastSystem = $sysRepo->getMaxSystemId();
			/**
			 * @var SystemsEntity $lastSystem
			 */
			if(count($lastSystem) == 0){
				$lastSystem = new SystemsEntity();
				$lastSystem->setSystemId($this->getParameter('unreg_system_id'));
			}else{
				$lastSystem = $lastSystem[0];
			}

			$repoCust = $this->getDoctrine()->getRepository(CustomerEntity::class);
			$cust = $repoCust->findOneBy(['name' => 'OiHealthcare']);

			$nextSystem = $this->getNextTempSystemId($lastSystem->getSystemId());

			$tempSystem = new SystemsEntity();
			$tempSystem->setMacrdc($idata['macrdc']);
			$tempSystem->setSystemId('MR'.$nextSystem);
			$tempSystem->setSystemName('NOT REGISTERED '.$nextSystem);
			$odata['name'] = $tempSystem->getSystemName();
			$tempSystem->setEnabled(false);
			$tempSystem->setUseGlobalAlarms(true);
			$tempSystem->setNotes('This system was added automatically when the box said hello for the first time');
			$tempSystem = $utils->updateToCurrentGlobalAlarms($tempSystem);
			$tempSystem->setOffsetHe($this->getParameter('offsetHe'));
			$tempSystem->setOffsetVp($this->getParameter('offsetVp'));
			$tempSystem->setOffsetTc($this->getParameter('offsetTc'));
			$tempSystem->setOffsetRh($this->getParameter('offsetRh'));
			$tempSystem->setOffsetWf($this->getParameter('offsetWf'));
			$tempSystem->setOffsetWtf($this->getParameter('offsetWtf'));
			$tempSystem->setCustomerId($cust);
			$em->persist($tempSystem);
			$em->flush();
			$system_id_num = $tempSystem->getId();

			$this->get('monolog.logger.oivision')
				->info('New system created: SYSTEM ID='.$odata['sysid'].' MACRDC='.$idata['macrdc'].'MACOIV='.$idata['macoiv'].' SYSTEM='.$odata['name'].' ENABLED='.var_export($odata['enabled'],true),
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
		}

		if(isset($idata['errors'])){
			foreach ($idata['errors'] as $key=>$error){
				$oivError = new OivErrorsEntity();
				$oivError->setMacOiv($idata['macoiv']);
				$oivError->setMacRdc($idata['macrdc']);
				$oivError->setErrorTimestamp(new \DateTime($key));
				$oivError->setTimestamp(new \DateTime("now"));
				$oivError->setError($error);
				$em->persist($oivError);
				$em->flush();
			}
		}

		$hello = new HelloEntity();
		$hello->setMacOiv($idata['macoiv']);
		$hello->setMacRdc($idata['macrdc']);
		$hello->setVersion($idata['ver']);
		$hello->setRevision($idata['rev']);
		$hello->setSystem($system_id_num);
		$hello->setTimestamp(new \DateTime("now"));
		$em->persist($hello);
		$em->flush();

		$val = $sysRepo->findByMacRdc($idata['macrdc']);
		if($val) {

			$val = $val[0];
			/**
			 * @var SystemsEntity $val
			 */
			$odata['settings'] = [
				'in_storage' => $val->getInStorage(),

				'monitor_he' => $val->getMonitorHe(),
				'monitor_vp' => $val->getMonitorVp(),
				'monitor_tf' => $val->getMonitorTf(),
				'monitor_rh' => $val->getMonitorRh(),
				'monitor_dp' => $val->getMonitorDp(),
				'monitor_dpd' => $val->getMonitorDpDiff(),
				'monitor_wf' => $val->getMonitorWf(),
				'monitor_wt' => $val->getMonitorWt(),
				'monitor_q' => $val->getMonitorQ(),
				'monitor_cmp' => $val->getMonitorCmp(),

				'avg_he' => $val->getAvgHe(),
				'avg_vp' => $val->getAvgVp(),
				'avg_cnt' => $this->getParameter('avgHeVpCount'),

				'alarm_he_l' => $val->getAlarmHeL(),
				'alarm_vp_h' => $val->getAlarmVpH(),
				'alarm_vp_l' => $val->getAlarmVpL(),
				'alarm_tf_h' => $val->getAlarmTfH(),
				'alarm_tf_l' => $val->getAlarmTfL(),
				'alarm_rh_h' => $val->getAlarmRhH(),
				'alarm_rh_l' => $val->getAlarmRhL(),
				'alarm_dp_h' => $val->getAlarmDpH(),
				'alarm_dp_diff' => $val->getAlarmDpDiff(),
				'alarm_wf_h' => $val->getAlarmWfH(),
				'alarm_wf_l' => $val->getAlarmWfL(),
				'alarm_wt_h' => $val->getAlarmWtH(),
				'alarm_wt_l' => $val->getAlarmWtL(),
				'alarm_storage_he_l' => $val->getAlarmStorageHeL(),
				'alarm_storage_vp_h' => $val->getAlarmStorageVpH(),
				'alarm_storage_vp_l' => $val->getAlarmStorageVpL(),

				'critical_he_l' => $val->getCriticalHeL(),
				'critical_vp_h' => $val->getCriticalVpH(),
				'critical_vp_l' => $val->getCriticalVpL(),
				'critical_tf_h' => $val->getCriticalTfH(),
				'critical_tf_l' => $val->getCriticalTfL(),
				'critical_rh_h' => $val->getCriticalRhH(),
				'critical_rh_l' => $val->getCriticalRhL(),
				'critical_dp_h' => $val->getCriticalDpH(),
				'critical_dp_diff' => $val->getCriticalDpDiff(),
				'critical_wf_h' => $val->getCriticalWfH(),
				'critical_wf_l' => $val->getCriticalWfL(),
				'critical_wt_h' => $val->getCriticalWtH(),
				'critical_wt_l' => $val->getCriticalWtL(),
				'critical_storage_he_l' => $val->getCriticalStorageHeL(),
				'critical_storage_vp_h' => $val->getCriticalStorageVpH(),
				'critical_storage_vp_l' => $val->getCriticalStorageVpL(),

				'offset_he' => $val->getOffsetHe(),
				'offset_vp' => $val->getOffsetVp(),
				'offset_wf' => $val->getOffsetWf(),
				'offset_wtf' => $val->getOffsetWtf(),
				'offset_tc' => $val->getOffsetTc(),
				'offset_rh' => $val->getOffsetRh(),

				'updated' => $val->getAlarmSettingsUpdated(),
			];
		}else{
			$odata['code'] = 405;
			$odata['error'] = 'Hello settings invalid for system';
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
		}

		return new JsonResponse($odata, $odata['code']);
	}

	/**
	 * @Route("/api/errors/oiv", name="api_errors_oiv")
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function setErrorsOiv(Request $request){

//		$utils = $this->container->get('app.utils_common');

		$this->get('monolog.logger.oivision')
			->info('OIV Errors Request: IP='.$request->getClientIp().' HOST='.$request->getHost().' CONTENT='.$request->getContent(),
				['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
		$odata = array(
			'code' => 202,
			'error' => false,
		);
		if(!$this->checkRequest($request,  'post', 'json')){
			$odata['code'] = 405;
			$odata['error'] = 'Method not allowed';
			$this->get('monolog.logger.oivision')
				->info('OIV Errors Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}


		$idata = json_decode($request->getContent(), true);

		if(is_null($idata)){
			$odata['code'] = 400;
			$odata['error'] = json_last_error_msg();
			$this->get('monolog.logger.oivision')
				->info('OIV Errors Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		if(!isset($idata['macoiv'])
			or !preg_match('/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/',$idata['macoiv'])
			or $idata['macoiv'] == '00-00-00-00-00-01')
		{
			$odata['code'] = 405;
			$odata['error'] = 'Missing or invalid oiv mac address (macoiv)';
			$this->get('monolog.logger.oivision')
				->info('OIV Errors Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		if(!isset($idata['macrdc'])
			or !preg_match('/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/',$idata['macrdc'])
			or $idata['macrdc'] == '00-00-00-00-00-01')
		{
			$odata['code'] = 405;
			$odata['error'] = 'Missing or invalid rdc mac address (macrdc)';
			$this->get('monolog.logger.oivision')
				->info('Hello Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		$auth = $this->checkAuth($request, $idata['macrdc']);
		if($auth === false){
			$odata['code'] = 401;
			$odata['error'] = 'Unauthorized';
			$this->get('monolog.logger.oivision')
				->info('OIV Errors Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}else if($auth !== true){
			$odata['new_token'] = $auth;
		}

		if(!isset($idata['errors'])){
			$odata['code'] = 405;
			$odata['error'] = 'Missing errors (errors)';
			$this->get('monolog.logger.oivision')
				->info('OIV Errors Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		$em = $this->getDoctrine()->getManager();
		foreach ($idata['errors'] as $key=>$error){
			$oivError = new OivErrorsEntity();
			$oivError->setMacOiv($idata['macoiv']);
			$oivError->setMacRdc($idata['macrdc']);
			$oivError->setErrorTimestamp(new \DateTime($key));
			$oivError->setTimestamp(new \DateTime("now"));
			$oivError->setError($error);
			$em->persist($oivError);
			$em->flush();
		}

		return new JsonResponse($odata, $odata['code']);
	}

	/**
	 * @Route("/api/data/ge", name="api_data_ge")
	 * @param Request $request
	 * @return JsonResponse
	 * @throws \Moment\MomentException
	 */
	public function setDataGe(Request $request)
	{
		$utils = $this->container->get('app.utils_common');

		$this->get('monolog.logger.oivision')
			->info('GE Data Request: IP='.$request->getClientIp().' HOST='.$request->getHost().' CONTENT='.$request->getContent(),
				['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
		$odata = array(
			'code' => 202,
			'error' => false,
			'datetime' => null,
			'name' => 'NOT REGISTERED',
			'sysid' => 0,
			'enabled' => false,
			'alarm' => false,
			'critical' => false,
			'settings' => [],
		);
		if(!$this->checkRequest($request,  'post', 'json')){
			$odata['code'] = 405;
			$odata['error'] = 'Method not allowed';
			$this->get('monolog.logger.oivision')
				->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		$idata = json_decode($request->getContent(), true);

		if(is_null($idata)){
			$odata['code'] = 400;
			$odata['error'] = json_last_error_msg();
			$this->get('monolog.logger.oivision')
				->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		if(!isset($idata['macrdc'])
			or !preg_match('/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/',$idata['macrdc'])
			or $idata['macrdc'] == '00-00-00-00-00-01')
		{
			$odata['code'] = 405;
			$odata['error'] = 'Missing or invalid rdc mac address (macrdc)';
			$this->get('monolog.logger.oivision')
				->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		if(!isset($idata['macoiv'])
			or !preg_match('/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/',$idata['macoiv'])
			or $idata['macoiv'] == '00-00-00-00-00-01')
		{
			$odata['code'] = 405;
			$odata['error'] = 'Missing or invalid oiv mac address (macoiv)';
			$this->get('monolog.logger.oivision')
				->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		$auth = $this->checkAuth($request, $idata['macrdc']);
		if($auth === false){
			$odata['code'] = 401;
			$odata['error'] = 'Unauthorized';
			$this->get('monolog.logger.oivision')
				->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}else if($auth !== true){
			$odata['new_token'] = $auth;
		}

		if(!isset($idata['mfg']) or strtolower($idata['mfg']) != 'ge'){
			$odata['code'] = 405;
			$odata['error'] = 'Missing or invalid manufacturer (mfg)';
			$this->get('monolog.logger.oivision')
				->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		if(!isset($idata['data']) or !is_array($idata['data'])){
			$odata['code'] = 405;
			$odata['error'] = 'Missing or invalid data structure (data)';
			$this->get('monolog.logger.oivision')
				->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return new JsonResponse($odata, $odata['code']);
		}

		foreach ($idata['data'] as $dkey=>$data){
			foreach (['tf','rh','he','vp','wf','wt','q','cmp','timestamp'] as $system){
				if(!isset($idata['data'][$dkey][$system])){
					$odata['code'] = 405;
					$odata['error'] = 'Missing or invalid data (data['.$system.'])';
					$this->get('monolog.logger.oivision')
						->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
							['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
					return new JsonResponse($odata, $odata['code']);
				}
			}
		}


		$datetime = new Moment('now', 'UTC');
		$odata['datetime'] = $datetime->format();

		$em = $this->getDoctrine()->getManager();
		$sysRepo = $em->getRepository(SystemsEntity::class);
		/**
		 * @var SystemsEntity $system
		 */
		$system = $sysRepo->findByMacRdc($idata['macrdc']);

		if($system){
			$system = $system[0];
			$odata['name'] = $utils->cleanString($system->getSystemName());
			$odata['enabled'] = $system->getEnabled();
//			$system_id_num = $system->getId();
			$odata['sysid'] = $system->getSystemId();

			if(!$odata['enabled']){
				$odata['code'] = 403;
				$odata['error'] = 'The system is not enabled to send data';
				$this->get('monolog.logger.oivision')
					->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'].' SYSTEM ID='.$odata['sysid'],
						['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
				return new JsonResponse($odata, $odata['code']);
			}

			$apiKeyRepo = $this->getDoctrine()->getRepository(ApiKeyEntity::class);
			$apiKey = $apiKeyRepo->findKey($request->headers->get('api-key'));

			foreach ($idata['data'] as $dkey=>$data) {
				$sysData = new SystemsDataGeEntity();
				$sysData->setTimestamp(new \DateTime($idata['data'][$dkey]['timestamp']));
				$sysData->setMacRdc($idata['macrdc']);
				$sysData->setMacOiv($idata['macoiv']);
				$sysData->setSystem($system);
				$sysData->setHe($idata['data'][$dkey]['he']);
				$sysData->setVp($idata['data'][$dkey]['vp']);
				$sysData->setTf($idata['data'][$dkey]['tf']);
				$sysData->setRh($idata['data'][$dkey]['rh']);
				$sysData->setWf($idata['data'][$dkey]['wf']);
				$sysData->setWt($idata['data'][$dkey]['wt']);
				$sysData->setQ($idata['data'][$dkey]['q']);
				$sysData->setCmp($idata['data'][$dkey]['cmp']);
				$sysData->setInStorage($system->getInStorage());
				$sysData->setDp($utils->dewPoint($sysData->getTf(), $sysData->getRh()));
				$sysData->setDpdiff($sysData->getTf() - $sysData->getDp());

				$sysData = $utils->checkDataGeForAlarms($sysData, $system);

				$sysData->setApiKey($apiKey);

				if ($sysData->getHasAlarm()) {
					$sysData->setAlarmProcessed(false);
				}

				if ($sysData->getHasCriticalAlarm()) {
					$sysData->setCriticalProcessed(false);
				}

				$em->persist($sysData);
				$em->flush();
			}
			$this->get('monolog.logger.oivision')
				->info('GE Data Response: IP=' . $request->getClientIp() . ' MACRDC=' . $idata['macrdc'] . ' SYSTEM=' . $odata['name'] . ' SYSID=' . $odata['sysid'] . ' ENABLED=' . var_export($odata['enabled'], true),
					['key' => 'api', 'function' => __CLASS__ . '::' . __FUNCTION__.'::'.__LINE__]);

		}else{
			$this->get('monolog.logger.oivision')
				->info('GE Data Response: IP='.$request->getClientIp().' MACRDC='.$idata['macrdc'].' SYSTEM='.$odata['name'].' SYSID='.$odata['sysid'].' ENABLED='.var_export($odata['enabled'],true),
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);

			$odata['error'] = 'Could not match the system so data was rejected';
		}

		$val = $sysRepo->findByMacRdc($idata['macrdc']);
		if($val) {

			$val = $val[0];
			/**
			 * @var SystemsEntity $val
			 */
			$odata['settings'] = [
				'in_storage' => $val->getInStorage(),

				'monitor_he' => $val->getMonitorHe(),
				'monitor_vp' => $val->getMonitorVp(),
				'monitor_tf' => $val->getMonitorTf(),
				'monitor_rh' => $val->getMonitorRh(),
				'monitor_dp' => $val->getMonitorDp(),
				'monitor_dpd' => $val->getMonitorDpDiff(),
				'monitor_wf' => $val->getMonitorWf(),
				'monitor_wt' => $val->getMonitorWt(),
				'monitor_q' => $val->getMonitorQ(),
				'monitor_cmp' => $val->getMonitorCmp(),

				'avg_he' => $val->getAvgHe(),
				'avg_vp' => $val->getAvgVp(),
				'avg_cnt' => $this->getParameter('avgHeVpCount'),

				'alarm_he_l' => $val->getAlarmHeL(),
				'alarm_vp_h' => $val->getAlarmVpH(),
				'alarm_vp_l' => $val->getAlarmVpL(),
				'alarm_tf_h' => $val->getAlarmTfH(),
				'alarm_tf_l' => $val->getAlarmTfL(),
				'alarm_rh_h' => $val->getAlarmRhH(),
				'alarm_rh_l' => $val->getAlarmRhL(),
				'alarm_dp_h' => $val->getAlarmDpH(),
				'alarm_dp_diff' => $val->getAlarmDpDiff(),
				'alarm_wf_h' => $val->getAlarmWfH(),
				'alarm_wf_l' => $val->getAlarmWfL(),
				'alarm_wt_h' => $val->getAlarmWtH(),
				'alarm_wt_l' => $val->getAlarmWtL(),
				'alarm_storage_he_l' => $val->getAlarmStorageHeL(),
				'alarm_storage_vp_h' => $val->getAlarmStorageVpH(),
				'alarm_storage_vp_l' => $val->getAlarmStorageVpL(),

				'critical_he_l' => $val->getCriticalHeL(),
				'critical_vp_h' => $val->getCriticalVpH(),
				'critical_vp_l' => $val->getCriticalVpL(),
				'critical_tf_h' => $val->getCriticalTfH(),
				'critical_tf_l' => $val->getCriticalTfL(),
				'critical_rh_h' => $val->getCriticalRhH(),
				'critical_rh_l' => $val->getCriticalRhL(),
				'critical_dp_h' => $val->getCriticalDpH(),
				'critical_dp_diff' => $val->getCriticalDpDiff(),
				'critical_wf_h' => $val->getCriticalWfH(),
				'critical_wf_l' => $val->getCriticalWfL(),
				'critical_wt_h' => $val->getCriticalWtH(),
				'critical_wt_l' => $val->getCriticalWtL(),
				'critical_storage_he_l' => $val->getCriticalStorageHeL(),
				'critical_storage_vp_h' => $val->getCriticalStorageVpH(),
				'critical_storage_vp_l' => $val->getCriticalStorageVpL(),

				'offset_he' => $val->getOffsetHe(),
				'offset_vp' => $val->getOffsetVp(),
				'offset_wf' => $val->getOffsetWf(),
				'offset_wtf' => $val->getOffsetWtf(),
				'offset_tc' => $val->getOffsetTc(),
				'offset_rh' => $val->getOffsetRh(),

				'updated' => $val->getAlarmSettingsUpdated(),
			];
		}else{
			$odata['code'] = 405;
			$odata['error'] = 'Settings invalid for system';
			$this->get('monolog.logger.oivision')
				->info('GE Data Error: IP='.$request->getClientIp().' ERROR='.$odata['error'],
					['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
		}

		return new JsonResponse($odata, $odata['code']);
	}

	/**
	 * @param Request $request
	 * @param string $method
	 * @param string $contentType
	 * @return bool
	 */
	private function checkRequest(Request $request, $method = 'GET', $contentType = null)
	{
		//Check content type header if null then skip
		if(isset($contentType)){
			if(!is_null($contentType) and $contentType != $request->getContentType()){
				$this->get('monolog.logger.oivision')
					->info('Invalid Content Type',	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
				return false;
			}
		}

		//Check method
		if(!isset($method) or is_null($method)){
			$this->get('monolog.logger.oivision')
				->info('Method is not set',	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			throw new Exception('$method must be set');
		}
		if(strtoupper($method) != $request->getMethod()){
			$this->get('monolog.logger.oivision')
				->info('Invalid Method',	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return false;
		}

		return true;
	}

	/**
	 * @param Request $request
	 * @param string $mac_rdc
	 * @return mixed
	 * @throws \Moment\MomentException
	 */
	private function checkAuth(Request $request, string $mac_rdc)
	{
		if(is_null($request->headers->get('api-key'))){
			$this->get('monolog.logger.oivision')
				->info('API Key is null',	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return false;
		}
		$repo = $this->getDoctrine()->getRepository(ApiKeyEntity::class);
		$keyFind = $repo->findKey($request->headers->get('api-key'));
//		dump($keyFind);
		if(!$keyFind){
			$this->get('monolog.logger.oivision')
				->info('API Key is not valid. No key found.',	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return false;
		}
		/**
		 * @var $keyFind ApiKeyEntity
		 */
		switch ($keyFind->getCustomerId()->getId()){
			case 0:
			case 1:
				$key = false;
				break;
			default:
				$key = $keyFind;
		}
		if($key === false){
			$sysRepo = $this->getDoctrine()->getRepository(SystemsEntity::class);
			/**
			 * @var $sys SystemsEntity
			 */
			$sys = $sysRepo->findOneBy(array('mac_rdc'=>$mac_rdc));
//			dump($sys);
			if(!is_null($sys) and !is_null($sys->getCustomerId())){
				$macFind = $repo->findCustomer($sys->getCustomerId()->getId());
//				dump($macFind);
				if(is_null($macFind)){
					$this->get('monolog.logger.oivision')
						->info('API Key could not be found for RDC MAC: '.$mac_rdc,	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
				}else{
					$key = $macFind;
					if(!$sys->getCustomerId()->isEnabled()){
						$this->get('monolog.logger.oivision')
							->info('API Key is not valid. Customer disabled.',	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
						return false;
					}
				}
			}

		}

		if($key === false){
			$key = $keyFind;
		}

//		dump($key);die();

		$sysRepo = $this->getDoctrine()->getRepository(SystemsEntity::class);
		/**
		 * @var $sys SystemsEntity
		 */
		$sys = $sysRepo->findOneBy(array('mac_rdc'=>$mac_rdc));
		if(!is_null($sys) and  !is_null($sys->getCustomerId()) and !$sys->getCustomerId()->isEnabled()){
			$this->get('monolog.logger.oivision')
				->info('API Key is not valid. Customer disabled.',	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return false;
		}

		$m = new Moment($key->getExpire());
		if($m->isBefore(new Moment('now'))){
			$this->get('monolog.logger.oivision')
				->info('API Key has expired ['.$key->getToken().']',	['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return false;
		}

		if($key != $keyFind){
			return $key->getToken();
		}

		return true;
	}

	/**
	 * @param $lastSystemId
	 * @return bool|string
	 */
	private function getNextTempSystemId($lastSystemId)
	{
		$matches = array();
		preg_match('/(\D*)(.*)/', $lastSystemId, $matches);
		$next = intval($matches[max(array_keys($matches))])+1;
		if(is_null($next)) {
			$this->get('monolog.logger.oivision')
				->error('Unable to generate the next temporary system id.', ['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			return false;
		}
		if($next < 9000) {
			$this->get('monolog.logger.oivision')
				->info('No temporary system id found so will use MR9000', ['key' => 'api', 'function' => __CLASS__.'::'.__FUNCTION__.'::'.__LINE__]);
			$next = 9000;
		}
		return $next;

	}


}
