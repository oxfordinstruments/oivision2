<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 7/5/2017
 * Time: 3:55 PM
 */

namespace AppBundle\Security;


use AppBundle\Entity\LdapGroupsEntity;
use AppBundle\Entity\UserEntity;
use AppBundle\Form\LoginForm;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Nelmio\Alice\support\models\User;
use Proxies\__CG__\AppBundle\Entity\CustomerEntity;
use Psr\Container\ContainerInterface;
use Symfony\Bridge\Doctrine\Security\User\EntityUserProvider;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Ldap\Exception\ConnectionException;
use Symfony\Component\Ldap\Ldap;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormGuarAuthenticator extends AbstractGuardAuthenticator
{
	use TargetPathTrait;

	private $formFactory;
	private $em;
	/**
	 * @var RouterInterface
	 */
	private $router;
	/**
	 * @var UserPasswordEncoder
	 */
	private $passwordEncoder;

	protected $session;

	private $parameterBag;
	/**
	 * @var ContainerInterface
	 */
	private $container;

	public function __construct(FormFactoryInterface $formFactory,
	                            EntityManager $em,
	                            RouterInterface $router,
	                            UserPasswordEncoder $passwordEncoder,
	                            Session $session,
	                            ParameterBagInterface $parameterBag,
	                            ContainerInterface $container
	)
	{
		$this->formFactory = $formFactory;
		$this->em = $em;
		$this->router = $router;
		$this->passwordEncoder = $passwordEncoder;
		$this->session = $session;
		$this->parameterBag = $parameterBag;
		$this->container = $container;
	}


	/**
	 * Get the authentication credentials from the request and return them
	 * as any type (e.g. an associate array). If you return null, authentication
	 * will be skipped.
	 *
	 * Whatever value you return here will be passed to getUser() and checkCredentials()
	 *
	 * For example, for a form login, you might:
	 *
	 *      if ($request->request->has('_username')) {
	 *          return array(
	 *              'username' => $request->request->get('_username'),
	 *              'password' => $request->request->get('_password'),
	 *          );
	 *      } else {
	 *          return;
	 *      }
	 *
	 * Or for an API token that's on a header, you might use:
	 *
	 *      return array('api_key' => $request->headers->get('X-API-TOKEN'));
	 *
	 * @param Request $request
	 *
	 * @return mixed|null
	 */
	public function getCredentials(Request $request)
	{
		$isLoginSubmit = $request->getPathInfo() == '/login' and $request->isMethod('POST');
		if(!$isLoginSubmit){
			return null;
		}


		$form = $this->formFactory->create(LoginForm::class);
		$form->handleRequest($request);
		$data = $form->getData();

		$request->getSession()->set(
			Security::LAST_USERNAME,
			$data['_username']
		);

		return $data;
	}

	/**
	 * Return a UserInterface object based on the credentials.
	 *
	 * The *credentials* are the return value from getCredentials()
	 *
	 * You may throw an AuthenticationException if you wish. If you return
	 * null, then a UsernameNotFoundException is thrown for you.
	 *
	 * @param mixed $credentials
	 * @param UserProviderInterface $userProvider
	 *
	 * @param bool $noLdap
	 * @return null|UserInterface
	 */
	public function getUser($credentials, UserProviderInterface $userProvider, bool $noLdap = false)
	{
		$username = $credentials['_username'];


		$user = $this->em->getRepository(UserEntity::class)
			->findOneBy(['uid' => $username]);

		if($this->parameterBag->get('use_ldap') and is_null($user)) {
			$ldapUser = $this->getUserLdap($username);
			if(!is_bool($ldapUser)) {
				$user = new UserEntity();
				$user->setUid($username);
				$user->setName($ldapUser->getAttribute('cn')[0]);
				$user->setEmail($ldapUser->getAttribute('mail')[0]);
				$user->setEnabled(true);
				$user->setRegistered(true);
				$user->setPasswordChangeNeeded(false);
				$user->setPhone($ldapUser->getAttribute('telephoneNumber')[0]);
				if(is_null($user->getPhone())){
					$user->setPhone($this->parameterBag->get('company_phone'));
				}
				$user->setTimeZone('UTC');
				$user->setComment('gidNumber='.$ldapUser->getAttribute('gidNumber')[0]);
			}
		}

		return $user;
	}

	private function getUserLdap($username) {
		$base_dn = $this->parameterBag->get('ldap_base_dn');
		$admin_dn = $this->parameterBag->get('ldap_admin_dn');
		$admin_password = $this->parameterBag->get('ldap_admin_pw');
		$ldap = Ldap::create('ext_ldap', array(
			'host' => $this->parameterBag->get('ldap_host'),
			'port' => $this->parameterBag->get('ldap_port'),
			'version' => $this->parameterBag->get('ldap_version'),
			'encryption' => $this->parameterBag->get('ldap_encrypt'),
		));
		try {
			$ldap->bind($admin_dn, $admin_password);
		}catch (ConnectionException $ce) {
			$this->container->get('monolog.logger.security')->info('Unable to bind admin Error: '.$ce->getMessage(),
				['key' => 'ldap', 'function' => __CLASS__.'::'.__FUNCTION__]);
			throw new CustomUserMessageAuthenticationException(
				'Call Support!'
			);
		}
		$query = $ldap->query($this->parameterBag->get('ldap_users_ou').','.$base_dn, '(&(objectclass=person)('.$this->parameterBag->get('ldap_users_uid').'='.$username.'))');
		$results = $query->execute();
		if($results->count() == 1){
			return $results->toArray()[0];
		}else{
			return false;
		}
	}


	/**
	 * Returns true if the credentials are valid.
	 *
	 * If any value other than true is returned, authentication will
	 * fail. You may also throw an AuthenticationException if you wish
	 * to cause authentication to fail.
	 *
	 * The *credentials* are the return value from getCredentials()
	 *
	 * @param mixed $credentials
	 * @param UserEntity|UserInterface $user
	 * @return bool
	 */
	public function checkCredentials($credentials, UserInterface $user)
	{

		if(is_null($user)){
			return false;
		}

		$password = $credentials['_password'];
		$username = $credentials['_username'];
		$base_dn = $this->parameterBag->get('ldap_base_dn');

		if(is_numeric($user->getId())){
			//User is in the DB
			if($this->passwordEncoder->isPasswordValid($user, $password)) {
				//User has a valid pw in db
				if (!$user->getEnabled()) {
					throw new CustomUserMessageAuthenticationException(
						'Username is denied access to ' . $this->parameterBag->get('site_name') . '!'
					);
				}
				if (!$user->getRegistered()) {
					throw new CustomUserMessageAuthenticationException(
						'Registration has not been approved yet' . $this->parameterBag->get('site_name') . '!'
					);
				}
				return true;
			}else{
				//Try to ldap bind user
				$ldap = Ldap::create('ext_ldap', array(
					'host' => $this->parameterBag->get('ldap_host'),
					'port' => $this->parameterBag->get('ldap_port'),
					'version' => $this->parameterBag->get('ldap_version'),
					'encryption' => $this->parameterBag->get('ldap_encrypt'),
				));

				$userDn = $this->parameterBag->get('ldap_users_uid').'='.$user->getUsername().','.$this->parameterBag->get('ldap_users_ou').','.$this->parameterBag->get('ldap_base_dn');

				try {
					$ldap->bind($userDn, $password);  //If invalid pw then catch the error

					//Check that the user is in the oivision group
					$query = $ldap->query($this->parameterBag->get('ldap_groups_ou').','.$base_dn, '(&(gidNumber='.$this->parameterBag->get('ldap_gid_num').')('.$this->parameterBag->get('ldap_group_uid').'='.$username.'))');
					$resultsGroup = $query->execute();
					if($resultsGroup->count() == 0){
						throw new CustomUserMessageAuthenticationException(
							'Username is denied access to '.$this->parameterBag->get('site_name').'!'
						);
					}
					return true;

				}catch (ConnectionException $ce) {
					//If we get here the bind on the ldap user failed.
					$this->container->get('monolog.logger.security')->info('User: '.$user->getUsername().' bind failed. Error: '.$ce->getMessage(),
						['key' => 'ldap', 'function' => __CLASS__.'::'.__FUNCTION__]);
					throw new CustomUserMessageAuthenticationException(
						'Invalid Login Credentials!'
					);
				}
			}
		}else{

			//Try to ldap bind user
			$ldap = Ldap::create('ext_ldap', array(
				'host' => $this->parameterBag->get('ldap_host'),
				'port' => $this->parameterBag->get('ldap_port'),
				'version' => $this->parameterBag->get('ldap_version'),
				'encryption' => $this->parameterBag->get('ldap_encrypt'),
			));

			$userDn = $this->parameterBag->get('ldap_users_uid').'='.$user->getUsername().','.$this->parameterBag->get('ldap_users_ou').','.$this->parameterBag->get('ldap_base_dn');

			try {
				$ldap->bind($userDn, $password);  //If invalid pw then catch the error

				//Check that the user is in the oivision group
				$query = $ldap->query($this->parameterBag->get('ldap_groups_ou').','.$base_dn, '(&(gidNumber='.$this->parameterBag->get('ldap_gid_num').')('.$this->parameterBag->get('ldap_group_uid').'='.$username.'))');
				$resultsGroup = $query->execute();

				if($resultsGroup->count() == 0){
					throw new CustomUserMessageAuthenticationException(
						'Username is denied access to '.$this->parameterBag->get('site_name').'!'
					);
				}

				//The user is valid at this point so now we add the user to the db
				if(!$this->addDbUser($user)){
					$this->container->get('monolog.logger.oivision')->info('User: '.$user->getUsername().'Error: Failed to create user in the database',
						['key' => 'user', 'function' => __CLASS__.'::'.__FUNCTION__]);
					throw new CustomUserMessageAuthenticationException(
						'Error Occurred. Contact support!'
					);
					return false;
				}
				return true;

			}catch (ConnectionException $ce) {
				//If we get here the bind on the ldap user failed.
				//dump('ldap user bind failed');
				$this->container->get('monolog.logger.security')->info('User: '.$user->getUsername().' bind failed. Error: '.$ce->getMessage(),
					['key' => 'ldap', 'function' => __CLASS__.'::'.__FUNCTION__]);
				throw new CustomUserMessageAuthenticationException(
					'Invalid Login Credentials!'
				);
			}
		}

		return false;

	}

	private function addDbUser(UserEntity $user){
		//dump("ADDING USER TO DB");


		$roles = $this->em->getRepository(LdapGroupsEntity::class)->findOneBy(['gid' => str_replace('gidNumber=','',$user->getComment())])->getRoles();
		$user->setRoles(json_decode($roles));
		$user->setCustomerId($this->em->getRepository(CustomerEntity::class)->findOneBy(['name' => $this->parameterBag->get('default_cust_name')]));
		if(!in_array('ROLE_EMPLOYEE', json_decode($roles))){
			$user->setEnabled(false);
		}
		$user->setEmailAlarmNotify(true);
		$user->setEmailCriticalNotify(true);

		try{
			$this->em->persist($user);
			$this->em->flush();
		}catch (NotNullConstraintViolationException $nn) {
//			dump($nn);die();
			throw new CustomUserMessageAuthenticationException(
				'Contact Support to update your user profile!'
			);
			return false;
		}

		if(!in_array('ROLE_EMPLOYEE', json_decode($roles))){
			$this->container->get('monolog.logger.security')->info($user->getUsername().' is a customer and needs a customer id assigned before they can login',
				['key' => 'ldap', 'function' => __CLASS__.'::'.__FUNCTION__]);
			throw new CustomUserMessageAuthenticationException(
				'Contact support to assign your customer id!'
			);
			return false;
		}

//		dump("HERE");die();
		return true;
	}

	/**
	 * Called when authentication executed, but failed (e.g. wrong username password).
	 *
	 * This should return the Response sent back to the user, like a
	 * RedirectResponse to the login page or a 403 response.
	 *
	 * If you return null, the request will continue, but the user will
	 * not be authenticated. This is probably not what you want to do.
	 *
	 * @param Request $request
	 * @param AuthenticationException $exception
	 *
	 * @return Response|null
	 */
	public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
	{
		$request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
		$url = $this->router->generate('security_login');
		return new RedirectResponse($url);
	}

	/**
	 * Called when authentication executed and was successful!
	 *
	 * This should return the Response sent back to the user, like a
	 * RedirectResponse to the last page they visited.
	 *
	 * If you return null, the current request will continue, and the user
	 * will be authenticated. This makes sense, for example, with an API.
	 *
	 * @param Request $request
	 * @param TokenInterface $token
	 * @param string $providerKey The provider (i.e. firewall) key
	 *
	 * @return Response|null
	 */
	public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
	{
		$targetPath = $this->getTargetPath($request->getSession(), $providerKey);

		if (!$targetPath) {
			$targetPath = $this->router->generate('homepage');
		}

		$changePwd = $token->getUser()->getPasswordChangeNeeded();
		if($changePwd){
			$targetPath = $this->router->generate('users_profile');
			$this->session->getFlashBag()->add('error', 'Please change your password now!');
		}



		return new RedirectResponse($targetPath);
	}

	/**
	 * Does this method support remember me cookies?
	 *
	 * Remember me cookie will be set if *all* of the following are met:
	 *  A) This method returns true
	 *  B) The remember_me key under your firewall is configured
	 *  C) The "remember me" functionality is activated. This is usually
	 *      done by having a _remember_me checkbox in your form, but
	 *      can be configured by the "always_remember_me" and "remember_me_parameter"
	 *      parameters under the "remember_me" firewall key
	 *
	 * @return bool
	 */
	public function supportsRememberMe()
	{
		return true;
	}

	/**
	 * Returns a response that directs the user to authenticate.
	 *
	 * This is called when an anonymous request accesses a resource that
	 * requires authentication. The job of this method is to return some
	 * response that "helps" the user start into the authentication process.
	 *
	 * Examples:
	 *  A) For a form login, you might redirect to the login page
	 *      return new RedirectResponse('/login');
	 *  B) For an API token authentication system, you return a 401 response
	 *      return new Response('Auth header required', 401);
	 *
	 * @param Request $request The request that resulted in an AuthenticationException
	 * @param AuthenticationException $authException The exception that started the authentication process
	 *
	 * @return Response
	 */
	public function start(Request $request, AuthenticationException $authException = null)
	{
		$url = $this->router->generate('security_login');
		return new RedirectResponse($url);
	}
}