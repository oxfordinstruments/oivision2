<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/19/2017
 * Time: 7:21 PM
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\SystemsEntity;

class SystemsRepository extends EntityRepository
{
	public function findAllSystems()
	{
		return $this->createQueryBuilder('systems')
			->orderBy('systems.systemId', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllSystemsEnabled()
	{
		return $this->createQueryBuilder('systems')
			->andWhere('systems.enabled = 1')
			->orderBy('systems.systemId', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllSystemsNotEnabled()
	{
		return $this->createQueryBuilder('systems')
			->andWhere('systems.enabled = 0')
			->orderBy('systems.systemId', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllMySystemsEnabled($customerId)
	{
		return $this->createQueryBuilder('systems')
			->andWhere('systems.enabled = 1')
			->andWhere("systems.customerId = :customerId")
			->setParameter('customerId', $customerId)
			->orderBy('systems.systemId', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllMySystemsNotEnabled($customerId)
	{
		return $this->createQueryBuilder('systems')
			->andWhere('systems.enabled = 0')
			->andWhere("systems.customerId = :customerId")
			->setParameter('customerId', $customerId)
			->orderBy('systems.systemId', 'DESC')
			->getQuery()
			->execute();
	}

	public function findByMacRdc($mac_rdc)
	{
		return $this->createQueryBuilder('systems')
			->andWhere("systems.mac_rdc = '".$mac_rdc."'")
			->setMaxResults(1)
			->getQuery()
			->execute();

	}

	public function getMaxSystemId()
	{
		return $this->createQueryBuilder('systems')
			->orderBy('systems.systemId', 'DESC')
			->setMaxResults(1)
			->getQuery()
			->execute();

	}

	public function countSystems()
	{
		$query = $this->createQueryBuilder('s')
			->where('s.id > 0')
			->getQuery()
			->execute()
		;

		return count($query)-1;

	}

	public function findAllEnabledByCustomer($customerId)
	{
		return $this->createQueryBuilder('s')
			->where("s.customerId = $customerId")
			->andWhere('s.enabled = 1')
			->orderBy('s.systemId', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllMySystemsWithTimestamp($customerId)
	{
		return $this->createQueryBuilder('s')
			->addSelect('FIRST(SELECT sdg1.timestamp FROM AppBundle:SystemsDataGeEntity sdg1 WHERE sdg1.system = s.id ORDER BY sdg1.timestamp DESC) AS ts')
			->addSelect('FIRST(SELECT sdg2.he FROM AppBundle:SystemsDataGeEntity sdg2 WHERE sdg2.system = s.id ORDER BY sdg2.timestamp DESC) AS he')
			->addSelect('FIRST(SELECT sdg3.vp FROM AppBundle:SystemsDataGeEntity sdg3 WHERE sdg3.system = s.id ORDER BY sdg3.timestamp DESC) AS vp')
			->where("s.customerId = :customerId")
			->andWhere('s.enabled = 1')
			->setParameter('customerId', $customerId)
			->orderBy('s.systemId', 'ASC')
			->getQuery()
			->execute();
	}

	public function findAllSystemsWithTimestamp()
	{
		return $this->createQueryBuilder('s')
			->addSelect('FIRST(SELECT sdg1.timestamp FROM AppBundle:SystemsDataGeEntity sdg1 WHERE sdg1.system = s.id ORDER BY sdg1.timestamp DESC) AS ts')
			->addSelect('FIRST(SELECT sdg2.he FROM AppBundle:SystemsDataGeEntity sdg2 WHERE sdg2.system = s.id ORDER BY sdg2.timestamp DESC) AS he')
			->addSelect('FIRST(SELECT sdg3.vp FROM AppBundle:SystemsDataGeEntity sdg3 WHERE sdg3.system = s.id ORDER BY sdg3.timestamp DESC) AS vp')
			->andWhere('s.enabled = 1')
			->orderBy('s.systemId', 'ASC')
			->getQuery()
			->execute();
	}

	public function findAllByCustomer($customerId)
	{
		return $this->createQueryBuilder('s')
			->where("s.customerId = $customerId")
			->orderBy('s.systemId', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllOldSystemMacs()
	{
		return $this->createQueryBuilder('s')
			->select('s.id, s.mac_rdc, s.systemId')
			->where('s.oivOld = 1')
			->getQuery()
			->execute();
	}
}