<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/11/2017
 * Time: 11:12 AM
 */

namespace AppBundle\Repository;

use AppBundle\Entity\SystemsEntity;
use AppBundle\Entity\UserEntity;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class SystemsDataGeRepository extends EntityRepository
{

	public function getCurrentHourAlarms(AuthorizationCheckerInterface $checker, UserEntity $user = null, bool $critical = false)
	{
		$qb =  $this->createQueryBuilder('a')
			->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXHourDate());
		if($critical){
			$qb->andWhere('a.hasCriticalAlarm = 1');
		}else{
			$qb->andWhere('a.hasAlarm = 1');
		}

		if (!is_null($user) and !$checker->isGranted('ROLE_EMPLOYEE')) {
			$qb->andWhere('s.customerId = :customerId')
				->setParameter('customerId', $user->getCustomerId());
		}

		return $qb->getQuery()->execute();
	}

	public function getCurrentHourCriticalAlarms(AuthorizationCheckerInterface $checker, UserEntity $user = null)
	{
		return $this->getCurrentHourAlarms($checker, $user, true);
	}

	public function getCurrentHourAlarmsAllCount(AuthorizationCheckerInterface $checker, UserEntity $user = null)
	{
		$qb =  $this->createQueryBuilder('a')
			->select('count(a.id)')
			->leftJoin('a.system', 's')
			->groupBy('a.mac_rdc')
			->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXHourDate())
			->andWhere('a.hasCriticalAlarm = 1 OR a.hasAlarm = 1');

		if (!is_null($user) and !$checker->isGranted('ROLE_EMPLOYEE')) {
			$qb->andWhere('s.customerId = :customerId')
				->setParameter('customerId', $user->getCustomerId());
		}

		$result = $qb->getQuery()->getResult();


		return count($result);
	}

	public function getTodaysAlarms(AuthorizationCheckerInterface $checker, UserEntity $user = null, bool $critical = false)
	{

		$qb = $this->createQueryBuilder('a')
			->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXDayDate());

		if($critical){
			$qb->andWhere('a.hasCriticalAlarm = 1');
		}else{
			$qb->andWhere('a.hasAlarm = 1');
		}

		if (!is_null($user) and !$checker->isGranted('ROLE_EMPLOYEE')) {
			$qb->andWhere('s.customerId = :customerId')
				->setParameter('customerId', $user->getCustomerId());
		}

		return $qb->getQuery()->execute();
	}

	public function getTodaysCriticalAlarms(AuthorizationCheckerInterface $checker, UserEntity $user = null)
	{
		return $this->getTodaysAlarms($checker, $user, true);
	}

	public function getSystemsCurrentHourAlarms(AuthorizationCheckerInterface $checker, UserEntity $user = null, bool $critical = false)
	{
		$qb = $this->createQueryBuilder('a')
			->leftJoin('a.system', 's')
			->groupBy('s.id')
			->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXHourDate());

		if($critical){
			$qb->andWhere('a.hasCriticalAlarm = 1');
		}else{
			$qb->andWhere('a.hasAlarm = 1');
		}
		if (!is_null($user) and !$checker->isGranted('ROLE_EMPLOYEE')) {
			$qb->andWhere('s.customerId = :customerId')
				->setParameter('customerId', $user->getCustomerId());
		}

		return $qb->getQuery()->execute();
	}

	public function getSystemsCurrentHourCriticalAlarms(AuthorizationCheckerInterface $checker, UserEntity $user = null)
	{
		return $this->getSystemsCurrentHourAlarms($checker, $user,true);
	}

	public function getSystemsCurrentDayAlarms(
		AuthorizationCheckerInterface $checker,
		UserEntity $user = null,
		bool $critical = false,
		bool $group = true,
		bool $getProcessed = true,
		bool $orderDesc = false
	)
	{
		$qb = $this->createQueryBuilder('a')
			->leftJoin('a.system', 's');

		if($group){
			$qb->groupBy('s.systemId');
		}

		$qb->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXDayDate());

		if($critical){
			$qb->andWhere('a.hasCriticalAlarm = 1');
			if(!$getProcessed){
				$qb->andWhere('a.criticalProcessed = 0');
			}
		}else{
			$qb->andWhere('a.hasAlarm = 1');
			if(!$getProcessed){
				$qb->andWhere('a.alarmProcessed = 0');
			}
		}

		if (!is_null($user) and !$checker->isGranted('ROLE_EMPLOYEE')) {
			$qb->andWhere('s.customerId = :customerId')
				->setParameter('customerId', $user->getCustomerId());
		}

		if($orderDesc){
			$qb->orderBy('a.timestamp', 'DESC');
		}
		return $qb->getQuery()->execute();
	}

	public function getSystemsCurrentDayCriticalAlarms(AuthorizationCheckerInterface $checker, UserEntity $user = null, bool $group = true, bool $getProcessed = true)
	{
		return $this->getSystemsCurrentDayAlarms($checker, $user, true, $group, $getProcessed);
	}

	public function getSystemsCurrentDayAlarmsCount(AuthorizationCheckerInterface $checker, UserEntity $user = null, bool $critical = false, bool $getProcessed = true)
	{
		$qb = $this->createQueryBuilder('a')
			->select('COUNT(a.id)')
			->leftJoin('a.system', 's')
			->groupBy('s.systemId')
			->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXDayDate());

		if($critical){
			$qb->andWhere('a.hasCriticalAlarm = 1');
			if(!$getProcessed){
				$qb->andWhere('a.criticalProcessed = 0');
			}
		}else{
			$qb->andWhere('a.hasAlarm = 1');
			if(!$getProcessed){
				$qb->andWhere('a.alarmProcessed = 0');
			}
		}

		if (!is_null($user) and !$checker->isGranted('ROLE_EMPLOYEE')) {
			$qb->andWhere('s.customerId = :customerId')
				->setParameter('customerId', $user->getCustomerId());
		}

		return $qb->getQuery()->getScalarResult();
	}

	public function getSystemsCurrentDayCriticalAlarmsCount(AuthorizationCheckerInterface $checker, UserEntity $user = null, bool $getProcessed = true)
	{
		return $this->getSystemsCurrentDayAlarms($checker, $user, true, $getProcessed);
	}

	public function getSystemsCurrentDayAllAlarms(AuthorizationCheckerInterface $checker, UserEntity $user = null)
	{
		$qb = $this->createQueryBuilder('a')
			->leftJoin('a.system', 's')
			->groupBy('s.systemId')
			->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXDayDate())
			->andWhere('a.hasCriticalAlarm = 1 OR a.hasAlarm = 1');

		if (!is_null($user) and !$checker->isGranted('ROLE_EMPLOYEE')) {
			$qb->andWhere('s.customerId = :customerId')
				->setParameter('customerId', $user->getCustomerId());
		}

		return $qb->getQuery()->execute();
	}

	public function getSystemsNotProcessedCritical()
	{
		$qb = $this->createQueryBuilder('a')
			->leftJoin('a.system', 's')
			->leftJoin ('s.customerId', 'c')
			->addSelect('c.name, c.id')
			->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXDayDate(5))
			->andWhere('a.criticalProcessed = 0');

		return $qb->getQuery()->execute();
	}

	public function getSystemsNotProcessedCriticalCount()
	{
		$qb = $this->createQueryBuilder('a')
			->select('COUNT(a.id)')
			->leftJoin('a.system', 's')
//			->groupBy('s.systemId')
			->where('a.timestamp > :date')
			->setParameter(':date', $this->minusXDayDate(5))
			->andWhere('a.criticalProcessed = 0');

		return $qb->getQuery()->getScalarResult();
	}



	public function getSystemsAlarmsBetweenTimes(
		AuthorizationCheckerInterface $checker,
		UserEntity $user = null,
		DateTime $startDate,
		DateTime $endDate,
		bool $critical = false,
		bool $group = true,
		bool $getProcessed = true,
		bool $orderDesc = false
	)
	{
		$qb = $this->createQueryBuilder('a')
			->leftJoin('a.system', 's');

		if($group){
			$qb->groupBy('s.systemId');
		}

		$qb->where('a.timestamp BETWEEN :end AND :start')
			->setParameter(':start', $startDate)
			->setParameter(':end', $endDate);

		if($critical){
			$qb->andWhere('a.hasCriticalAlarm = 1');
			if(!$getProcessed){
				$qb->andWhere('a.criticalProcessed = 0');
			}
		}else{
			$qb->andWhere('a.hasAlarm = 1');
			if(!$getProcessed){
				$qb->andWhere('a.alarmProcessed = 0');
			}
		}

		if (!is_null($user) and !$checker->isGranted('ROLE_EMPLOYEE')) {
			$qb->andWhere('s.customerId = :customerId')
				->setParameter('customerId', $user->getCustomerId());
		}

		if($orderDesc){
			$qb->orderBy('a.timestamp', 'DESC');
		}
		return $qb->getQuery()->execute();
	}

	public function getSystemsCriticalAlarmsBetweenTimes(
		AuthorizationCheckerInterface $checker,
		UserEntity $user = null,
		DateTime $startDate,
		DateTime $endDate,
		bool $group = true,
		bool $getProcessed = true,
		bool $orderDesc = false
	)
	{
		return $this->getSystemsAlarmsBetweenTimes($checker, $user, $startDate, $endDate, true, $group, $getProcessed, $orderDesc);
	}



	public function getSystemData(SystemsEntity $system, int $days = 14, int $limit = 500)
	{
		$from = new \DateTime('now');
		$from->setTimezone(new \DateTimeZone('UTC'));
		$from->add(new \DateInterval('PT1H'));
		$to   = new \DateTime('now');
		$to->setTimezone(new \DateTimeZone('UTC'));
		$to->sub(new \DateInterval('P'.$days.'D'));

		return $this->createQueryBuilder('d')
			->where('d.system = :system')
			->andWhere("d.timestamp BETWEEN :to AND :from")
			->setParameter(':system', $system)
			->setParameter(':from', $from)
			->setParameter(':to', $to)
			->orderBy('d.timestamp','DESC')
			->setMaxResults($limit)
			->getQuery()
			->execute();
	}

	public function getSystemDataBetweenDates(SystemsEntity $system, \DateTime $from, \DateTime $to, int $limit = 500)
	{
		return $this->createQueryBuilder('d')
			->where('d.system = :system')
			->andWhere("d.timestamp BETWEEN :to AND :from")
			->setParameter(':system', $system)
			->setParameter(':from', $from)
			->setParameter(':to', $to)
			->orderBy('d.timestamp','DESC')
			->setMaxResults($limit)
			->getQuery()
			->execute();
	}

	public function deleteSystemData($mac){
		$qb = $this->createQueryBuilder( 'd')
			->delete('AppBundle:SystemsDataGeEntity', 'd')
			->andWhere('d.mac_rdc = :mac')
			->setParameter(':mac', $mac);
		return $qb->getQuery()->execute();

	}

	private function minusXHourDate(int $hours = 1)
	{
		$date = new \DateTime();
		$date->modify('-'.$hours.' hour');
		$date->modify('-1 minute');
		return $date;
	}

	private function minusXDayDate(int $days = 1)
	{
		$date = new \DateTime();
		$date->modify('-'.$days.' day');
		return $date;
	}
}