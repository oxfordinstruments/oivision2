<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/18/2017
 * Time: 9:03 PM
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GlobalAlarmsRepository extends EntityRepository
{
	public function findLastId()
	{
		return $this->createQueryBuilder('global_alarms')
			->orderBy('global_alarms.id')
			->setMaxResults(1)
			->getQuery()
			->execute();
	}
}