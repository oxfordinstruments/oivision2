<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 12/20/2016
 * Time: 3:05 PM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\TestEntity;
use Doctrine\ORM\EntityRepository;

class TestRepository extends EntityRepository
{
	/**
	 * @return TestEntity[]
	 */
	public function findAllSystemsByName()
	{
		return $this->createQueryBuilder('system')
			->andWhere('system.system = :sysname1')
			->orWhere('system.system = :sysname2')
			->setParameter('sysname1', 'NX/i')
			->setParameter('sysname2', 'Symfony')
			->orderBy('system.name', 'DESC')
			->getQuery()
			->execute();
	}
}