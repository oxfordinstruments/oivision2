<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/18/2017
 * Time: 9:03 PM
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ApiKeyRepository extends EntityRepository
{
	public function findKey($token)
	{
		return $this->createQueryBuilder('api_key')
			->andWhere("api_key.token = '".$token."'")
			->getQuery()
			->getOneOrNullResult();
	}

	public function findCustomer($cust)
	{
		return $this->createQueryBuilder('c')
			->andWhere("c.customerId = '".$cust."'")
			->getQuery()
			->getOneOrNullResult();
	}
}