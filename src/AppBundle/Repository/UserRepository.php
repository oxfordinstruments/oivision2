<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/28/2017
 * Time: 4:26 PM
 */

namespace AppBundle\Repository;

use AppBundle\Entity\UserEntity;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
	public function findAllUsersEnabled()
	{
		return $this->createQueryBuilder('user')
			->andWhere('user.enabled = 1')
			->orderBy('user.uid', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllUsersNotEnabled()
	{
		return $this->createQueryBuilder('user')
			->andWhere('user.enabled = 0')
			->orderBy('user.uid', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllUsersNotRegistered()
	{
		return $this->createQueryBuilder('user')
			->andWhere('user.registered = 0')
			->orderBy('user.uid', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllUsersEnabledAlarmNotify()
	{
		return $this->createQueryBuilder('u')
			->andWhere('u.enabled = 1')
			->andWhere('u.emailAlarmNotify = 1')
			->orderBy('u.uid', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllUsersEnabledCriticalNotify()
	{
		return $this->createQueryBuilder('u')
			->andWhere('u.enabled = 1')
			->andWhere('u.emailCriticalNotify = 1')
			->orderBy('u.uid', 'DESC')
			->getQuery()
			->execute();
	}

	public function getEnabled()
	{

	}
}