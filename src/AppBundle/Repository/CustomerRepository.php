<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 11/6/2017
 * Time: 11:54 AM
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CustomerRepository extends EntityRepository
{
	public function findAllCustomersEnabled()
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.enabled = 1')
			->orderBy('c.name', 'DESC')
			->getQuery()
			->execute();
	}

	public function findAllCustomersNotEnabled()
	{
		return $this->createQueryBuilder('c')
			->andWhere('c.enabled = 0')
			->orderBy('c.name', 'DESC')
			->getQuery()
			->execute();
	}

}