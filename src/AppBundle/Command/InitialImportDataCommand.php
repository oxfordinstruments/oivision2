<?php

namespace AppBundle\Command;

use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use AppBundle\Resources\combineOldData;
use mysqli;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class InitialImportDataCommand extends ContainerAwareCommand
{
	private $year;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
	    $now = new \DateTime('now');
	    $now->sub(new \DateInterval('P1Y'));
	    $year = intval($now->format('Y'));

        $this
            ->setName('app:initialImportData')
            ->setDescription('Initial Import of Old Oivision Data')
	        ->addOption('year', null, InputOption::VALUE_OPTIONAL, '--year 2017  The year to import data from up to now. Defaults to now minus 1 year.', $year)
	        ->addOption('reset', null, InputOption::VALUE_OPTIONAL, 'Set --reset 1  Ignore new data.', 0)
	        ->addOption('all', null, InputOption::VALUE_OPTIONAL, '--all 1  Process all data ignoring new flag', 0)
	        ;
		set_time_limit(0);
		ini_set('memory_limit','1G');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

    	$this->year = intval($input->getOption('year'));

	    $output->writeln("Using Year: ".$this->year);
	    $output->writeln("Using Reset: ".$input->getOption('reset'));
	    $cod = new combineOldData($this->getContainer(), $this->year);

	    if($input->getOption('reset') == 1){
	    	$cod->setCompletedSites(array());
	    	$cod->markAll(0);
	    }

	    $mysqli = new mysqli(
	    	$this->getContainer()->getParameter('remote_database_host'),
		    $this->getContainer()->getParameter('remote_database_user'),
		    $this->getContainer()->getParameter('remote_database_password'),
		    $this->getContainer()->getParameter('remote_database_name')
	    );

	    if ($mysqli->connect_errno) {
		    die('There was an error running the query [' . $mysqli->connect_error . ']');
	    }

	    if(!empty($cod->getCompletedSitesString($output))) {
		    $sql = "SELECT * FROM sites WHERE mac IN(" . $cod->getCompletedSitesString($output) . ");";
		    if($result = $mysqli->query($sql)){
			    while ($row = $result->fetch_assoc()) {
				    $output->writeln("Existing System: " . $row['site_name']);
			    }
		    }
	    }

	    $sql = "SELECT * FROM sites WHERE mac NOT IN(" . $cod->getCompletedSitesString($output) . ");";
	    echo $sql, PHP_EOL;
	    if(!$result = $mysqli->query($sql)){
	    	die($mysqli->error);
	    }
	    $mmSites = [];
	    while ($row = $result->fetch_assoc()) {
		    array_push($mmSites, $row);
	    }
	    $result->free();
	    $mysqli->close();

	    $repo = $this->getContainer()->get('doctrine')->getRepository(SystemsDataGeEntity::class);
	    foreach ($mmSites as $site) {
		    $output->writeln("Deleting data for: ".$site['mac']);
		    $repo->createQueryBuilder('s')
			    ->delete()
			    ->where('s.mac_oiv = :system')
			    ->setParameter(':system', $site['mac'])
			    ->getQuery()
			    ->execute();
	    }

	    $repo = $this->getContainer()->get('doctrine')->getRepository(SystemsEntity::class);
	    foreach ($mmSites as $site) {
		    $output->writeln("Deleting site: ".$site['site_name']);
		    $repo->createQueryBuilder('s')
			    ->delete()
			    ->where('s.mac_rdc = :system')
			    ->setParameter(':system', $site['mac'])
			    ->getQuery()
			    ->execute();
	    }
	    
	    $cod->createSystems($output, $mmSites);

	    $container = $this->getContainer();
	    foreach ($mmSites as $site) {
	    	$this->getContainer()->get('doctrine')->getManager()->clear();
		    $codx = new combineOldData($container, $this->year);
	        $codx->importData($output, $site, $input->getOption('reset'), $input->getOption('all'));
	        $codx->clean();
	        unset($codx);
		    gc_enable();
	    }

	    $cod->markAll();

	    $output->writeln("DONE");

    }
}
