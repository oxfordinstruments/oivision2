<?php

namespace AppBundle\Command;

use AppBundle\Entity\ApiKeyEntity;
use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class getDataCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('app:get_data_command')
			->setDescription('Gets the data from OiVision 1.0')
			->addOption('debug', ['-d'], InputOption::VALUE_NONE, 'Debug')
			->addOption('no_persist', ['-p'], InputOption::VALUE_NONE, 'Do not persist data to the db')
			->addOption('cron', ['-c'], InputOption::VALUE_NONE, "Don't show progress for cron job");
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln(sprintf('%s - Begin getting remote data...',strftime("%c")));

		$persist = $input->getOption('no_persist');
		$persist = !$persist;
		$debug = $input->getOption('debug');
		$cron = $input->getOption('cron');

		$client = new Client([
			'base_uri' => 'http://oi-vision.com/api/',
			'timeout' => 120.0,
			'headers' => [
				'Content-Type' => 'application/json',
				'Token' => $this->getContainer()->getParameter('oiv_api_key'),
			]
		]);

		try{
			$response = $client->request('POST', 'getData', [
				'json' => [
					'limit' => $this->getContainer()->getParameter('oiv_max_import')
				]
			]);
		}catch (ClientException $clientException){
			$output->writeln(sprintf("Error Code: %s", $clientException->getCode()));
			$output->writeln(sprintf("Error Msg: %s", $clientException->getMessage()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}catch (RequestException $requestException){
			$output->writeln(sprintf("Error Code: %s", $requestException->getCode()));
			$output->writeln(sprintf("Error Msg: %s", $requestException->getMessage()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}


		if($response->getStatusCode() != 200){
			$output->writeln(sprintf("Error Code: %s", $response->getStatusCode()));
			$output->writeln(sprintf("Error Msg: %s", $response->getBody()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}

		$responseJson = json_decode($response->getBody(), true);

		$output->writeln(sprintf("%s - Getting systems list...",strftime("%c")));
		$sysRepo = $this->getContainer()->get('doctrine')->getManager()->getRepository(SystemsEntity::class);
		$systems = $sysRepo->findAllOldSystemMacs();
		$systemMacs = null;
		foreach ($systems as $system){
			$systemMacs[$system['id']] = $system['mac_rdc'];
		}

		$output->writeln(sprintf('%s - Importing data...',strftime("%c")));

		$importErrors = array();
		$importedData = array();
		$totalMacs = count(array_keys($responseJson['data']));

		foreach ($responseJson['data'] as $key=>$dataArr){
			if(!$cron){
				$output->write(sprintf("%s --- %s ---", $totalMacs, $key));
			}


			if(!in_array($key, $systemMacs)){
				$importErrors[$key] = ['error' => 'Mac not found in local systems list'];
				if ($debug){$output->writeln(sprintf("Mac %s not found", $key));}
				$totalMacs -= 1;
				if(!$cron){
					$output->writeln("");
					$output->writeln("");
				}
				continue;
			}
			$systemId = array_search($key, $systemMacs);
			if ($debug){$output->writeln(sprintf("Importing SysID: %s Mac: %s", $systemId, $systemMacs[$systemId]));}
			/** @var SystemsEntity $system */
			$system = $sysRepo->findOneBy(['id' => $systemId]);
			$importRtn = $this->importData($system, $dataArr, $output, $persist, $debug, $cron);

			if(!empty($importRtn)){
				$imptmp = array();
				foreach ($importRtn as $idx => $id){
					array_push($imptmp, $idx);
				}
				$importedData[$key] = $imptmp;
			}
			$totalMacs -= 1;
		}

		$rtn = true;
		if(!empty($importErrors)){
			$output->writeln(sprintf("Import Errors: %s", json_encode($importErrors, JSON_PRETTY_PRINT)));
			$rtn = false;
		}


		$output->writeln(sprintf("%s - Sending mark request to remote...",strftime("%c")));

		try{
			$response = $client->request('POST', 'markData',[
				'body' => json_encode($importedData),
			]);
		}catch (ClientException $clientException){
			$output->writeln(sprintf("Error Code: %s", $clientException->getCode()));
			$output->writeln(sprintf("Error Msg: %s", $clientException->getMessage()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}catch (RequestException $requestException){
			$output->writeln(sprintf("Error Code: %s", $requestException->getCode()));
			$output->writeln(sprintf("Error Msg: %s", $requestException->getMessage()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}

		if($response->getStatusCode() != 200){
			$output->writeln(sprintf("Error Code: %s", $response->getStatusCode()));
			$output->writeln(sprintf("Error Msg: %s", $response->getBody()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}
		if($debug){
			$responseJson = json_decode($response->getBody(), true);
			$output->writeln(print_r($responseJson));
		}

		$output->writeln(sprintf("%s - Done importing data",strftime("%c")));
		return $rtn;
	}

	/**
	 * @param SystemsEntity $system
	 * @param array $dataArr
	 * @param OutputInterface $output
	 * @param bool $persist
	 * @param bool $debug
	 * @param bool $cron
	 * @return array
	 *
	 * Returns array(remote index => local id) or empty array if persist is false.
	 */
	private function importData(SystemsEntity $system, array $dataArr, OutputInterface $output, bool $persist, bool $debug, bool $cron)
	{
		$em = $this->getContainer()->get('doctrine')->getManager();
		$utils = $this->getContainer()->get('app.utils_common');

		$apiRepo = $this->getContainer()->get('doctrine')->getManager()->getRepository(ApiKeyEntity::class);
		$apiToken = $apiRepo->findKey('6548admin101317');
		$tz = new \DateTimeZone('America/Kentucky/Louisville');

		$totalData = count(array_keys($dataArr));

		$importedIds = array();
		foreach ($dataArr as $key=>$datum){
			if(!$cron){
				$output->write(sprintf("%s,", $totalData));
			}

			$data = new SystemsDataGeEntity();

			$data->setPulledData(true);
			$data->setApiKey($apiToken);
			$data->setMacRdc($system->getMacRdc());
			$data->setMacOiv($system->getMacRdc());
			$data->setInStorage($system->getInStorage());
			$data->setSystem($system);
			$data->setHe(floatval($datum['He']));
			$data->setVp(floatval($datum['HeP']));
			$data->setTf(floatval($datum['Tf']));
			$data->setRh(floatval($datum['Rh']));
			$data->setWf(floatval($datum['Wf']));
			$data->setWt(floatval($datum['Wtf']));
			$data->setQ($this->oz($datum['FieldOn']));
			$data->setCmp($this->oz($datum['CompOn']));
			$data->setDp($utils->dewPoint($data->getTf(), $data->getRh()));
			$data->setDpdiff($data->getTf() - $data->getDp());

			$insertDate = new \DateTime($datum['insertDate'], $tz);
			$insertDate->setTimezone(new \DateTimeZone('UTC'));
			$data->setTimestamp($insertDate);

			$data = $utils->checkDataGeForAlarms($data, $system);

			if($persist){
				$em->persist($data);
				$em->flush();
				$importedIds[$datum['index']] = $data->getId();
			}
			if($debug){$output->writeln(sprintf("Imported remote index: %s  Persisted in db: %s  Local index: %s", $datum['index'], json_encode($persist), json_encode($data->getId()) ));}
			$totalData -= 1;
		}

		if(!$cron) {
			$output->writeln("");
			$output->writeln("");
		}
		return $importedIds;
	}

	private function yn($string)
	{
		if(strtolower($string) == 'y'){
			return true;
		}
		return false;
	}

	private function oz($string)
	{
		if(strtolower($string) == '1'){
			return true;
		}
		return false;
	}
}
