<?php

namespace AppBundle\Command;

use AppBundle\Entity\GlobalAlarmSettingsEntity;
use AppBundle\Entity\SystemsEntity;
use AppBundle\Repository\GlobalAlarmsRepository;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Proxies\__CG__\AppBundle\Entity\CustomerEntity;
use ProxyManager\Proxy\FallbackValueHolderInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use GuzzleHttp\Client;

class getSitesCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('app:get_sites_command')
			->setDescription('Gets the sites enabled from OIV 1.0');
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln('Begin getting remote sites...</br>');

		$client = new Client([
			'base_uri' => 'http://oi-vision.com/api/',
			'timeout' => 120.0,
			'headers' => [
				'Content-Type' => 'application/json',
				'Token' => $this->getContainer()->getParameter('oiv_api_key'),
			]
		]);

		try{
			$response = $client->request('POST', 'getSites');
		}catch (ClientException $clientException){
			$output->writeln(sprintf("Error Code: %s", $clientException->getCode()));
			$output->writeln(sprintf("Error Msg: %s", $clientException->getMessage()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}catch (RequestException $requestException){
			$output->writeln(sprintf("Error Code: %s", $requestException->getCode()));
			$output->writeln(sprintf("Error Msg: %s", $requestException->getMessage()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}


		if($response->getStatusCode() != 200){
			$output->writeln(sprintf("Error Code: %s", $response->getStatusCode()));
			$output->writeln(sprintf("Error Msg: %s", $response->getBody()));
			$output->writeln(sprintf("%s exiting !", $this->getName()));
			return false;
		}

		$responseJson = json_decode($response->getBody(), true);

		$output->writeln('Begin getting local sites...</br>');
		$sysRepo = $this->getContainer()->get('doctrine')->getManager()->getRepository(SystemsEntity::class);
		$systems = $sysRepo->findAllOldSystemMacs();
		$systemMacs = null;
		$systemIds = null;
		foreach ($systems as $system){
			$systemMacs[$system['id']] = $system['mac_rdc'];
			$systemIds[$system['id']] = strtolower($system['systemId']);
		}

		$output->writeln('Checking for new systems...</br>');
		$newSystems = false;
		$updateSystems = false;
		foreach ($responseJson['data'] as $key => $datum){
			if(!in_array(strtolower($datum['mac']), $systemMacs)){
				if(!in_array( 'mr'.$datum['site_id'], $systemIds)){
					$newSystems[$key] = [ 'index' => $datum['index'], 'mac' => strtolower($datum['mac']) ];
				}else{
					$updateSystems[$key] = [ 'index' => $datum['index'], 'site_id' => strtolower($datum['site_id']) ];
				}

			}
		}

		if(is_array($newSystems)){
			$output->writeln("New Systems found. Adding new systems to db...</br>");
			foreach ($newSystems as $key=>$system){
				$this->addSystem($output, $responseJson['data'][$key]);
			}
		}

		if(is_array($updateSystems)){
			$output->writeln("Update Systems found. Updating systems in db...</br>");
			foreach ($updateSystems as $key=>$system){
				$this->updateSystem($output, $responseJson['data'][$key], $system['site_id']);
			}
		}

		$output->writeln('Done');
		return true;
	}

	private function addSystem(OutputInterface $output, array $sysData)
	{
		$customerRepo = $this->getContainer()->get('doctrine')->getRepository(CustomerEntity::class);
		/** @var CustomerEntity $customer */
		$customer = $customerRepo->findOneBy(['id' => 1]);

		$gasRepo = $this->getContainer()->get('doctrine')->getRepository(GlobalAlarmSettingsEntity::class);
		$gas = $gasRepo->findLastId();
		/** @var GlobalAlarmSettingsEntity $gas */
		$gas = $gas[0];

		$system = new SystemsEntity();
		$system->setEnabled(true);
		$system->setRegistered(true);
		$system->setMacRdc(strtolower($sysData['mac']));
		$system->setOivOld(true);
		$system->setCustomerId($customer);
		$system->setSystemId('MR'.$sysData['site_id']);
		$system->setSystemName($sysData['site_name']);
		$system->setSystemAddress($sysData['site_address']);
		$system->setSystemCity($sysData['site_city']);
		$system->setSystemState(strtoupper($sysData['site_st']));
		$system->setSystemZip($sysData['site_zip']);

		$system->setMonitorHe($this->yn($sysData['He']));
		$system->setMonitorHeb($this->yn($sysData['HeB']));
		$system->setMonitorVp($this->yn($sysData['HeP']));
		$system->setMonitorTf($this->yn($sysData['Tf']));
		$system->setMonitorRh($this->yn($sysData['Rh']));
		$system->setMonitorWf($this->yn($sysData['Wf']));
		$system->setMonitorWt($this->yn($sysData['Wtf']));
		$system->setMonitorQ($this->yn($sysData['FieldOn']));
		$system->setMonitorCmp($this->yn($sysData['CompOn']));

		$system->setAvgHe(true);
		$system->setAvgVp(true);

		$system->setInStorage($this->yn($sysData['Storage']));
		$system->setUseGlobalAlarms($this->yn($sysData['use_global_alarm']));
		$system->setNotes('Added from OiVision 1.x'. PHP_EOL . $sysData['notes']);

		$system->setAlarmHeL(floatval($sysData['alarm_he']));
		$system->setAlarmHebH(floatval($sysData['alarm_heb']));
		$system->setAlarmVpH(floatval($sysData['alarm_vph']));
		$system->setAlarmVpL(floatval($sysData['alarm_vpl']));
		$system->setAlarmTfH(floatval($sysData['alarm_rth']));
		$system->setAlarmTfL(floatval($sysData['alarm_rtl']));
		$system->setAlarmRhH(floatval($sysData['alarm_rhh']));
		$system->setAlarmRhL(floatval($sysData['alarm_rhl']));
		$system->setAlarmWfH(floatval($sysData['alarm_wfh']));
		$system->setAlarmWfL(floatval($sysData['alarm_wfl']));
		$system->setAlarmWtH(floatval($sysData['alarm_wth']));
		$system->setAlarmWtL(floatval($sysData['alarm_wtl']));

		$system->setAlarmStorageHeL(floatval($sysData['alarm_she']));
		$system->setAlarmStorageVpH(floatval($sysData['alarm_svph']));
		$system->setAlarmStorageVpL(floatval($sysData['alarm_svpl']));

		$system->setCriticalHeL(floatval($sysData['alarm_hec']));
		$system->setCriticalHebH(floatval($sysData['alarm_heb']) + 2);
		$system->setCriticalVpH(floatval($sysData['alarm_vphc']));
		$system->setCriticalVpL(floatval($sysData['alarm_vplc']));
		$system->setCriticalTfH(floatval($sysData['alarm_rthc']));
		$system->setCriticalTfL(floatval($sysData['alarm_rtlc']));
		$system->setCriticalRhH(floatval($sysData['alarm_rhhc']));
		$system->setCriticalRhL(floatval($sysData['alarm_rhlc']));
		$system->setCriticalWfH(floatval($sysData['alarm_wfhc']));
		$system->setCriticalWfL(floatval($sysData['alarm_wflc']));
		$system->setCriticalWtH(floatval($sysData['alarm_wthc']));
		$system->setCriticalWtL(floatval($sysData['alarm_wtlc']));

		$system->setCriticalStorageHeL(floatval($sysData['alarm_shec']));
		$system->setCriticalStorageVpH(floatval($sysData['alarm_svphc']));
		$system->setCriticalStorageVpL(floatval($sysData['alarm_svplc']));

		$system->setOffsetHe($this->getContainer()->getParameter('offsetHe'));
		$system->setOffsetRh($this->getContainer()->getParameter('offsetRh'));
		$system->setOffsetTc($this->getContainer()->getParameter('offsetTc'));
		$system->setOffsetVp($this->getContainer()->getParameter('offsetVp'));
		$system->setOffsetWf($this->getContainer()->getParameter('offsetWf'));
		$system->setOffsetWtf($this->getContainer()->getParameter('offsetWtf'));

		$system->setAlarmDpDiff($gas->getAlarmDpDiff());
		$system->setAlarmDpH($gas->getAlarmDpH());
		$system->setCriticalDpDiff($gas->getCriticalDpDiff());
		$system->setCriticalDpH($gas->getCriticalDpH());
		$system->setMonitorDp(false);
		$system->setMonitorDpDiff(false);

		$em = $this->getContainer()->get('doctrine.orm.entity_manager');
		$em->persist($system);
		$em->flush();
		$output->writeln("Created System: ".$sysData['site_name']);

	}

	private function updateSystem(OutputInterface $output, array $sysData, string $site_id){
		$systemRepo = $this->getContainer()->get('doctrine')->getRepository(SystemsEntity::class);

		$gasRepo = $this->getContainer()->get('doctrine')->getRepository(GlobalAlarmSettingsEntity::class);
		$gas = $gasRepo->findLastId();
		/** @var GlobalAlarmSettingsEntity $gas */
		$gas = $gas[0];

		/** @var SystemsEntity $system */
		$system = $systemRepo->findOneBy(['systemId' => 'MR'.$site_id]);
		$systemOrig = clone $system;

		$output->writeln(sprintf("Updating %s </br>", $system->getSystemId()));
		$macChange = false;
		$systemIndex = false;
		$macNew = false;
		if(strtolower($sysData['mac']) != strtolower($system->getMacRdc())){
			$output->writeln(sprintf("-- changing mac from %s to %s</br>", $system->getMacRdc(), $sysData['mac']));
			$macChange = true;
			$systemIndex = $system->getId();
			$macNew = $sysData['mac'];
			$system->setMacRdc($macNew);
		}
		$system->setSystemName($sysData['site_name']);
		$system->setSystemAddress($sysData['site_address']);
		$system->setSystemCity($sysData['site_city']);
		$system->setSystemState(strtoupper($sysData['site_st']));
		$system->setSystemZip($sysData['site_zip']);

		$system->setMonitorHe($this->yn($sysData['He']));
		$system->setMonitorHeb($this->yn($sysData['HeB']));
		$system->setMonitorVp($this->yn($sysData['HeP']));
		$system->setMonitorTf($this->yn($sysData['Tf']));
		$system->setMonitorRh($this->yn($sysData['Rh']));
		$system->setMonitorWf($this->yn($sysData['Wf']));
		$system->setMonitorWt($this->yn($sysData['Wtf']));
		$system->setMonitorQ($this->yn($sysData['FieldOn']));
		$system->setMonitorCmp($this->yn($sysData['CompOn']));

		$system->setAvgHe(true);
		$system->setAvgVp(true);

		$system->setInStorage($this->yn($sysData['Storage']));
		$system->setUseGlobalAlarms($this->yn($sysData['use_global_alarm']));
		$system->setNotes($sysData['notes']);

		$system->setAlarmHeL(floatval($sysData['alarm_he']));
		$system->setAlarmHebH(floatval($sysData['alarm_heb']));
		$system->setAlarmVpH(floatval($sysData['alarm_vph']));
		$system->setAlarmVpL(floatval($sysData['alarm_vpl']));
		$system->setAlarmTfH(floatval($sysData['alarm_rth']));
		$system->setAlarmTfL(floatval($sysData['alarm_rtl']));
		$system->setAlarmRhH(floatval($sysData['alarm_rhh']));
		$system->setAlarmRhL(floatval($sysData['alarm_rhl']));
		$system->setAlarmWfH(floatval($sysData['alarm_wfh']));
		$system->setAlarmWfL(floatval($sysData['alarm_wfl']));
		$system->setAlarmWtH(floatval($sysData['alarm_wth']));
		$system->setAlarmWtL(floatval($sysData['alarm_wtl']));

		$system->setAlarmStorageHeL(floatval($sysData['alarm_she']));
		$system->setAlarmStorageVpH(floatval($sysData['alarm_svph']));
		$system->setAlarmStorageVpL(floatval($sysData['alarm_svpl']));

		$system->setCriticalHeL(floatval($sysData['alarm_hec']));
		$system->setCriticalHebH(floatval($sysData['alarm_heb']) + 2);
		$system->setCriticalVpH(floatval($sysData['alarm_vphc']));
		$system->setCriticalVpL(floatval($sysData['alarm_vplc']));
		$system->setCriticalTfH(floatval($sysData['alarm_rthc']));
		$system->setCriticalTfL(floatval($sysData['alarm_rtlc']));
		$system->setCriticalRhH(floatval($sysData['alarm_rhhc']));
		$system->setCriticalRhL(floatval($sysData['alarm_rhlc']));
		$system->setCriticalWfH(floatval($sysData['alarm_wfhc']));
		$system->setCriticalWfL(floatval($sysData['alarm_wflc']));
		$system->setCriticalWtH(floatval($sysData['alarm_wthc']));
		$system->setCriticalWtL(floatval($sysData['alarm_wtlc']));

		$system->setCriticalStorageHeL(floatval($sysData['alarm_shec']));
		$system->setCriticalStorageVpH(floatval($sysData['alarm_svphc']));
		$system->setCriticalStorageVpL(floatval($sysData['alarm_svplc']));

		$system->setOffsetHe($this->getContainer()->getParameter('offsetHe'));
		$system->setOffsetRh($this->getContainer()->getParameter('offsetRh'));
		$system->setOffsetTc($this->getContainer()->getParameter('offsetTc'));
		$system->setOffsetVp($this->getContainer()->getParameter('offsetVp'));
		$system->setOffsetWf($this->getContainer()->getParameter('offsetWf'));
		$system->setOffsetWtf($this->getContainer()->getParameter('offsetWtf'));

		$system->setAlarmDpDiff($gas->getAlarmDpDiff());
		$system->setAlarmDpH($gas->getAlarmDpH());
		$system->setCriticalDpDiff($gas->getCriticalDpDiff());
		$system->setCriticalDpH($gas->getCriticalDpH());
		$system->setMonitorDp(false);
		$system->setMonitorDpDiff(false);

		$this->diffs($output, $system, $systemOrig);

		$em = $this->getContainer()->get('doctrine.orm.entity_manager');
		$em->persist($system);
		$em->flush();

		if($macChange){
			$this->updateData($output, $systemIndex, $macNew);
		}

	}

	private function updateData(OutputInterface $output, $index, $macNew){
		$output->writeln("Changing mac on data points...</br>");
		$em = $this->getContainer()->get('doctrine.orm.entity_manager');
		$builder = $em->getConnection()->createQueryBuilder();
		return $builder->update('systems_data_ge s')
			->set('s.mac_rdc', ':mac')
			->set('s.mac_oiv', ':mac')
			->where('s.system_id = :idx')
			->setParameter('mac', $macNew)
			->setParameter('idx', $index)
			->execute();


	}

	private function diffs(OutputInterface $output, SystemsEntity $oldsys, SystemsEntity $newsys){
		try{
			$refoldsys = new \ReflectionClass($oldsys);
		}catch (\Exception $e){
			$output->writeln($e->getMessage());
			$output->writeln($e->getLine());
		}
		try{
			$refnewsys = new \ReflectionClass($newsys);
		}catch (\Exception $e){
			$output->writeln($e->getMessage());
			$output->writeln($e->getLine());
		}
		$refoldsysprops = $refoldsys->getProperties();

		/** @var \ReflectionProperty $propertyOld */
		foreach ($refoldsysprops as $propertyOld){
			try{
				$propertyOld->setAccessible(true);
				$oldval = $propertyOld->getValue($oldsys);
				if(is_array($oldval)){continue;}

				$propertyNew = $refnewsys->getProperty($propertyOld->getName());
				$propertyNew->setAccessible(true);
				$newval = $propertyNew->getValue($newsys);
				$name = $propertyOld->getName();
				if($oldval !== $newval){
					$output->writeln(sprintf("-- %s change from %s to %s</br>", $name, $oldval, $newval));
				}
			}catch (\Exception $e){continue;}

		}



	}

	private function yn($string)
	{
		if(strtolower($string) == 'y'){
			return true;
		}
		return false;
	}

	private function oz($string)
	{
		if(strtolower($string) == '1'){
			return true;
		}
		return false;
	}
}
