<?php

namespace AppBundle\Command;

use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use AppBundle\Entity\UserEntity;
use AppBundle\Repository\SystemsDataGeRepository;
use AppBundle\Repository\UserRepository;
use Swift_Mime_SimpleMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CheckCriticalsCommand extends ContainerAwareCommand
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this
			->setName('app:checkCriticals')
			->setDescription('Check for alarms sent from boxes')
			->setHelp('This command checks the data sent from boxes for alarms')
			->addArgument('timezone', InputArgument::REQUIRED, 'PHP Timezone of the users to send emails')
			->addOption('debug', ['-d'], InputOption::VALUE_NONE, 'Debug')
			->addOption('processed', ['-p'], InputOption::VALUE_NONE, 'Get Processed records')
			->addOption('startDate', ['-a'], InputOption::VALUE_OPTIONAL, 'Start Date in format 2018-03-08 12:00:00', false)
			->addOption('endDate', ['b'], InputOption::VALUE_OPTIONAL, 'End Date in format 2018-03-08 12:00:00', false)
		;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$utils = $this->getContainer()->get('app.utils_common');
		if(trim($input->getArgument('timezone')) != 'all') {
			if (!in_array($input->getArgument('timezone'), $utils->getTimeZonesList())) {
				throw new \InvalidArgumentException('Timezone must be one of: all, ' . implode(', ', $utils->getTimeZonesList()));
			}
		}

		if($input->getOption('debug')){
			$output->writeln('Debug Enabled');
		}

		$token = new AnonymousToken('dummy', 'dummy', ['ROLE_USER']);
		$this->getContainer()->get('security.token_storage')->setToken($token);

		$this->getContainer()->get('monolog.logger.oivision')
			->info('Begin Checking for alarms',
				['key' => 'cron', 'function' => __CLASS__.'::'.__FUNCTION__]);

		/**
		 * @var SystemsDataGeRepository $sysDataRepo
		 */
		$sysDataRepo = $this->getContainer()->get('doctrine')->getRepository(SystemsDataGeEntity::class);

		$processed = array();

		/**
		 * @var UserRepository $usersRepo
		 */
		$usersRepo = $this->getContainer()->get('doctrine')->getRepository(UserEntity::class);


		$users = $usersRepo->findAllUsersEnabledAlarmNotify();
		$adminUser = $usersRepo->findOneBy(['uid' => 'admin']);
		$mm = $this->getContainer()->get('app.mail_manager');
		$utils = $this->getContainer()->get('app.utils_common');
		$em = $this->getContainer()->get('doctrine')->getManager();

		$startDate = new \DateTime();
		if(is_string($input->getOption('startDate'))){
			$startDate->setTimestamp(strtotime($input->getOption('startDate')));
		}

		if($input->getOption('debug')){
			$output->writeln('StartDate: '.date_format($startDate, 'c'));
		}

		if(is_string($input->getOption('endDate'))){
			$endDate = new \DateTime();
			$endDate->setTimestamp(strtotime($input->getOption('endDate')));
		}else{
			$endDate = clone $startDate;
			$endDate->modify('-'.$this->getContainer()->getParameter('checkCriticalsHours').' hour');
			$endDate->modify('-1 minute');
		}

		if($input->getOption('debug')){
			$output->writeln('EndDate: '.date_format($endDate, 'c'));
		}



		/**
		 * @var SystemsEntity[]
		 */
		$adminUser->dataStore = [];

		$token = new AnonymousToken('dummy', $adminUser, $utils->getInheritedRoles($adminUser->getRoles()));
		$this->getContainer()->get('security.token_storage')->setToken($token);
		$getProcessed = false;
		if($input->getOption('processed')){
			$getProcessed = true;
		}
		$sysData = $sysDataRepo->getSystemsCriticalAlarmsBetweenTimes($this->getContainer()->get('security.authorization_checker'), $adminUser, $startDate, $endDate, false, $getProcessed, true);

		/**
		 * @var SystemsDataGeEntity $datum
		 */
		foreach ($sysData as $datum) {
			if(!in_array($datum->getSystem(), $adminUser->dataStore)){
				array_push($adminUser->dataStore, $datum->getSystem());
			}
			if(!in_array($datum, $processed)){
				array_push($processed, $datum);
			}
		}

		/**
		 * @var SystemsDataGeEntity $datum
		 */
		foreach ($sysData as $key => $datum) {
			$system = array_search($datum->getSystem(), $adminUser->dataStore);

			if($system !== false){
				$tmp = $adminUser->dataStore[$system]->getData();
				if(is_null($tmp)){
					$tmp = [];
				}
				array_push($tmp, $datum);
				$adminUser->dataStore[$system]->setData($tmp);
			}

		}

		if($input->getOption('debug')){
			$output->writeln("Systems Count: ".count($adminUser->dataStore));
			$output->writeln("Processed Count: ".count($processed));
		}



		/**
		 * @var UserEntity $user
		 * @var UserEntity[] $users
		 */
		foreach ($users as $user) {
			if (trim($input->getArgument('timezone')) != 'all') {
				if ($user->getTimeZone() != $input->getArgument('timezone')) {
					continue;
				}
			}


			if($user->getUid() != 'admin'){
				$user->dataStore = [];
				/**
				 * @var UserEntity $adminUser
				 * @var SystemsEntity $system
				 */
				foreach ($adminUser->getDataStore() as $system){
					if($user->getCustomerId() === $system->getCustomerId() or in_array('ROLE_EMPLOYEE', $utils->getInheritedRoles($user->getRoles()))){
						if(empty($user->getMySystems())){
							array_push($user->dataStore, $system);
						}elseif(in_array($system->getId(), $user->getMySystems())){
							array_push($user->dataStore, $system);
						}
					}
				}

			}

			if(empty($user->dataStore)){continue;}

			if($this->getContainer()->getParameter('mailer_enable')) {
				$output->write('Sending email to ' . $user->getName() . ' <' . $user->getEmail() . '>');
				$sent = $mm->sendEmail(
					$user->getEmail(),
					$this->getContainer()->getParameter('site_box_name') . " Critical Alarms!",
					[
						'name' => $user->getName(),
						'dateStart' => $startDate,
						'dateEnd' => $endDate,
						'systems' => $user->dataStore
					],
					':Emails:criticalAlarm.html.twig',
					Swift_Mime_SimpleMessage::PRIORITY_HIGH
				);

				if ($sent > 0) {
					$output->writeln('  ...Email sent');
				} else {
					$output->writeln('  ...Failed to send');
				}
			}else{
				$output->write('Sending email to ' . $user->getName() . ' <' . $user->getEmail() . '>   ');
				$output->writeln('Mailer Disabled. Not sending.');
			}

		}
		/**
		 * @var SystemsDataGeEntity $item
		 */
		foreach ($processed as $key=>$item){
			$item->setCriticalProcessed(true);
			$em->persist($item);
		}
		$em->flush();

		if($input->getOption('debug')){
			$output->writeln("Command Done");
		}
	}
}
