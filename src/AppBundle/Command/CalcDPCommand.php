<?php

namespace AppBundle\Command;

use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use mysqli;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Resources\UtilitiesCommon;

class CalcDPCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
	    $now = new \DateTime('now');
	    $now->sub(new \DateInterval('P1Y'));
	    $year = intval($now->format('Y'));

        $this
            ->setName('app:calcDP')
            ->setDescription('Calculate the missing dew points for Oivision Data')
	        ->addOption('size', ['-s'], InputOption::VALUE_OPTIONAL, '--year 100  the batch size to use. Default is 100.', 100)
	        ->addOption('all', ['-a'], InputOption::VALUE_NONE, 'Debug')
	        ;
		set_time_limit(0);
		ini_set('memory_limit','1G');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$bacthSize = $input->getOption('size');

	    $em = $this->getContainer()->get('doctrine.orm.entity_manager');

	    $utils = new UtilitiesCommon($em, $this->getContainer());

	    $sql = /** @lang dql */
		    "SELECT sdg FROM AppBundle\Entity\SystemsDataGeEntity sdg ";
	    if(!$input->getOption('all')){
	    	$sql .= " WHERE sdg.dp = 0";
	    }

	    $q = $em->createQuery($sql);
		$ires = $q->iterate();

		$i = 0;
		foreach ($ires as $row){
			/**
			 * @var SystemsDataGeEntity $datum
			 */
			$datum = $row[0];
			$datum->setDp($utils->dewPoint($datum->getTf(), $datum->getRh()));
			$datum->setDpdiff($datum->getTf() - $datum->getDp());

			if($datum->getSystem()->getMonitorDp()){
				if($datum->getSystem()->getCriticalDpH() <= $datum->getDp()){
					$datum->setDpc(true);
					$datum->setHasCriticalAlarm(true);
					$datum->setCriticalProcessed(true);
				}elseif($datum->getSystem()->getAlarmDpH() <= $datum->getDp()){
					$datum->setDpa(true);
					$datum->setHasAlarm(true);
					$datum->setAlarmProcessed(true);
				}
			}

			if($datum->getSystem()->getMonitorDpDiff()){
				if($datum->getDpdiff() <= $datum->getSystem()->getCriticalDpDiff()){
					$datum->setDpdc(true);
					$datum->setHasCriticalAlarm(true);
					$datum->setCriticalProcessed(true);
				}elseif($datum->getDpdiff() <= $datum->getSystem()->getAlarmDpDiff()){
					$datum->setDpda(true);
					$datum->setHasAlarm(true);
					$datum->setAlarmProcessed(true);
				}
			}

//			dump($datum);die();

			$output->writeln('Processing ID: '.$datum->getId());
			if (($i % $bacthSize) === 0){
				$em->flush();
				$em->clear();
			}
			++$i;
		}
		$em->flush();

	    $output->writeln("DONE");

    }



}
