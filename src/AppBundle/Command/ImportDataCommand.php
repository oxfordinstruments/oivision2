<?php

namespace AppBundle\Command;

use AppBundle\Entity\ApiKeyEntity;
use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use mysqli;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportDataCommand extends ContainerAwareCommand
{
	private $testing = false;
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:importData')
            ->setDescription('Import of Old Oivision Data')
	        ->addOption('testing', ['-t'], InputOption::VALUE_NONE, 'Do not set remote db flag for data pulled')
	        ;
		set_time_limit(0);
		ini_set('memory_limit','1G');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$this->testing = $input->getOption('testing');

		$output->writeln('Begin importing remote data...');
    	$sysRepo = $this->getContainer()->get('doctrine')->getManager()->getRepository(SystemsEntity::class);
		$systems = $sysRepo->findBy(array('enabled' => true, 'oivOld' => true));
	    /**
	     * @var SystemsEntity $system
	     */
		foreach ($systems as $system){
			$this->importSystemData($system, $output);
//			die('STOP');
		}

	    $output->writeln("Done with import.");

    }

	/**
	 * @param SystemsEntity $system
	 * @throws \Exception
	 */
	private function importSystemData(SystemsEntity $system, OutputInterface $output)
    {
	    $em = $this->getContainer()->get('doctrine')->getManager();
	    $utils = $this->getContainer()->get('app.utils_common');

	    $mysqli = new mysqli(
		    $this->getContainer()->getParameter('remote_database_host'),
		    $this->getContainer()->getParameter('remote_database_user'),
		    $this->getContainer()->getParameter('remote_database_password'),
		    $this->getContainer()->getParameter('remote_database_name')
	    );

	    $sql = "SELECT * FROM `".$system->getMacRdc()."` WHERE new = 0 ;";
	    if(!$result = $mysqli->query($sql)){
		    throw new \Exception($mysqli->error.PHP_EOL);
	    }
	    if($result->num_rows == 0){
	    	return;
	    }
//	    echo sprintf("Importing: %s  %s  Rows: %d",$system->getSystemName(), $system->getMacRdc(), $result->num_rows),PHP_EOL;
		$output->writeln(sprintf("Importing: %s  %s  Rows: %d",$system->getSystemName(), $system->getMacRdc(), $result->num_rows));
	    $rows = [];
	    while ($row = $result->fetch_assoc()){
		    array_push($rows, $row);
	    }


	    $apiRepo = $this->getContainer()->get('doctrine')->getRepository(ApiKeyEntity::class);
	    $apiToken = $apiRepo->findKey('6548admin101317');
	    $tz = new \DateTimeZone('America/Kentucky/Louisville');

	    $completedIds = array();

	    foreach ($rows as $row){
		    $data = new SystemsDataGeEntity();

		    $data->setPulledData(true);
		    $data->setApiKey($apiToken);
		    $data->setMacRdc($system->getMacRdc());
		    $data->setMacOiv($system->getMacRdc());
		    $data->setInStorage($system->getInStorage());
		    $data->setSystem($system);
		    $data->setHe(floatval($row['He']));
		    $data->setVp(floatval($row['HeP']));
		    $data->setTf(floatval($row['Tf']));
		    $data->setRh(floatval($row['Rh']));
		    $data->setWf(floatval($row['Wf']));
		    $data->setWt(floatval($row['Wtf']));
		    $data->setQ($this->oz($row['FieldOn']));
		    $data->setCmp($this->oz($row['CompOn']));
		    $data->setDp($utils->dewPoint($data->getTf(), $data->getRh()));
		    $data->setDpdiff($data->getTf() - $data->getDp());

		    $insertDate = new \DateTime($row['insertDate'], $tz);
		    $insertDate->setTimezone(new \DateTimeZone('UTC'));
		    $data->setTimestamp($insertDate);

		    $data = $utils->checkDataGeForAlarms($data, $system);

//			$data->setAlarmProcessed(true);
//			$data->setCriticalProcessed(true);

		    if(!$this->testing){
			    $sql = "UPDATE `".$system->getMacRdc()."` SET new = 1 WHERE `index` = ".$row['index'].";";
			    if(!$result = $mysqli->query($sql)){
				    throw new \Exception($mysqli->error.PHP_EOL);
			    }else{
				    $em->persist($data);
				    $em->flush();
				    $em->detach($data);
				    array_push($completedIds, intval($row['index']));
			    }
		    }else{
			    $em->persist($data);
			    $em->flush();
			    $em->detach($data);
			    array_push($completedIds, intval($row['index']));
		    }


		    unset($data);
	    }
    }

	private function yn($string)
	{
		if(strtolower($string) == 'y'){
			return true;
		}
		return false;
	}

	private function oz($string)
	{
		if(strtolower($string) == '1'){
			return true;
		}
		return false;
	}
}
