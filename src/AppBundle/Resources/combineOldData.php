<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/12/2017
 * Time: 8:39 PM
 */

namespace AppBundle\Resources;



use AppBundle\Entity\ApiKeyEntity;
use AppBundle\Entity\CustomerEntity;
use AppBundle\Entity\SystemsDataGeEntity;
use League\Flysystem\Exception;
use mysqli;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Entity\SystemsEntity;
use League\Flysystem\Adapter\Local as Adapter;
class combineOldData
{

	private $em;

	private $minYear;

	private  $container;

	public function __construct(ContainerInterface $container, int $minYear)
	{
		$this->container = $container;
		$this->minYear = $minYear;

	}

	public function __destruct()
	{
		unset($this->em);
		unset($this->minYear);
	}

	public function clean()
	{
		unset($this->em);
		unset($this->minYear);
	}


	/**
	 * @param OutputInterface $output
	 * @param array $site
	 * @param int $reset
	 * @param int $all
	 * @return array
	 * @throws \Exception
	 */
	public function importData(OutputInterface $output, array $site, int $reset, int $all)
	{

		set_time_limit(0);
		ini_set('memory_limit','500m');

		$completedMacTables = $this->getCompletedSites($output);

		$utils = $this->container->get('app.utils_common');

		$mysqli = new mysqli(
			$this->container->getParameter('remote_database_host'),
			$this->container->getParameter('remote_database_user'),
			$this->container->getParameter('remote_database_password'),
			$this->container->getParameter('remote_database_name')
		);

		$apiRepo = $this->container->get('doctrine')->getRepository(ApiKeyEntity::class);
		$apiToken = $apiRepo->findKey('6548admin101317');
		$tz = new \DateTimeZone('America/Kentucky/Louisville');

		$sysRepo = $this->container->get('doctrine')->getRepository(SystemsEntity::class);
		$system = $sysRepo->findByMacRdc($site['mac']);
		$em = $this->container->get('doctrine')->getManager();
		$sql = "SELECT * FROM `".$site['mac']."` WHERE YEAR(insertDate) >= ".$this->minYear;
		if($reset == 0 or $all == 0 ){
			$sql .= " AND new = 0;";
		}else{
			$sql .= ";";
		}
		if(!$result = $mysqli->query($sql)){
			throw new \Exception($mysqli->error.PHP_EOL);
		}

		$output->writeln("Importing System: ".$site['site_name']);
		$output->writeln("Memory: ".$utils->humanReadable(memory_get_usage()));
		$output->writeln("Number of rows: ".$result->num_rows);

		$rows = [];
		while ($row = $result->fetch_assoc()){
			array_push($rows, $row);
		}

		$completedIds = array();
		$y = 0;
		$z = 0;
		$time_start = microtime(true);
		$row_start = microtime(true);
		foreach ($rows as $row){
			$data = new SystemsDataGeEntity();

			if($y >= 500){
				$row_end = microtime(true);
				$row_exec_time = ($row_end - $row_start);
				$cur_execution_time = ($row_end - $time_start)/60;
				$output->writeln("Memory: ".$utils->humanReadable(memory_get_usage())." Row: ".$z." Time: ".round($row_exec_time,2)." Secs  Elapsed Time: ".round($cur_execution_time, 2)." Mins");
				$y = 0;
				$row_start = microtime(true);

			}
			$data->setPulledData(true);
			$data->setApiKey($apiToken);
			$data->setMacRdc($site['mac']);
			$data->setMacOiv($site['mac']);
			$data->setInStorage($this->yn($site['Storage']));
			$data->setSystem($system[0]);
			$data->setHe(floatval($row['He']));
			$data->setVp(floatval($row['HeP']));
			$data->setTf(floatval($row['Tf']));
			$data->setRh(floatval($row['Rh']));
			$data->setWf(floatval($row['Wf']));
			$data->setWt(floatval($row['Wtf']));
			$data->setQ($this->oz($row['FieldOn']));
			$data->setCmp($this->oz($row['CompOn']));
			$data->setDp($utils->dewPoint($data->getTf(), $data->getRh()));

			$insertDate = new \DateTime($row['insertDate'], $tz);
			$insertDate->setTimezone(new \DateTimeZone('UTC'));
			$data->setTimestamp($insertDate);

			$data = $utils->checkDataGeForAlarms($data, $system[0]);

//			$data->setAlarmProcessed(true);
//			$data->setCriticalProcessed(true);

			$em->persist($data);
			$em->flush();
			$em->detach($data);

			unset($data);
			$y++;
			$z++;
			array_push($completedIds, intval($row['index']));
		}
		$result->close();
		$mysqli->close();
		$time_end = microtime(true);
		$execution_time = ($time_end - $time_start)/60;
		$output->writeln('Data Execution Time: '.round($execution_time, 2).' Mins');
		array_push($completedMacTables, $site['mac']);

		$em->clear();

		$this->setCompletedSites($completedMacTables);
		$output->writeln("Completed Import");
		$output->writeln('');
		$this->setNotNew($site['mac'], $completedIds);
		return [$completedMacTables];
	}

	/**
	 * @param $macTable
	 * @param $ids
	 * @throws \Exception
	 */
	public function setNotNew($macTable, $ids){

		if(count($ids) == 0){
			return;
		}

		$mysqli = new mysqli(
			$this->container->getParameter('remote_database_host'),
			$this->container->getParameter('remote_database_user'),
			$this->container->getParameter('remote_database_password'),
			$this->container->getParameter('remote_database_name')
		);

		$sql = "UPDATE `".$macTable."` SET new = 1 WHERE `index` IN(".implode(',',$ids).");";
		if(!$result = $mysqli->query($sql)){
			//dump($sql);
			//dump($ids);
			throw new \Exception($mysqli->error.PHP_EOL);
		}
		$mysqli->close();
	}

	public function createSystems(OutputInterface $output, array $mmSites)
	{
		$custRepo = $em = $this->container->get('doctrine')->getManager()->getRepository(CustomerEntity::class);
		/**
		 * @var CustomerEntity $cust
		 */
		$cust = $custRepo->findOneBy(['name' => 'OiHealthcare']);

		foreach ($mmSites as $key => $site) {
			$output->writeln("Created System: ".$site['site_name']);

			$system = new SystemsEntity();

			$system->setEnabled($this->yn($site['enabled']));
			$system->setRegistered(true);

			$system->setMacRdc(strtolower($site['mac']));
			$system->setSystemId('MR' . $site['site_id']);
			$system->setSystemName($site['site_name']);
			$system->setSystemAddress($site['site_address']);
			$system->setSystemCity($site['site_city']);
			$system->setSystemState(strtoupper($site['site_st']));
			$system->setSystemZip($site['site_zip']);
			$system->setCustomerId($cust);

			$system->setMonitorHe($this->yn($site['He']));
			$system->setMonitorHeb($this->yn($site['HeB']));
			$system->setMonitorVp($this->yn($site['HeP']));
			$system->setMonitorTf($this->yn($site['Tf']));
			$system->setMonitorRh($this->yn($site['Rh']));
			$system->setMonitorWf($this->yn($site['Wf']));
			$system->setMonitorWt($this->yn($site['Wtf']));
			$system->setMonitorQ($this->yn($site['FieldOn']));
			$system->setMonitorCmp($this->yn($site['CompOn']));

			$system->setAvgHe(true);
			$system->setAvgVp(true);

			$system->setInStorage($this->yn($site['Storage']));
			$system->setUseGlobalAlarms($this->yn($site['use_global_alarm']));
			$system->setNotes('Added from OiVision 1.x'. PHP_EOL . $site['notes']);

			$system->setAlarmHeL(floatval($site['alarm_he']));
			$system->setAlarmHebH(floatval($site['alarm_heb']));
			$system->setAlarmVpH(floatval($site['alarm_vph']));
			$system->setAlarmVpL(floatval($site['alarm_vpl']));
			$system->setAlarmTfH(floatval($site['alarm_rth']));
			$system->setAlarmTfL(floatval($site['alarm_rtl']));
			$system->setAlarmRhH(floatval($site['alarm_rhh']));
			$system->setAlarmRhL(floatval($site['alarm_rhl']));
			$system->setAlarmWfH(floatval($site['alarm_wfh']));
			$system->setAlarmWfL(floatval($site['alarm_wfl']));
			$system->setAlarmWtH(floatval($site['alarm_wth']));
			$system->setAlarmWtL(floatval($site['alarm_wtl']));

			$system->setAlarmStorageHeL(floatval($site['alarm_she']));
			$system->setAlarmStorageVpH(floatval($site['alarm_svph']));
			$system->setAlarmStorageVpL(floatval($site['alarm_svpl']));

			$system->setCriticalHeL(floatval($site['alarm_hec']));
			$system->setCriticalHebH(floatval($site['alarm_heb']) + 2);
			$system->setCriticalVpH(floatval($site['alarm_vphc']));
			$system->setCriticalVpL(floatval($site['alarm_vplc']));
			$system->setCriticalTfH(floatval($site['alarm_rthc']));
			$system->setCriticalTfL(floatval($site['alarm_rtlc']));
			$system->setCriticalRhH(floatval($site['alarm_rhhc']));
			$system->setCriticalRhL(floatval($site['alarm_rhlc']));
			$system->setCriticalWfH(floatval($site['alarm_wfhc']));
			$system->setCriticalWfL(floatval($site['alarm_wflc']));
			$system->setCriticalWtH(floatval($site['alarm_wthc']));
			$system->setCriticalWtL(floatval($site['alarm_wtlc']));

			$system->setCriticalStorageHeL(floatval($site['alarm_shec']));
			$system->setCriticalStorageVpH(floatval($site['alarm_svphc']));
			$system->setCriticalStorageVpL(floatval($site['alarm_svplc']));

			$system->setOffsetHe($this->container->getParameter('offsetHe'));
			$system->setOffsetRh($this->container->getParameter('offsetRh'));
			$system->setOffsetTc($this->container->getParameter('offsetTc'));
			$system->setOffsetVp($this->container->getParameter('offsetVp'));
			$system->setOffsetWf($this->container->getParameter('offsetWf'));
			$system->setOffsetWtf($this->container->getParameter('offsetWtf'));

			$system->setOivOld(true);

			$em = $this->container->get('doctrine')->getManager();
			$em->persist($system);
			$em->flush();

			unset($system);
		}

		return null;
	}

	private function yn($string)
	{
		if(strtolower($string) == 'y'){
			return true;
		}
		return false;
	}

	private function oz($string)
	{
		if(strtolower($string) == '1'){
			return true;
		}
		return false;
	}

	public function getCompletedSites(OutputInterface $output)
	{
		if(!file_exists($this->container->getParameter('app.temp_dir').'/combineOldData.json')){
			$output->writeln('Creating combineOldData.json');
			file_put_contents($this->container->getParameter('app.temp_dir').'/combineOldData.json', json_encode(array()));
			//return json_decode('[]');
		}
		$fs = new \League\Flysystem\Filesystem(new Adapter($this->container->getParameter('app.temp_dir')));
		return json_decode($fs->read('combineOldData.json'), true);
	}

	public function getCompletedSitesString(OutputInterface $output)
	{
		$cs = $this->getCompletedSites($output);
		return '"'.implode('","', $cs).'"';
	}

	public function setCompletedSites(array $combinedSites)
	{
		$fs = new \League\Flysystem\Filesystem(new Adapter($this->container->getParameter('app.temp_dir')));
		$fs->put('combineOldData.json', json_encode($combinedSites, true));
	}

	/**
	 * @param int $flag
	 * @throws \Exception
	 */
	public function markAll($flag = 1)
	{
		$mysqli = new mysqli(
			$this->container->getParameter('remote_database_host'),
			$this->container->getParameter('remote_database_user'),
			$this->container->getParameter('remote_database_password'),
			$this->container->getParameter('remote_database_name')
		);

		$sql = "SELECT mac FROM sites;";
		if(!$result = $mysqli->query($sql)){
			die($mysqli->error);
		}
		$mmSites = [];
		while ($row = $result->fetch_assoc()) {
			array_push($mmSites, $row);
		}

		foreach ($mmSites as $site){
			$sql = "UPDATE `".$site['mac']."` SET new = ".$flag.";";
			if(!$result = $mysqli->query($sql)){
				//dump($sql);
				throw new \Exception($mysqli->error.PHP_EOL);
			}
		}
	}
}
