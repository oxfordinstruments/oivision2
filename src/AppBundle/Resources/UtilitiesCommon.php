<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 2:57 PM
 */

namespace AppBundle\Resources;


use AppBundle\Entity\ApiKeyEntity;
use AppBundle\Entity\GlobalAlarmSettingsEntity;
use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManager;
use Nelmio\Alice\Persister\Doctrine;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class UtilitiesCommon
{
	/**
	 * @var EntityManager
	 */
	private $em;
	/**
	 * @var Container
	 */
	private $container;

	public function __construct(EntityManager $entityManager, ContainerInterface $container)
	{
		$this->em = $entityManager;
		$this->container = $container;
	}

	/**
	 * Used to get a random string of x length
	 *
	 * @param $length
	 * @return string
	 */
	public function getToken($length){
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet) - 1;
		for ($i=0; $i < $length; $i++) {
			$token .= $codeAlphabet[$this->crypto_rand_secure(0, $max)];
		}
		return $token;
	}

	/**
	 * Used in getToken function
	 *
	 * @param $min
	 * @param $max
	 * @return mixed
	 */
	private function crypto_rand_secure($min, $max){
		$range = $max - $min;
		if ($range < 1) return $min; // not so random...
		$log = ceil(log($range, 2));
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd >= $range);
		return $min + $rnd;
	}

	/**
	 * Get timesone name abbreviation. e.g. EST
	 * @param string $tz
	 * @return string
	 */
	public function tz_abbreviation($tz = 'UTC'){
		$dateTime = new DateTime();
		$dateTime->setTimeZone(new DateTimeZone($tz));
		return $dateTime->format('T');
	}

	/**
	 * Get timezone name gmt offset. e.g. -0500
	 * @param string $tz
	 * @return string
	 */
	public function tz_offset($tz = 'UTC'){
		$dateTime = new DateTime();
		$dateTime->setTimeZone(new DateTimeZone($tz));
		return $dateTime->format('P');
	}

	/**
	 * Get timezone offset in seconds. e.g. -18000
	 * @param string $tz
	 * @return int
	 */
	public function tz_offset_sec($tz = 'UTC'){
		if(is_null($tz)){
			$tz = 'UTC';
		}
		$timezone = new DateTimeZone($tz);
		return $timezone->getOffset(new DateTime);
	}

	/**
	 * @return string
	 */
	public function generate_uuid() {
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			mt_rand( 0, 0xffff ),
			mt_rand( 0, 0x0fff ) | 0x4000,
			mt_rand( 0, 0x3fff ) | 0x8000,
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}

	/**
	 * @param SystemsEntity $systemsEntity
	 * @return SystemsEntity
	 */
	public function updateToCurrentGlobalAlarms(SystemsEntity $systemsEntity)
	{
		$sys = $systemsEntity;
		if($sys->getUseGlobalAlarms()) {
			$garRepo = $this->em->getRepository(GlobalAlarmSettingsEntity::class);
			$garData = $garRepo->findLastId();
			$garKeys = array_keys((array)$garData[0]);
			foreach ($garKeys as $key => $val) {
				$v1 = preg_replace('/\x00.*\x00/', '', $val);
				$garKeys[$key] = $v1;
				switch ($v1) {
					case 'id':
						unset($garKeys[$key]);
						break;
					case 'uid':
						unset($garKeys[$key]);
						break;
					case 'timestamp':
						unset($garKeys[$key]);
						break;
				}
			}
			foreach ($garKeys as $value) {
				$o = $garData[0]->$value;
				$sys->$value = $o;
			}
		}
		/** @var SystemsEntity $sys */
		return $sys;
	}

	/**
	 * @param $str
	 * @param array $noStrip
	 * @return mixed|string
	 */
	public function camelCase($str, array $noStrip = [])
	{
		// non-alpha and non-numeric characters become spaces
		$str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
		$str = trim($str);
		// uppercase the first character of each word
		$str = ucwords($str);
		$str = str_replace(" ", "", $str);
		$str = lcfirst($str);

		return $str;
	}

	/**
	 * @param SystemsDataGeEntity $data
	 * @param SystemsEntity $system
	 * @return SystemsDataGeEntity
	 */
	public function checkDataGeForAlarms(SystemsDataGeEntity $data, SystemsEntity $system)
	{
		if($system->getUseGlobalAlarms()) {
			$gasRepo = $this->em->getRepository(GlobalAlarmSettingsEntity::class);
			$gas = $gasRepo->findLastId();
			$ad = $gas[0];
		}else{
			$ad = $system;
		}


		if($system->getMonitorTf()){
			if($ad->getCriticalTfH() <= $data->getTf() or $data->getTf() <= $ad->getCriticalTfL()){
				$data->setTfc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($ad->getAlarmTfH() <= $data->getTf() or $data->getTf() <= $ad->getAlarmTfL()){
				$data->setTfa(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorRh()){
			if($ad->getCriticalRhH() <= $data->getRh() or $data->getRh() <= $ad->getCriticalRhL()){
				$data->setRhc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($ad->getAlarmRhH() <= $data->getRh() or $data->getRh() <= $ad->getAlarmRhL()){
				$data->setRha(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorDp()){
			if($ad->getCriticalDpH() <= $data->getDp()){
				$data->setDpc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($ad->getAlarmDpH() <= $data->getDp()){
				$data->setDpa(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorDpDiff()){
			if($data->getDpdiff() <= $ad->getCriticalDpDiff()){
				$data->setDpdc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($data->getDpdiff() <= $ad->getAlarmDpDiff()){
				$data->setDpda(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorWt()){
			if($ad->getCriticalWtH() <= $data->getWt() or $data->getWt() <= $ad->getCriticalWtL()){
				$data->setWtc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($ad->getAlarmWtH() <= $data->getWt() or $data->getWt() <= $ad->getAlarmWtL()){
				$data->setWta(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorWf()){
			if($ad->getCriticalWfH() <= $data->getWf() or $data->getWf() <= $ad->getCriticalWfL()){
				$data->setWfc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($ad->getAlarmWfH() <= $data->getWf() or $data->getWf() <= $ad->getAlarmWfL()){
				$data->setWfa(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorVp()){
			if($ad->getCriticalVpH() <= $data->getVp() or $data->getVp() <= $ad->getCriticalVpL()){
				$data->setVpc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($ad->getAlarmVpH() <= $data->getVp() or $data->getVp() <= $ad->getAlarmVpL()){
				$data->setVpa(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorHe()){
			if($data->getHe() <= $ad->getCriticalHeL()){
				$data->setHec(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($data->getHe() <= $ad->getAlarmHeL()){
				$data->setHea(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}


		if($system->getMonitorVp() and $system->getInStorage()){
			if($ad->getCriticalStorageVpH() <= $data->getVp() or $data->getVp() <= $ad->getCriticalStorageVpL()){
				$data->setVpc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($ad->getAlarmStorageVpH() <= $data->getVp() or $data->getVp() <= $ad->getAlarmStorageVpL()){
				$data->setVpa(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorHe() and $system->getInStorage()){
			if($data->getHe() <= $ad->getCriticalStorageHeL()){
				$data->setHec(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}elseif($data->getHe() <= $ad->getAlarmStorageHeL()){
				$data->setHea(true);
				$data->setHasAlarm(true);
				$data->setAlarmProcessed(false);
			}
		}

		if($system->getMonitorCmp()){
			if(!$data->getCmp()){
				$data->setCmpc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}
		}

		if($system->getMonitorQ() and !$system->getInStorage()){
			if(!$data->getQ()){
				$data->setQc(true);
				$data->setHasCriticalAlarm(true);
				$data->setCriticalProcessed(false);
			}
		}


		return $data;
	}

	/**
	 * @param $string
	 * @return string
	 */
	public function cleanString($string)
	{
		return preg_replace('/[^A-Za-z0-9\-_ ]/', '', $string);
	}

	public function humanReadable($size)
	{
		$unit=array('b','kb','mb','gb','tb','pb');
		return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
	}

	public function getTimeZonesList()
	{
		return [
			"Eastern" => "America/New_York",
			"Central" => "America/Chicago",
			"Mountain" => "America/Denver",
			"Mountain no DST" => "America/Phoenix",
			"Pacific" => "America/Los_Angeles",
			"Alaska" => "America/Anchorage",
			"Hawaii" => "America/Adak",
			"Hawaii no DST" => "Pacific/Honolulu",
		];
	}

	public function getInheritedRoles(array $_roles)
	{
		$hierarchy = $this->container->getParameter('security.role_hierarchy.roles');
		$roleHierarchy = new RoleHierarchy($hierarchy);
		$roles = [];
		foreach ($_roles as $_role) {
			foreach ($roleHierarchy->getReachableRoles([new Role($_role)]) as $tmp) {
				array_push($roles, $tmp);
			}

		}
		$roles = array_map(function(Role $role) { return $role->getRole(); }, $roles);
		return array_unique($roles);
	}

	public function minusXHourDate(int $hours = 1)
	{
		$date = new \DateTime();
		$date->modify('-'.$hours.' hour');
		$date->modify('-1 minute');
		return $date;
	}

	public function minusXDayDate(int $days = 1)
	{
		$date = new \DateTime();
		$date->modify('-'.$days.' day');
		return $date;
	}

	/**
	 * $temp in fahrenheit unless $celsius is true
	 * $humidity in percent
	 * $fahrenheit set true will return dew point in fahrenheit
	 *
	 * @param float $temp
	 * @param int $humidity
	 * @param bool $celsius
	 * @return float
	 */
	public function dewPoint($temp, $humidity, $celsius=false)
	{
		if(!$celsius){
			$temp = $this->fahrenheitToCelsius($temp);
		}
		$dewpoint = round(((pow(($humidity/100), 0.125))*(112+0.9*$temp)+(0.1*$temp)-112),3);

		if(!$celsius){
			return $this->celsiusToFahrenheit($dewpoint);
		}else{
			return $dewpoint;
		}
	}

	/**
	 * @param float $celsius
	 * @return float
	 */
	public function celsiusToFahrenheit(float $celsius) : float
	{
		return $celsius * 1.8 + 32;
	}

	/**
	 * @param float $fahrenheit
	 * @return float
	 */
	public function fahrenheitToCelsius(float $fahrenheit) : float
	{
		return ($fahrenheit - 32) / 1.8;
	}
}