<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 3/7/2018
 * Time: 10:56 AM
 */

namespace AppBundle\Resources;


use RuntimeException;
use Symfony\Component\Console\Output\StreamOutput;

class StreamedOutput extends StreamOutput
{
	protected function doWrite($message, $newline)
	{
		if (
			false === @fwrite($this->getStream(), $message) ||
			(
				$newline &&
				(false === @fwrite($this->getStream(), PHP_EOL))
			)
		) {
			throw new RuntimeException('Unable to write output.');
		}

		echo $message;

		ob_flush();
		flush();
	}
}