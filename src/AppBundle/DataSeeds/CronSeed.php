<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:46 PM
 */

namespace AppBundle\DataSeeds;

use Doctrine\ORM\EntityManager;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MfgSeed
 * @package AppBundle\DataSeeds
 */
class CronSeed extends Seed
{

	protected function configure()
	{
		//The seed won't load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName('cron');

		parent::configure();
	}

	/**
	 * Load a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Doctrine\DBAL\DBALException
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 */
	public function load(InputInterface $input, OutputInterface $output)
	{
		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();


		$jobs = [
			[
				'name' => 'checkAlarms1',
				'command' => 'app:checkAlarms all morning',
				'schedule' => '7 12 * * *',
				'description' => 'Check for alarms morning',
				'enabled' => true
			],
			[
				'name' => 'checkAlarms2',
				'command' => 'app:checkAlarms all afternoon',
				'schedule' => '7 20 * * *',
				'description' => 'Check for alarms afternoon',
				'enabled' => true
			],
			[
				'name' => 'checkCriticals',
				'command' => 'app:checkCriticals all',
				'schedule' => '6 * * * *',
				'description' => 'Check for critical alarms',
				'enabled' => true
			],
			[
				'name' => 'importData',
				'command' => 'app:importData',
				'schedule' => '*/30 * * * *',
				'description' => 'Import data from old OiVision',
				'enabled' => false
			],
			[
				'name' => 'getSites',
				'command' => 'app:get_sites',
				'schedule' => '12 */6 * * *',
				'description' => 'Import systems from old OiVision',
				'enabled' => true
			],
			[
				'name' => 'getData',
				'command' => 'app:get_data',
				'schedule' => '*/30 * * * *',
				'description' => 'Import data from old OiVision',
				'enabled' => true
			]
		];

		foreach ($jobs as $job) {

			$sql = "INSERT INTO cron_job (name, command, schedule, description, enabled) VALUES (:name, :command, :schedule, :description, :enabled)";

			$stmt = $this->manager->getConnection()->prepare($sql);
			$stmt->bindValue(':name', $job['name']);
			$stmt->bindValue(':command', $job['command']);
			$stmt->bindValue(':schedule', $job['schedule']);
			$stmt->bindValue(':description', $job['description']);
			$stmt->bindValue(':enabled', $job['enabled']);
			$stmt->execute();

		}

		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Exception
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		/**
		 * @var EntityManager $em
		 */
		$em = $this->container->get('doctrine')->getManager();
		$em->getConnection()->exec('DELETE FROM cron_report');
		$em->getConnection()->exec('DELETE FROM cron_job');


	}

	/**
	 * @return int
	 */
	public function getOrder(): int {
		return 0;
	}

}