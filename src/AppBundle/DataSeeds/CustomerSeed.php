<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:46 PM
 */

namespace AppBundle\DataSeeds;

use AppBundle\Entity\CustomerEntity;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MfgSeed
 * @package AppBundle\DataSeeds
 */
class CustomerSeed extends Seed
{

	protected function configure()
	{
		//The seed won't load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName('customer');

		parent::configure();
	}

	/**
	 * Load a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Doctrine\DBAL\DBALException
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 */
	public function load(InputInterface $input, OutputInterface $output)
	{
		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();


		$jobs = [
			[
				'enabled' => true,
				'name' => 'OiHealthcare',
				'address' => '1027 SW 30th Ave',
				'city' => 'Deerfield Beach',
				'state' => 'FL',
				'zip' => '33442',
				'phone' => '888-673-5151',
				'email' => '',
				'comment' => ''
			],
			[
				'enabled' => true,
				'name' => 'Admin',
				'address' => '1027 SW 30th Ave',
				'city' => 'Deerfield Beach',
				'state' => 'FL',
				'zip' => '33442',
				'phone' => '888-673-5151',
				'email' => '',
				'comment' => ''
			]
		];

		foreach ($jobs as $key => $job) {

			$sql = "INSERT INTO customer (enabled, name, address, city, state, zip, phone, email, comment) VALUES (:enabled, :name, :address, :city, :state, :zip, :phone, :email, :comment);";

			$stmt = $this->manager->getConnection()->prepare($sql);
			//$stmt->bindValue(':id', $key);
			$stmt->bindValue(':enabled', $job['enabled']);
			$stmt->bindValue(':name', $job['name']);
			$stmt->bindValue(':address', $job['address']);
			$stmt->bindValue(':city', $job['city']);
			$stmt->bindValue(':state', $job['state']);
			$stmt->bindValue(':zip', $job['zip']);
			$stmt->bindValue(':phone', $job['phone']);
			$stmt->bindValue(':email', $job['email']);
			$stmt->bindValue(':comment', $job['comment']);

			$stmt->execute();

		}

		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Exception
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		$em = $this->container->get('doctrine')->getManager();
		$className = $em->getClassMetadata(CustomerEntity::class)->getName();
		$em->createQuery('DELETE FROM '.$className)->execute();
	}

	/**
	 * @return int
	 */
	public function getOrder(): int {
		return 1;
	}

}