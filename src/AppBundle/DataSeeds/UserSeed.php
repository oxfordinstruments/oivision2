<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:46 PM
 */

namespace AppBundle\DataSeeds;

use AppBundle\Entity\ApiKeyEntity;
use AppBundle\Entity\CustomerEntity;
use AppBundle\Entity\UserEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MfgSeed
 * @package AppBundle\DataSeeds
 */
class UserSeed extends Seed
{

	protected function configure()
	{
		//The seed won"t load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName("user");

		parent::configure();
	}

	/**
	 * Load a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 * @throws \Doctrine\DBAL\DBALException
	 */
	public function load(InputInterface $input, OutputInterface $output)
	{
		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();

		$repoCust = $this->doctrine->getRepository(CustomerEntity::class);

		/**
		 * The roles values should be single quotes on outside
		 */
		$users = [
			[
				'uid' => 'admin',
				'password' => '$2y$13$ZFj1WJB5ZHD7jfI0h4NwYucaLzSx0tNNLuUKjpQaMMU1j6vusmZPW',
				'name' => 'Admin Istrator',
				'email' => 'admin@me.com',
				'roles' => '["ROLE_ADMIN","ROLE_USER"]',
				'cid' => 'Admin'
			],
			[
				'uid' => 'oivision',
				'password' => '$2y$13$ZFj1WJB5ZHD7jfI0h4NwYucaLzSx0tNNLuUKjpQaMMU1j6vusmZPW',
				'name' => 'OiVision Admin',
				'email' => 'oivision@me.com',
				'roles' => '["ROLE_ADMIN","ROLE_USER"]',
				'cid' => 'OiHealthcare'
			]
		];

		foreach ($users as $key=>$user){

			$cust = $repoCust->findOneBy(array('name' => $user['cid']));

			$sql = 'INSERT INTO `user` (`uid`, `enabled`, `registered`, `name`, `address`, `city`, `state`, `zip`, `phone`, `email`, `comment`, `password`, `roles`, `password_change_needed`, `password_change_date`, `customer_id_id`, `email_alarm_notify`, `email_critical_notify`, `time_zone`) 
				VALUES (:uid, 1, 1, :name, "123 Nowhere St", "Pekin", "IN", "47164", "123-123-1234", :email, NULL, :password, :roles, 1, NULL, :cust, 1, 1, "America/New_York");';

			$stmt = $this->manager->getConnection()->prepare($sql);
			$stmt->bindValue(':uid', $user['uid']);
			$stmt->bindValue(':password', $user['password']);
			$stmt->bindValue(':name', $user['name']);
			$stmt->bindValue(':email', $user['email']);
			$stmt->bindValue(':roles', $user['roles']);
			$stmt->bindValue(':cust', $cust->getId());
			$stmt->execute();
		}


		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Exception
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		$em = $this->container->get('doctrine')->getManager();
		$className = $em->getClassMetadata(UserEntity::class)->getName();
		$em->createQuery('DELETE FROM '.$className)->execute();
	}

	/**
	 * @return int
	 */
	public function getOrder(): int {
		return 2;
	}

}