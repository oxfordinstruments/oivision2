<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:46 PM
 */

namespace AppBundle\DataSeeds;

use AppBundle\Entity\MfgEntity;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MfgSeed
 * @package AppBundle\DataSeeds
 */
class MfgSeed extends Seed
{

	protected function configure()
	{
		//The seed won't load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName('mfg');

		parent::configure();
	}

	/**
	 * Load a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 */
	public function load(InputInterface $input, OutputInterface $output)
	{
		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();


		$mfgs = [
			'GE' => 'General Electric Healthcare',
			'Philips' => 'Philips Healthcare',
			'Siemens' => 'Siemens Healthcare',
			'Toshiba' => 'Toshiba Medical Systems',
			'Hitachi' => 'Hitachi Medical Corporation'
		];

		$repo = $this->doctrine->getRepository('AppBundle:MfgEntity');


		foreach ($mfgs as $abv => $name) {

			if($repo->findOneBy(array('abv' => $abv))) {
				continue;
			}

			$em = new MfgEntity();

			$em->setAbv($abv);
			$em->setName($name);

			//Doctrine manager is also available
			$this->manager->persist($em);

			$this->manager->flush();
		}

		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Exception
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		$em = $this->container->get('doctrine')->getManager();
		$className = $em->getClassMetadata(MfgEntity::class)->getName();
		$em->createQuery('DELETE FROM '.$className)->execute();
	}

	/**
	 * @return int
	 */
	public function getOrder(): int {
		return 0;
	}
}