<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:46 PM
 */

namespace AppBundle\DataSeeds;

use AppBundle\Entity\ApiKeyEntity;
use AppBundle\Entity\CustomerEntity;
use Doctrine\ORM\Query\ResultSetMapping;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MfgSeed
 * @package AppBundle\DataSeeds
 */
class ApiKeySeed extends Seed
{

	protected function configure()
	{
		//The seed won't load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName('apikey');

		parent::configure();
	}

	/**
	 * Load a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 * @throws \Doctrine\ORM\ORMException
	 */
	public function load(InputInterface $input, OutputInterface $output)
	{
		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();


		$apikeys = [
			'admin' => [
				'token' => '6548admin101317',
				'cid' => 'Admin',
				'created' => '2000-01-01 12:00:00',
				'expire' => '2100-01-01 12:00:00',
				//'mac_rdc' => '00-00-00-00-00-00'
			],
			'oivision' => [
				'token' => '7549oivision105685',
				'cid' => 'OiHealthcare',
				'created' => '2000-01-01 12:00:00',
				'expire' => '2100-01-01 12:00:00',
				//'mac_rdc' => '00-00-00-00-00-00'

			]
		];

		$repoAke = $this->doctrine->getRepository(ApiKeyEntity::class);
		$repoCust = $this->doctrine->getRepository(CustomerEntity::class);

		foreach ($apikeys as $key) {

			if($repoAke->findOneBy(array('token' => $key['token']))) {
				continue;
			}

			$cust = $repoCust->findOneBy(array('name' => $key['cid']));

			$ake = new ApiKeyEntity();

			$ake->setCustomerId($cust);
			$ake->setCreated($key['created']);
			$ake->setExpire($key['expire']);
			$ake->setToken($key['token']);
			//$ake->setMacRdc($key['mac_rdc']);

			//Doctrine manager is also available
			$this->manager->persist($ake);

			$this->manager->flush();
		}

		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Exception
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		$em = $this->container->get('doctrine')->getManager();
		$className = $em->getClassMetadata(ApiKeyEntity::class)->getName();
		$em->createQuery('DELETE FROM '.$className)->execute();
	}

	/**
	 * @return int
	 */
	public function getOrder(): int {
		return 2;
	}
}