<?php

namespace AppBundle\DataSeeds;


use AppBundle\Entity\LdapGroupsEntity;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class LdapGroupsSeed
 * @package AppBundle\DataSeeds
 */
class LdapGroupsSeed extends Seed
{

	protected function configure()
	{
		//The seed won't load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName('ldapgroups');

		parent::configure();
	}

	/**
	 * Load a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 */
	public function load(InputInterface $input, OutputInterface $output)
	{
		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();


		$groups = [
			1202 => ["ROLE_USER","ROLE_MANAGER_USER"],
			1204 => ["ROLE_USER"],
			1103 => ["ROLE_USER","ROLE_EMPLOYEE","ROLE_ADMIN"],
			1102 => ["ROLE_USER","ROLE_EMPLOYEE","ROLE_MANAGER_EMPLOYEE"],
			1101 => ["ROLE_USER","ROLE_EMPLOYEE"],
		];

		$repo = $this->doctrine->getRepository(LdapGroupsEntity::class);


		foreach ($groups as $gid => $roles) {

			if($repo->findOneBy(array('gid' => $gid))) {
				continue;
			}

			$em = new LdapGroupsEntity();

			$em->setGid($gid);
			$em->setRoles(json_encode($roles));

			//Doctrine manager is also available
			$this->manager->persist($em);

			$this->manager->flush();
		}

		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Exception
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		$em = $this->container->get('doctrine')->getManager();
		$className = $em->getClassMetadata(LdapGroupsEntity::class)->getName();
		$em->createQuery('DELETE FROM '.$className)->execute();
	}

	/**
	 * @return int
	 */
	public function getOrder(): int {
		return 0;
	}
}