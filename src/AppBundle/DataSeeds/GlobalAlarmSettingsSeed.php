<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:46 PM
 */

namespace AppBundle\DataSeeds;

use AppBundle\Entity\GlobalAlarmSettingsEntity;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GlobalAlarmSettingsSeed
 * @package AppBundle\DataSeeds
 */
class GlobalAlarmSettingsSeed extends Seed
{

	protected function configure()
	{
		//The seed won't load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName('globalAlarmSettings');

		parent::configure();
	}

	/**
	 * Load a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 */
	public function load(InputInterface $input, OutputInterface $output)
	{
		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();

		//Access doctrine through $this->doctrine
		$repo = $this->doctrine->getRepository('AppBundle:GlobalAlarmSettingsEntity');

		$em = new GlobalAlarmSettingsEntity();

		$em->setCriticalHeL(35.0);
		$em->setAlarmHeL(45.0);

		$em->setAlarmHebH(1.5);
		$em->setCriticalHebH(2.0);

		$em->setCriticalVpH(5.1);
		$em->setAlarmVpH(4.6);
		$em->setAlarmVpL(0.8);
		$em->setCriticalVpL(0.1);

		$em->setCriticalWfH(5.0);
		$em->setAlarmWfH(4.0);
		$em->setAlarmWfL(1.0);
		$em->setCriticalWfL(0.5);

		$em->setCriticalWtH(85.0);
		$em->setAlarmWtH(82.5);
		$em->setAlarmWtL(39.5);
		$em->setCriticalWtL(32.0);

		$em->setCriticalRhH(110.0);
		$em->setAlarmRhH(90.0);
		$em->setAlarmRhL(30.0);
		$em->setCriticalRhL(25.0);

		$em->setCriticalTfH(100.0);
		$em->setAlarmTfH(90.0);
		$em->setAlarmTfL(59);
		$em->setCriticalTfL(55.0);

		$em->setAlarmDpH(70.0);
		$em->setCriticalDpH(80.0);
		$em->setAlarmDpDiff(5.0);
		$em->setCriticalDpH(0.0);

		$em->setCriticalStorageHeL(9.0);
		$em->setAlarmStorageHeL(10.0);

		$em->setCriticalStorageVpH(8.0);
		$em->setAlarmStorageVpH(5.0);
		$em->setAlarmStorageVpL(0.9);
		$em->setCriticalStorageVpL(0.5);

		$em->setUid('admin');


		//Doctrine manager is also available
		$this->manager->persist($em);
		$this->manager->flush();
		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Exception
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		$em = $this->container->get('doctrine')->getManager();
		$className = $em->getClassMetadata(GlobalAlarmSettingsEntity::class)->getName();
		$em->createQuery('DELETE FROM '.$className)->execute();
	}

	/**
	 * @return int
	 */
	public function getOrder(): int {
		return 0;
	}

}