<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:46 PM
 */

namespace AppBundle\DataSeeds;

use AppBundle\Entity\DictWordsEntity;
use Doctrine\ORM\Query\ResultSetMapping;
use Soyuka\SeedBundle\Command\Seed;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DictWordsSeed
 * @package AppBundle\DataSeeds
 */
class DictWordsSeed extends Seed
{

	protected function configure()
	{
		//The seed won't load if this is not set
		//The resulting command will be {prefix}:country
		$this
			->setSeedName('dictWords');

		parent::configure();
	}

	/**
	 * Load a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Doctrine\Common\Persistence\Mapping\MappingException
	 * @throws \Doctrine\ORM\ORMException
	 */
	public function load(InputInterface $input, OutputInterface $output)
	{
		//Doctrine logging eats a lot of memory, this is a wrapper to disable logging
		$this->disableDoctrineLogging();

		//Access doctrine through $this->doctrine
		$dictWordsRepository = $this->doctrine->getRepository('AppBundle:DictWordsEntity');


		foreach ($this->getWords() as $word) {

			if($dictWordsRepository->findOneBy(array('word' => $word))) {
				continue;
			}

			$em = new DictWordsEntity();

			$em->setWord($word);

			//Doctrine manager is also available
			$this->manager->persist($em);

			$this->manager->flush();
		}

		$this->manager->clear();
	}

	/**
	 * Unload a seed.
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @throws \Exception
	 */
	public function unload(InputInterface $input, OutputInterface $output)
	{
		$em = $this->container->get('doctrine')->getManager();
		$className = $em->getClassMetadata(DictWordsEntity::class )->getName();
		$em->createQuery('DELETE FROM '.$className)->execute();
	}

	public function getWords()
	{
		$words = file(__DIR__.'/../Resources/dictWords.txt');
		shuffle($words);
		return array_slice($words, 0, 1000);
	}

	/**
	 * @return int
	 */
	public function getOrder(): int {
		return 0;
	}
}