<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/27/2017
 * Time: 2:48 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\SystemsEntity;
use AppBundle\Form\UserChangeInfoForm;
use AppBundle\Form\UserChangePasswordForm;
use AppBundle\Form\UserNewEditForm;
use AppBundle\Form\UserPreferencesForm;
use AppBundle\Form\UserPreferencesSystemsForm;
use AppBundle\Form\UserRegistrationForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\UserEntity;

class UserController extends Controller
{

	/**
	 * @Route("/register", name="user_register")
	 */
	public function registerAction(Request $request)
	{
		$form = $this->createForm(UserRegistrationForm::class);

		$form->handleRequest($request);

		if($form->isSubmitted() and $form->isValid()){

			$user = $form->getData();

			$em = $this->getDoctrine()->getManager();
			$em->persist($user);
			$em->flush();

			$this->addFlash('success', "Thank you ".$user->getName()." for registering! Please allow up to 48 hours for your registration to be processed.");

			return $this->redirectToRoute('homepage');
		}

		return $this->render('User/register.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin/users", name="users_view")
	 */
	public function usersIndexAction()
	{
		$em = $this->getDoctrine()->getManager();
		$usersEnabled = $em->getRepository(UserEntity::class)
			->findAllUsersEnabled();
		$usersNotEnabled = $em->getRepository(UserEntity::class)
			->findAllUsersNotEnabled();

		$usersNotRegistered = $em->getRepository(UserEntity::class)
			->findAllUsersNotRegistered();

		return $this->render('Admin/users.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
			'usersEnabled' => $usersEnabled,
			'usersNotEnabled' =>$usersNotEnabled,
			'usersNotRegistered' => $usersNotRegistered
		]);
	}

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin/users/new", name="users_new")
	 */
	public function usersNewAction(Request $request)
	{
		$form = $this->createForm(UserNewEditForm::class);

		//only happens for POST requests
		$form->handleRequest($request);
		if($form->isSubmitted() and $form->isValid()){
			$userForm = $form->getData();

			$repo = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:DictWordsEntity');
			$dbWords = $repo->findRandWordsNew(2);

			$word = '';
			foreach ($dbWords as $wd)
			{
				$word .= trim($wd->getWord());
			}
			$word .= rand(1,1000);

			$userForm->setPlainPassword($word);


			$em = $this->getDoctrine()->getManager();
			$em->persist($userForm);
			$em->flush();


			$this->addFlash(
				'success',
				sprintf('User %s has been created', $userForm->getUid())
			);

			$this->addFlash(
				'success',
				sprintf("%s's new password is %s",ucfirst($userForm->getUid()),  $word)
			);

			return $this->redirectToRoute('users_view');

		}

		return $this->render('Admin/usersNewEdit.html.twig',[
			'userForm' => $form->createView(),
			'newUser' => true
		]);
	}

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin/users/{id}", name="users_edit")
	 */
	public function usersEditAction(Request $request, UserEntity $user)
	{
		$form = $this->createForm(UserNewEditForm::class, $user);

		//only happens for POST requests
		$form->handleRequest($request);
		if($form->isSubmitted() and $form->isValid()){
			$userForm = $form->getData();
			if($userForm->getEnabled()){
				$userForm->setRegistered(true);
			}
			$em = $this->getDoctrine()->getManager();
			$em->persist($userForm);
			$em->flush();

			$this->addFlash(
				'success',
				sprintf('User %s has been edited', $userForm->getUid())
			);

			return $this->redirectToRoute('users_view');

		}

		return $this->render('Admin/usersNewEdit.html.twig',[
			'userForm' => $form->createView(),
			'newUser' => false,
			'state' => $user->getState()
		]);
	}

	/**
	 * @Security("is_granted('ROLE_USER')")
	 * @Route("/profile", name="users_profile")
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function usersProfileAction(Request $request)
	{
		$user = $this->getUser();
		$formPassword = $this->createForm(UserChangePasswordForm::class, $this->getUser());
		$formInfo = $this->createForm(UserChangeInfoForm::class, $this->getUser());

		$formPassword->handleRequest($request);
		$formInfo->handleRequest($request);

		if($formPassword->isSubmitted() and $formPassword->isValid()){
			$passwordForm = $formPassword->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($passwordForm);
			$em->flush();

			$this->addFlash(
				'success',
				'Password has been changed'
			);

			return $this->redirectToRoute('users_profile');

		}else if($formInfo->isSubmitted() and $formInfo->isValid()) {
			/**
			 * @var UserEntity $infoForm
			 */
			$infoForm = $formInfo->getData();

			$em = $this->getDoctrine()->getManager();
			$em->persist($infoForm);
			$em->flush();

			$this->addFlash(
				'success',
				'User info has been changed'
			);

			return $this->redirectToRoute('users_profile');
		}


		return $this->render('User/usersProfile.html.twig', [
			'passwordForm' => $formPassword->createView(),
			'infoForm' => $formInfo->createView(),
			'state' => $user->getState()
		]);
	}

	/**
	 * @Security("is_granted('ROLE_USER')")
	 * @Route("/prefs", name="users_prefs")
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function usersPreferencesAction(Request $request)
	{
		/**
		 * @var UserEntity $user
		 */
		$user = $this->getUser();
		$formPrefs = $this->createForm(UserPreferencesForm::class, $user);
		$formPrefs->handleRequest($request);

		$sysRepo = $this->getDoctrine()->getRepository(SystemsEntity::class);
		$systems = $sysRepo->findAllEnabledByCustomer($user->getCustomerId()->getId());

		$formPrefsSys = $this->createForm(UserPreferencesSystemsForm::class, ['systems' => $systems, 'user' => $user]);
		$formPrefsSys->handleRequest($request);

		if($formPrefs->isSubmitted() and $formPrefs->isValid()){
			$prefsForm = $formPrefs->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($prefsForm);
			$em->flush();

			$this->addFlash(
				'success',
				'Preferences has been changed'
			);

			return $this->redirectToRoute('users_prefs');
		}elseif ($formPrefsSys->isSubmitted() and $formPrefsSys->isValid()){
			$prefsSysForm = $formPrefsSys->getData();
			$user->setMySystems($prefsSysForm['systems']);
			$em = $this->getDoctrine()->getManager();
			$em->persist($user);
			$em->flush();

			$this->addFlash(
				'success',
				'Preferences has been changed'
			);

			return $this->redirectToRoute('users_prefs');

		}

		return $this->render('User/usersPreferences.html.twig', [
			'prefsForm' => $formPrefs->createView(),
			'prefsSysForm' => $formPrefsSys->createView()
		]);
	}

	/**
	 * Returns an array of available roles for the app
	 *
	 * @param $originalRoles
	 * @return array
	 */
	private function getRoles($originalRoles)
	{
		$roles = array();

		/**
		 * Get all unique roles
		 */
		foreach ($originalRoles as $originalRole => $inheritedRoles) {
			foreach ($inheritedRoles as $inheritedRole) {
				$roles[$inheritedRole] = array();
			}

			$roles[$originalRole] = array();
		}

		/**
		 * Get all inherited roles from the unique roles
		 */
		foreach ($roles as $key => $role) {
			$roles[$key] = $this->getInheritedRoles($key, $originalRoles);
		}

		return $roles;
	}

	/**
	 * Helper funct for getRoles
	 *
	 * @param $role
	 * @param $originalRoles
	 * @param array $roles
	 * @return array
	 */
	private function getInheritedRoles($role, $originalRoles, $roles = array())
	{
		/**
		 * If the role is not in the originalRoles array,
		 * the role inherit no other roles.
		 */
		if (!array_key_exists($role, $originalRoles)) {
			return $roles;
		}

		/**
		 * Add all inherited roles to the roles array
		 */
		foreach ($originalRoles[$role] as $inheritedRole) {
			$roles[$inheritedRole] = $inheritedRole;
		}

		/**
		 * Check for each inhered role for other inherited roles
		 */
		foreach ($originalRoles[$role] as $inheritedRole) {
			return $this->getInheritedRoles($inheritedRole, $originalRoles, $roles);
		}
	}
}