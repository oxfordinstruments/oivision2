<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 6/27/2017
 * Time: 2:48 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\CustomerEntity;
use AppBundle\Form\CustomerNewEditForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\UserEntity;

class CustomerController extends Controller
{

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin/customers", name="customers_view")
	 */
	public function customersIndexAction()
	{
		$em = $this->getDoctrine()->getManager();
		$customersEnabled = $em->getRepository(CustomerEntity::class)
			->findAllCustomersEnabled();
		$customersNotEnabled = $em->getRepository(CustomerEntity::class)
			->findAllCustomersNotEnabled();

		return $this->render('Admin/customers.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
			'customersEnabled' => $customersEnabled,
			'customersNotEnabled' =>$customersNotEnabled
		]);
	}

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin/customers/new", name="customers_new")
	 */
	public function customersNewAction(Request $request)
	{
		$form = $this->createForm(CustomerNewEditForm::class);

		//only happens for POST requests
		$form->handleRequest($request);
		if($form->isSubmitted() and $form->isValid()){
			$customerForm = $form->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($customerForm);
			$em->flush();


			$this->addFlash(
				'success',
				sprintf('Customer %s has been created', $customerForm->getName())
			);

			return $this->redirectToRoute('customers_view');

		}

		return $this->render('Admin/customersNewEdit.html.twig',[
			'customerForm' => $form->createView(),
			'newCustomer' => true
		]);
	}

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin/customers/{id}", name="customers_edit")
	 */
	public function customersEditAction(Request $request, CustomerEntity $customer)
	{
		$form = $this->createForm(CustomerNewEditForm::class, $customer);

		//only happens for POST requests
		$form->handleRequest($request);
		if($form->isSubmitted() and $form->isValid()){
			$customerForm = $form->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($customerForm);
			$em->flush();

			$this->addFlash(
				'success',
				sprintf('Customer %s has been edited', $customerForm->getName())
			);

			return $this->redirectToRoute('customers_view');

		}

		return $this->render('Admin/customersNewEdit.html.twig',[
			'customerForm' => $form->createView(),
			'newCustomer' => false,
			'state' => $customer->getState()
		]);
	}

}