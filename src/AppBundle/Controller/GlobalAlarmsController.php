<?php

namespace AppBundle\Controller;


use AppBundle\Form\GlobalAlarmsForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\GlobalAlarmSettingsEntity;
use AppBundle\Repository\GlobalAlarmsRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpFoundation\Request;

class GlobalAlarmsController extends Controller
{
	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin/globalalarms", name="global_alarms")
	 */
    public function indexAction(Request $request)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$data = $em->getRepository(GlobalAlarmSettingsEntity::class)
	    ->findLastId();
    	//dump($data);
    	$user = $this->getUser();
    	$data[0]->setUid($user->getUid());

	    $form = $this->createForm(GlobalAlarmsForm::class, $data[0]);

	    //only happens for POST requests
	    $form->handleRequest($request);
	    if($form->isSubmitted() and $form->isValid()){
		    $formData = $form->getData();
		    $formDataNew = clone $formData;

		    $em = $this->getDoctrine()->getManager();
		    $em->persist($formDataNew);
		    $em->flush();

		    $this->addFlash(
			    'success',
			    'Global Alarm Settings have been updated!'
		    );

		    return $this->redirectToRoute('global_alarms');

	    }

	    return $this->render('Admin/globalAlarms.html.twig',[
		    'alarmsForm' => $form->createView()
	    ]);
    }
}
