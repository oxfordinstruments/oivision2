<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use AppBundle\Entity\UserEntity;
use AppBundle\Repository\SystemsDataGeRepository;
use AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\User\User;


/**
 * Class TestingController
 * @package AppBundle\Controller
 * @Security("is_granted('ROLE_ADMIN')")
 */
class TestingController extends Controller
{

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/testing/alarms", name="testAlarms")
	 * @throws \Twig\Error\Error
	 */
	public function indexAction()
	{
		$utils = $this->get('app.utils_common');

		/**
		 * @var User $userOrig
		 */
		$tokenOrig = $this->container->get('security.token_storage')->getToken(); //Testing Only

		$token = new AnonymousToken('dummy', 'dummy', ['ROLE_USER']);
		$this->container->get('security.token_storage')->setToken($token);

		$this->container->get('monolog.logger.oivision')
			->info('Begin Checking for alarms',
				['key' => 'cron', 'function' => __CLASS__.'::'.__FUNCTION__]);

		/**
		 * @var SystemsDataGeRepository $sysDataRepo
		 */
		$sysDataRepo = $this->container->get('doctrine')->getRepository(SystemsDataGeEntity::class);
		$alarmsCount = $sysDataRepo->getSystemsCurrentDayAlarmsCount($this->container->get('security.authorization_checker'));
//		if(count($alarmsCount) == 0){
//			$this->container->get('monolog.logger.oivision')
//				->info('No Alarms Today',
//					['key' => 'cron', 'function' => __CLASS__.'::'.__FUNCTION__]);
//			return true;
//		}

		/**
		 * @var UserRepository $usersRepo
		 */
		$usersRepo = $this->container->get('doctrine')->getRepository(UserEntity::class);

		$processed = [];

		$users = $usersRepo->findAllUsersEnabledAlarmNotify();
		$adminUser = $usersRepo->findOneBy(['uid' => 'admin']);
		$mm = $this->container->get('app.mail_manager');
		$utils = $this->container->get('app.utils_common');
		$em = $this->container->get('doctrine')->getManager();

		$startDate = new \DateTime();
		$startDate->setTimestamp(strtotime('2018-06-19 23:59:00'));
		$endDate = clone $startDate;
		$endDate->modify('-5 days');
		$endDate->modify('-1 minute');

		/**
		 * @var SystemsEntity[]
		 */
		$adminUser->dataStore = [];

		$token = new AnonymousToken('dummy', $adminUser, $utils->getInheritedRoles($adminUser->getRoles()));
		$this->container->get('security.token_storage')->setToken($token);

		$sysData = $sysDataRepo->getSystemsCriticalAlarmsBetweenTimes($this->container->get('security.authorization_checker'), $adminUser, $startDate, $endDate, false, true, true);
		//dump($sysData);

		/**
		 * @var SystemsDataGeEntity $datum
		 */
		foreach ($sysData as $datum) {
			if(!in_array($datum->getSystem(), $adminUser->dataStore)){
				array_push($adminUser->dataStore, $datum->getSystem());
			}
			if(!in_array($datum, $processed)){
				array_push($processed, $datum);
			}
		}

		/**
		 * @var SystemsDataGeEntity $datum
		 */
		foreach ($sysData as $key => $datum) {
			$system = array_search($datum->getSystem(), $adminUser->dataStore);

			if($system !== false){
				$tmp = $adminUser->dataStore[$system]->getData();
				if(is_null($tmp)){
					$tmp = [];
				}
				array_push($tmp, $datum);
				$adminUser->dataStore[$system]->setData($tmp);
			}

		}

		//dump($adminUser);

		/**
		 * @var UserEntity $user
		 */
		foreach ($users as $user) {

//				dump($user);
//				dump($utils->getInheritedRoles($user->getRoles()));

			if($user->getUid() != 'admin'){
				$user->dataStore = [];
				/**
				 * @var UserEntity $adminUser
				 * @var SystemsEntity $system
				 */
				foreach ($adminUser->getDataStore() as $system){
					if($user->getCustomerId() === $system->getCustomerId() or in_array('ROLE_EMPLOYEE', $utils->getInheritedRoles($user->getRoles()))){
						array_push($user->dataStore, $system);
					}
				}

			}

			if( empty($user->getDataStore())){continue;}
			//dump($user);

			if($this->getParameter('mailer_enable')) {
//				dump('Sending email to '.$user->getName().' <'.$user->getEmail().'>');
				$sent =$mm->sendEmail(
				$user->getEmail(),
				$this->container->getParameter('site_box_name')." Alarms Today",
				[
					'name' => $user->getName(),
					'date' => new \DateTime('now'),
					'systems' => $user->getDataStore()
				],
				':Emails:alarmsToday.html.twig');

				if($sent > 0){
					//dump('  ...Email sent');
				}else{
					//dump('  ...Failed to send');
				}
			}else{
				//dump('Mailer Disabled');
			}
		}
		//dump($processed);
		/**
		 * @var SystemsDataGeEntity $item
		 */
		foreach ($processed as $key=>$item){
			$item->setCriticalProcessed(true);
			$em->persist($item);
		}
		$em->flush();

		$this->container->get('security.token_storage')->setToken($tokenOrig); //Testing Only
		return $this->render(':Emails:criticalAlarm.html.twig', array(
			'name' => $adminUser->getName(),
			'dateStart' => $startDate,
			'dateEnd' => $endDate,
			'systems' => $adminUser->getDataStore()
		));
	}
}
