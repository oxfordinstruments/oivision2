<?php

namespace AppBundle\Controller;


use AppBundle\Entity\CustomerEntity;
use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Entity\SystemsEntity;
use AppBundle\Entity\TestEntity;
use AppBundle\Entity\UserEntity;
use AppBundle\Form\SystemShowAllForm;
use AppBundle\Form\TestFormType;
use AppBundle\Resources\UtilitiesCommon;
use Detection\MobileDetect;
use Moment\Moment;
use Ob\HighchartsBundle\Highcharts\Highchart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Ldap\Ldap;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Zend\Json\Expr;
use \Indaxia\OTR\ITransformable;
use \Indaxia\OTR\Traits\Transformable;
use \Indaxia\OTR\Annotations\Policy;

class DefaultController extends Controller
{


	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction(Request $request)
	{
		if (!is_null($this->getUser())) {
			return $this->redirectToRoute('dashboard');
		}

		return $this->render('Default/home.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
		]);
	}

	/**
	 * @Security("is_granted('ROLE_USER')")
	 * @Route("/dashboard", name="dashboard")
	 * @throws \Moment\MomentException
	 */
	public function dashboardAction(Request $request)
	{
		$user = $this->getUser();
		$sysRepo = $this->getDoctrine()->getRepository(SystemsEntity::class);
		/**
		 * @var SystemsEntity[] $systems
		 */
		if ($this->get('security.authorization_checker')->isGranted('ROLE_EMPLOYEE')) {
			$systems = $sysRepo->findAllSystemsWithTimestamp();
		} else {
			$systems = $sysRepo->findAllMySystemsWithTimestamp($user->getCustomerId()->getId());
		}

		$nowWarn = new Moment();
		$nowWarn->subtractHours($this->getParameter('dashboardLastReportWarnHours'));
		$nowError = new Moment();
		$nowError->subtractHours($this->getParameter('dashboardLastReportErrorHours'));
		foreach ($systems as $key=>$sys){
			if(is_null($sys['ts'])){
				$systems[$key]['css'] = 'red';
			}else{
				$lh = new Moment($sys['ts']);
				if($lh->isBefore($nowError)) {
					$systems[$key]['css'] = 'red';
				}else if($lh->isBefore($nowWarn)){
					$systems[$key]['css'] = 'yellow';
				}else{
					$systems[$key]['css'] = '';
				}
			}

		}

		$sysDataRepo = $this->getDoctrine()->getRepository(SystemsDataGeEntity::class);

		$sysDataAlarms = $sysDataRepo->getSystemsCurrentHourAlarms($this->get('service_container')->get('security.authorization_checker'), $this->getUser());
		/**
		 * @var SystemsDataGeEntity $alarm
		 * @var SystemsDataGeEntity[] $sysDataAlarms
		 */
		foreach ($sysDataAlarms as $key => $alarm){
			$alarms = [];
			if($alarm->getHea()){array_push($alarms, 'Helium');}
			if($alarm->getVpa()){array_push($alarms, 'Pressure');}
			if($alarm->getWfa()){array_push($alarms, 'Water Flow');}
			if($alarm->getWta()){array_push($alarms, 'Water Temp');}
			if($alarm->getRha()){array_push($alarms, 'Humidity');}
			if($alarm->getTfa()){array_push($alarms, 'Room Temp');}
			if($alarm->getDpa()){array_push($alarms, 'Room DP');}
			if($alarm->getDpda()){array_push($alarms, 'DP Diff');}
			$sysDataAlarms[$key]->setAlarmsString(implode(', ', $alarms));
		}

		$sysDataAlarmsCritical = $sysDataRepo->getSystemsCurrentHourCriticalAlarms($this->get('service_container')->get('security.authorization_checker'), $this->getUser());

		/**
		 * @var SystemsDataGeEntity $alarm
		 * @var SystemsDataGeEntity[] $sysDataAlarmsCritical
		 */
		foreach ($sysDataAlarmsCritical as $key => $alarm){
			$alarms = [];
			if($alarm->getHec()){array_push($alarms, 'Helium');}
			if($alarm->getVpc()){array_push($alarms, 'Pressure');}
			if($alarm->getWfc()){array_push($alarms, 'Water Flow');}
			if($alarm->getWtc()){array_push($alarms, 'Water Temp');}
			if($alarm->getRhc()){array_push($alarms, 'Humidity');}
			if($alarm->getDpc()){array_push($alarms, 'Dew Point');}
			if($alarm->getDpdc()){array_push($alarms, 'DP Diff');}
			if($alarm->getTfc()){array_push($alarms, 'Room Temp');}
			if($alarm->getCmpc()){array_push($alarms, 'Compressor');}
			if($alarm->getQc()){array_push($alarms, 'Field');}
			$sysDataAlarmsCritical[$key]->setCriticalsString(implode(', ', $alarms));
		}


		$sysDataDayAlarms = $sysDataRepo->getSystemsCurrentDayAllAlarms($this->get('service_container')->get('security.authorization_checker'), $this->getUser());


		return $this->render('Default/dashboard.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
			'systems' => $systems,
			'systemsAlarms' => $sysDataAlarms,
			'systemsAlarmsCritical' => $sysDataAlarmsCritical,
			'systemsAlarmsAll' => $sysDataDayAlarms
		]);
	}

	/**
	 * @Security("is_granted('ROLE_USER')")
	 * @Route("/system/{id}", name="system")
	 * @param Request $request
	 * @param SystemsEntity $system
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function systemAction(Request $request, SystemsEntity $system)
	{
		$custRepo = $this->getDoctrine()->getRepository(CustomerEntity::class);
		$oihCust = $custRepo->findOneBy(['name' => 'OiHealthcare']);
		/**
		 * @var UserEntity $user
		 */
		$user = $this->getUser();
		if(!$user->isAdmin()){
			if($user->getCustomerId()->getId() != $system->getCustomerId()->getId()
				and $user->getCustomerId()->getId() != $oihCust->getId())
			{
				return $this->redirectToRoute('dashboard');
			}
		}

		$form = $this->createForm(SystemShowAllForm::class);

		//only happens for POST requests
		$form->handleRequest($request);


		$days = $this->getParameter('data_show_days');
		$device = $this->get('mobile_detect.mobile_detector');
		if($device->isMobile()){
			$days = $this->getParameter('data_show_days_mobile');
		}
		if ($form->isSubmitted() and $form->isValid()) {
			$sysForm = $form->getData();
			$days = $this->getParameter('data_show_days_all');

		}
		$dataRepo = $this->getDoctrine()->getRepository(SystemsDataGeEntity::class);
		$data = $dataRepo->getSystemData($system, $days);

		$ts = [];
		$rh = [];
		$tf = [];
		$dp = [];
		$dpd = [];
		$wt = [];
		$wf = [];
		$he = [];
		$vp = [];
		$cp = [];
		$q = [];

		$envDataSet = false;
		$vesDataSet = false;

		/**
		 * @var SystemsDataGeEntity $datum
		 */
		$data = array_reverse($data);
		foreach ($data as $datum) {
			array_push($ts, $datum->getTimestamp()->format('Y-m-d H:i:s'));
			if($system->getMonitorRh()) {
				array_push($rh, $datum->getRh());
				$envDataSet = true;
			}

			if($system->getMonitorTf()) {
				array_push($tf, $datum->getTf());
				$envDataSet = true;
			}

			if($system->getMonitorTf() and $system->getMonitorRh()){
				if($datum->getDp() == 0){
					array_push($dp, null);
					array_push($dpd, null);
				}else{
					array_push($dp, $datum->getDp());
					array_push($dpd, $datum->getDpdiff());
				}

				$envDataSet = true;
			}

			if($system->getMonitorWf()) {
				array_push($wf, $datum->getWf());
				$envDataSet = true;
			}

			if($system->getMonitorWt()) {
				array_push($wt, $datum->getWt());
				$envDataSet = true;
			}

			if($system->getMonitorHe()){
				if($datum->getHe() != 111.11){
					array_push($he, $datum->getHe());
				}else{
					array_push($he, null);
				}
				$vesDataSet = true;
			}

			if($system->getMonitorVp()) {
				array_push($vp, $datum->getVp());
				$vesDataSet = true;
			}

			if($system->getMonitorCmp()) {
				if($datum->getCmp()){
					array_push($cp, 1);
				}else{
					array_push($cp, 0);
				}
				$envDataSet = true;
			}

			if($system->getMonitorQ()) {
				if($datum->getQ()){
					array_push($q, 1);
				}else{
					array_push($q, 0);
				}
				$envDataSet = true;
			}


		}

		$seriesEnv = array(
			array(
				'name'  => 'Room Rh',
				'type'  => 'spline',
				'color' => '#4572A7',
				'yAxis' => 1,
				'data'  => $rh,
				'tooltip' => [
					'valueSuffix' => ' %'
				]
			),
			array(
				'name'  => 'Room Tmp',
				'type'  => 'spline',
				'color' => '#AA4643',
				'data'  => $tf,

				'tooltip' => [
					'valueSuffix' => ' °F'
				]
			),
			array(
				'name'  => 'Dew Point',
				'type'  => 'spline',
				'color' => '#F28D29',
				'data'  => $dp,

				'tooltip' => [
					'valueSuffix' => ' °F'
				]
			),
			array(
				'name'  => 'DP Diff',
				'type'  => 'spline',
				'color' => '#000CFF',
				'data'  => $dpd,

				'tooltip' => [
					'valueSuffix' => ' °F'
				]
			),
			array(
				'name'  => 'Water Flow',
				'type'  => 'spline',
				'color' => '#7F3FBF',
				'data'  => $wf,
				'yAxis' => 3,
				'tooltip' => [
					'valueSuffix' => ' gpm'
				]
			),
			array(
				'name'  => 'Water Tmp',
				'type'  => 'spline',
				'color' => '#3FBFBF',
				'data'  => $wt,
				'tooltip' => [
					'valueSuffix' => ' °F'
				]
			),
			array(
				'name'  => 'Compressor',
				'type'  => 'column',
				'color' => '#28d805',
				'data'  => $cp,
//				'yAxis' => 1,
			),
			array(
				'name'  => 'Field',
				'type'  => 'column',
				'color' => '#ffe633',
				'data'  => $q,
//				'yAxis' => 1,
			),
		);
		$yDataEnv = array(
			array(
				'labels' => array(
					'formatter' => new Expr('function () { return this.value + " %rh" }'),
					'style'     => array('color' => '#4572A7')
				),
				'gridLineWidth' => 0,
				'title' => array(
					'text'  => 'Room Humidity',
					'style' => array('color' => '#4572A7')
				),
				'opposite' => false,
			),
			array(
				'labels' => array(
					'formatter' => new Expr('function () { return this.value + " \u2109" }'),
					'style'     => array('color' => '#AA4643')
				),
				'title' => array(
					'text'  => 'Room Temp',
					'style' => array('color' => '#AA4643')
				),
				'opposite' => true,
			),
			array(
				'labels' => array(
					'formatter' => new Expr('function () { return this.value + " \u2109" }'),
					'style'     => array('color' => '#F28D29')
				),
				'title' => array(
					'text'  => 'Dew Point',
					'style' => array('color' => '#F28D29')
				),
				'opposite' => true,
			),
			array(
				'labels' => array(
					'formatter' => new Expr('function () { return this.value + " \u2109" }'),
					'style'     => array('color' => '#000CFF')
				),
				'title' => array(
					'text'  => 'DP Diff',
					'style' => array('color' => '#000CFF')
				),
				'opposite' => true,
			),
			array(
				'labels' => array(
					'formatter' => new Expr('function () { return this.value + " \u2109" }'),
					'style'     => array('color' => '#3FBFBF')
				),
				'gridLineWidth' => 0,
				'title' => array(
					'text'  => 'Water Temp',
					'style' => array('color' => '#3FBFBF')
				),
				'opposite' => true,
			),
			array(
				'labels' => array(
					'formatter' => new Expr('function () { return this.value + " gpm" }'),
					'style'     => array('color' => '#7F3FBF')
				),
				'title' => array(
					'text'  => 'Water Flow',
					'style' => array('color' => '#7F3FBF')
				),
				'opposite' => false,
			),

		);
		$obEnv = new Highchart();
		$obEnv->chart->renderTo('chartEnviromentals'); // The #id of the div where to render the chart
		$obEnv->chart->type('column');
		$obEnv->chart->zoomType('x');
		$obEnv->title->text('Environmentals');
		$obEnv->xAxis->categories($ts);
		$obEnv->yAxis($yDataEnv);
		$obEnv->legend->enabled(true);
		$obEnv->tooltip->shared(true);
		$obEnv->series($seriesEnv);


		$seriesVes = array(
			array(
				'name'  => 'Helium',
				'type'  => 'spline',
				'color' => '#4572A7',
				'yAxis' => 0,
				'data'  => $he,
				'tooltip' => [
					'valueSuffix' => ' %'
				]
			),
			array(
				'name'  => 'Vessel Pressure',
				'type'  => 'spline',
				'color' => '#AA4643',
				'data'  => $vp,
				'yAxis' => 1,
				'tooltip' => [
					'valueSuffix' => ' psi'
				]
			),
		);
		$yDataVes = array(
			array(
				'labels' => array(
					'formatter' => new Expr('function () { return this.value + " %" }'),
					'style'     => array('color' => '#4572A7')
				),
				'title' => array(
					'text'  => 'Helium',
					'style' => array('color' => '#4572A7')
				),
				'opposite' => true,
			),
			array(
				'labels' => array(
					'formatter' => new Expr('function () { return this.value + " psi" }'),
					'style'     => array('color' => '#AA4643')
				),
				'gridLineWidth' => 0,
				'title' => array(
					'text'  => 'Pressure',
					'style' => array('color' => '#AA4643')
				),
				'opposite' => false,
			),


		);
		$obVes = new Highchart();
		$obVes->chart->renderTo('chartVessel'); // The #id of the div where to render the chart
		$obVes->chart->type('column');
		$obVes->chart->zoomType('x');
		$obVes->title->text('Vessel');
		$obVes->xAxis->categories($ts);
		$obVes->yAxis($yDataVes);
		$obVes->legend->enabled(true);
		$obVes->tooltip->shared(true);
		$obVes->series($seriesVes);

		$data = array_reverse($data);

		return $this->render('Default/system.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
			'system' => $system,
			'data' => $data,
			'days' => $days,
			'chartEnviromentals' => $obEnv,
			'chartVessel' => $obVes,
			'envDataSet' =>$envDataSet,
			'vesDataSet' =>$vesDataSet,
			'form' => $form->createView(),
		]);
	}

	/**
	 * @Security("is_granted('ROLE_USER')")
	 * @Route("/system/{id}/more", options={"expose"=true}, name="system_more")
	 * @param Request $request
	 * @param SystemsEntity $system
	 * @return JsonResponse
	 * @throws \Exception
	 */
	public function systemMoreDataAjax(Request $request, SystemsEntity $system)
	{
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(['error'=>'Must be an ajax call.']);
		}
		if(!$request->isMethod('POST')){
			return new JsonResponse(['error'=>'Call must be made via post.']);
		}
		if( $request->get('lastDate') === null ) {
			return new JsonResponse(['error'=>'lastDate not set']);
		}

		$from = new \DateTime($request->get('lastDate'));
		$from->setTimezone(new \DateTimeZone('UTC'));
		$to = clone $from;
		$to->add(new \DateInterval('P7D'));


		$dataRepo = $this->getDoctrine()->getRepository(SystemsDataGeEntity::class);
		/**
		 * @var SystemsDataGeEntity $data
		 * @var SystemsDataGeEntity $datum
		 */
		$data = $dataRepo->getSystemDataBetweenDates($system, $from, $to, 1500);
		foreach ($data as $key => $datum) {
			$data[$key] = $datum->toArray();
		}

		return new JsonResponse([
			'from' => $from,
			'to' => $to,
			'data' => $data,
		]);
	}

	/**
	 * @Security("is_granted('ROLE_USER')")
	 * @Route("/system/{id}/more", options={"expose"=true}, name="system_more")
	 * @param Request $request
	 * @param SystemsEntity $system
	 * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
	 * @throws \Exception
	 */
	public function systemMoreDataAjaxOld(Request $request, SystemsEntity $system)
	{
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(['error'=>'Must be an ajax call.']);
		}
		if(!$request->isMethod('POST')){
			return new JsonResponse(['error'=>'Call must be made via post.']);
		}
		if( $request->get('lastDate') === null ) {
			return new JsonResponse(['error'=>'lastDate not set', 'request'=>$request]);
		}

		$from = new \DateTime($request->get('lastDate'));
		$from->setTimezone(new \DateTimeZone('UTC'));
		$to = clone $from;
		$to->sub(new \DateInterval('P7D'));


		$dataRepo = $this->getDoctrine()->getRepository(SystemsDataGeEntity::class);
		/**
		 * @var SystemsDataGeEntity $data
		 */
		$data = $dataRepo->getSystemDataBetweenDates($system, $from, $to, 1500);


		return $this->render(':Default:systemMore.html.twig', [
			'system' => $system,
			'data' => $data,
		]);
	}

	/**
	 * @Route("/none", name="under_construction")
	 */
	public function underConstructionAction(Request $request)
	{
		return $this->render('Default/none.html.twig');
	}


	/**
	 * @Security("is_granted('ROLE_MANAGER_EMPLOYEE')")
	 * @Route("/new", name="new")
	 */
	public function newSystemAction(Request $request)
	{
		$form = $this->createForm(TestFormType::class);

		//only happens for POST requests
		$form->handleRequest($request);
		if ($form->isSubmitted() and $form->isValid()) {
			$testForm = $form->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($testForm);
			$em->flush();

			$this->addFlash(
				'success',
				sprintf('System created by %s', $this->getUser()->getUid())
			);

			return $this->redirectToRoute('homepage');

		}

		return $this->render('Default/new.html.twig', [
			'testForm' => $form->createView()
		]);
	}

	/**
	 * @Security("is_granted('ROLE_MANAGER_EMPLOYEE') or is_granted('ROLE_MANAGER_USER')")
	 * @Route("/{id}/edit", name="edit")
	 */
	public function editSystemAction(Request $request, TestEntity $test)
	{
		$form = $this->createForm(TestFormType::class, $test);

		//only happens for POST requests
		$form->handleRequest($request);
		if ($form->isSubmitted() and $form->isValid()) {
			$testForm = $form->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($testForm);
			$em->flush();

			$this->addFlash(
				'success',
				sprintf('System edited by %s', $this->getUser()->getUid())
			);

			return $this->redirectToRoute('homepage');

		}

		return $this->render('Default/edit.html.twig', [
			'testForm' => $form->createView()
		]);
	}

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/temp", name="temp")
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function temp(Request $request)
	{
		/**
		 * @var UserEntity $user
		 */
		$user = $this->container->get('doctrine')->getRepository(UserEntity::class)->findOneBy(['uid'=>'admin']);
		$utils = $this->container->get('app.utils_common');
		$roles = $utils->getInheritedRoles($user->getRoles());
		$user->setRoles($roles);

		$sysDataRepo = $this->container->get('doctrine')->getRepository(SystemsDataGeEntity::class);
		/**
		 * @var SystemsDataGeEntity $sysData
		 */
		$sysData = $sysDataRepo->getSystemsNotProcessedCritical();

		$startDate = new \DateTime();
//		$startDate->setTimestamp(strtotime('2018-03-08 12:00:00')); //Testing
		$endDate = clone $startDate;
		$endDate->modify('-'.$this->container->getParameter('checkCriticalsHours').' hour');
		$endDate->modify('-1 minute');

		if ($this->container->has('profiler'))
		{
			$this->container->get('profiler')->disable();
		}

		$message = (new \Swift_Message('Subject goes Here'))
			->setFrom('oivison@evotodi.com')
			->setTo('evotodi@gmail.com')
			->setBody(
				$this->renderView('Emails/criticalAlarm.html.twig',
					[
						'name' => "Justin Davis",
						'data' => $sysData,
						'dateStart' => $startDate,
						'dateEnd' => $endDate
					]),
				'text/html'
			);
		$mailer = $this->container->get('mailer');
		$mailer->send($message);

		if ($this->container->has('profiler'))
		{
			$this->container->get('profiler')->enable();
		}
		return $this->render('Default/none.html.twig',[]);
	}

	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/ldap", name="ldap")
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function ldap(Request $request)
	{
		$base_dn = $this->getParameter('ldap_base_dn');
		$admin_dn = $this->getParameter('ldap_admin_dn');
		$admin_password = $this->getParameter('ldap_admin_pw');
		$ldap = Ldap::create('ext_ldap', array(
			'host' => $this->getParameter('ldap_host'),
			'port' => $this->getParameter('ldap_port'),
			'version' => $this->getParameter('ldap_version'),
			'encryption' => $this->getParameter('ldap_encrypt'),
		));

		$ldap->bind($admin_dn, $admin_password);
		$query = $ldap->query('ou=people,'.$base_dn, '(&(objectclass=person)(uid=aa*))');
		$results = $query->execute();
//		dump($results->toArray());
		die();

		return $this->render(':Default:dashboard.html.twig',
			[
			]
		);
	}


}

;