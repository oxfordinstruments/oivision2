<?php

namespace AppBundle\Controller;


use AppBundle\Resources\StreamedOutput;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelInterface;
use SensioLabs\AnsiConverter\Theme\SolarizedTheme;


class AdminController extends Controller
{
	/**
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Route("/admin", name="adminHomepage")
	 * @throws \Exception
	 */
    public function indexAction()
    {
        return $this->render('Admin/admin.html.twig', array(
        ));
    }

	/**
	 * @param Request $request
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Method({"GET"})
	 * @Route("/admin/ajax/getRemoteData", name="getRemoteData", options = { "expose" = true })
	 *
	 * @return Response
	 */
	public function getRemoteData(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		$response = new StreamedResponse(function (){
			$app = new Application($this->get('kernel'));
			$app->setAutoExit(false);

			$input = new ArrayInput(array(
				'command' => 'app:get_data'
			));
			$input->setInteractive(false);

			$output = new StreamedOutput(fopen('php://stdout', 'w'));
			$app->run($input, $output);
		});


		return $response;
	}

	/**
	 * @param Request $request
	 * @Security("is_granted('ROLE_ADMIN')")
	 * @Method({"GET"})
	 * @Route("/admin/ajax/getRemoteSystems", name="getRemoteSystems", options = { "expose" = true })
	 *
	 * @return Response
	 */
	public function getRemoteSystems(Request $request)
	{
		if (!$request->isXmlHttpRequest()) {
			throw new BadRequestHttpException('AJAX request expected.');
		}

		$response = new StreamedResponse(function (){
			$app = new Application($this->get('kernel'));
			$app->setAutoExit(false);

			$input = new ArrayInput(array(
				'command' => 'app:get_sites'
			));
			$input->setInteractive(false);

			$output = new StreamedOutput(fopen('php://stdout', 'w'));
			$app->run($input, $output);
		});


		return $response;
	}
}
