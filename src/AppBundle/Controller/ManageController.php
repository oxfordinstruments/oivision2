<?php

namespace AppBundle\Controller;


use AppBundle\Entity\GlobalAlarmSettingsEntity;
use AppBundle\Entity\SystemsDataGeEntity;
use AppBundle\Form\SystemsNewEditForm;
use AppBundle\Repository\GlobalAlarmsRepository;
use AppBundle\Repository\SystemsDataGeRepository;
use AppBundle\Repository\SystemsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\SystemsEntity;
use AppBundle\Resources\UtilitiesCommon;

/**
 * Class ManageController
 * @package AppBundle\Controller
 * @Security("is_granted('ROLE_MANAGER_EMPLOYEE') or is_granted('ROLE_MANAGER_USER')")
 */
class ManageController extends Controller
{
	/**
	 * @Route("/manage/systems", name="systems_list")
	 */
    public function systemsIndexAction()
    {
	    $user = $this->getUser();
	    $em = $this->getDoctrine()->getManager();
	    if($this->get('security.authorization_checker')->isGranted('ROLE_EMPLOYEE')){
		    $systemsEnabledData = $em->getRepository(SystemsEntity::class)->findAllSystemsEnabled();
		    $systemsNotEnabledData = $em->getRepository(SystemsEntity::class)->findAllSystemsNotEnabled();
	    }else{
		    $systemsEnabledData = $em->getRepository(SystemsEntity::class)->findAllMySystemsEnabled($user->getCustomerId()->getId());
		    $systemsNotEnabledData = $em->getRepository(SystemsEntity::class)->findAllMySystemsNotEnabled($user->getCustomerId()->getId());
	    }



        return $this->render('Manage/systems.html.twig', array(
        	'systemsEnabledData' => $systemsEnabledData,
	        'systemsNotEnabledData' => $systemsNotEnabledData
        ));
    }

	/**
	 * @Route("/manage/systems/new", name="systems_new")
	 */
	public function systemsNewAction(Request $request)
	{
		$utils = $this->container->get('app.utils_common');
		$sys = new SystemsEntity();
		$sys = $utils->updateToCurrentGlobalAlarms($sys);
		$sys->setOffsetHe($this->getParameter('offsetHe'));
		$sys->setOffsetVp($this->getParameter('offsetVp'));
		$sys->setOffsetWf($this->getParameter('offsetWf'));
		$sys->setOffsetWtf($this->getParameter('OffsetWtf'));
		$sys->setOffsetTc($this->getParameter('offsetTc'));
		$sys->setOffsetRh($this->getParameter('offsetRh'));


		$form = $this->createForm(SystemsNewEditForm::class, $sys);

		//only happens for POST requests
		$form->handleRequest($request);
		if($form->isSubmitted() and $form->isValid()){
			$formData = $form->getData();
			$em = $this->getDoctrine()->getManager();
			$em->persist($formData);
			$em->flush();


			$this->addFlash(
				'success',
				'System has been created'
			);

			return $this->redirectToRoute('systems_list');

		}

		return $this->render(':Manage:systemsNewEdit.html.twig',[
			'systemsForm' => $form->createView(),
			'newSystem' => true
		]);
	}

	/**
	 * @Route("/manage/systems/{id}", name="systems_edit")
	 */
	public function systemsEditAction(Request $request, SystemsEntity $systems)
	{
		$form = $this->createForm(SystemsNewEditForm::class, $systems);

		//only happens for POST requests
		$form->handleRequest($request);
		if($form->isSubmitted() and $form->isValid()){
			/**
			 * @var SystemsEntity $formData
			 */
			$formData = $form->getData();
			$em = $this->getDoctrine()->getManager();
			if($form->get('clearData')->getData() == "YES"){
				$repo = $this->getDoctrine()->getRepository(SystemsDataGeEntity::class);
				$repo->deleteSystemData($formData->getMacRdc());
				$this->addFlash('info', 'Data Cleared for '.$formData->getSystemName());
				return $this->redirectToRoute('systems_list');
			}

			$em->persist($formData);
			$em->flush();

			$this->addFlash(
				'success',
				sprintf('System %s has been edited', $formData->getSystemName())
			);

			return $this->redirectToRoute('systems_list');

		}

		return $this->render(':Manage:systemsNewEdit.html.twig',[
			'systemsForm' => $form->createView(),
			'newSystem' => false,
			'state' => $systems->getSystemState()
		]);
	}
}
