<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/20/2017
 * Time: 10:10 AM
 */

namespace AppBundle\Entity;

use Doctrine\DBAL\Schema\Constraint;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GlobalAlarmsRepository")
 * @ORM\Table(name="globalAlarmSettings")
 */
class GlobalAlarmSettingsEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalHeL() <= this.getAlarmHeL()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="100", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float", precision=2)
	 */
	private $alarmHeL;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalHebH() >= this.getAlarmHebH()",
	 *     message="Greater Than Critical High"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="50", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmHebH;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmVpH() <= this.getCriticalVpH()",
	 *     message="Greater Than Critical High"
	 * )
	 * @Assert\Expression(
	 *     "this.getAlarmVpL() <= this.getAlarmVpH()",
	 *     message="Less Than Alarm Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="45", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmVpH;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmVpL() >= this.getCriticalVpL()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="45", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmVpL;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmTfH() <= this.getCriticalTfH()",
	 *     message="Greater Than Critical High"
	 * )
	 * @Assert\Expression(
	 *     "this.getAlarmTfL() <= this.getAlarmTfH()",
	 *     message="Less Than Alarm Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmTfH;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmTfL() >= this.getCriticalTfL()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmTfL;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmRhH() <= this.getCriticalRhH()",
	 *     message="Greater Than Critical High"
	 * )
	 * @Assert\Expression(
	 *     "this.getAlarmRhL() <= this.getAlarmRhH()",
	 *     message="Less Than Alarm Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmRhH;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmRhL() >= this.getCriticalRhL()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmRhL;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmWfH() <= this.getCriticalWfH()",
	 *     message="Greater Than Critical High"
	 * )
	 * @Assert\Expression(
	 *     "this.getAlarmWfL() <= this.getAlarmWfH()",
	 *     message="Less Than Alarm Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="50", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmWfH;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmWfL() >= this.getCriticalWfL()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="50", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmWfL;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmWtH() <= this.getCriticalWtH()",
	 *     message="Greater Than Critical High"
	 * )
	 * @Assert\Expression(
	 *     "this.getAlarmWtL() <= this.getAlarmWtH()",
	 *     message="Less Than Alarm Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmWtH;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmWtL() >= this.getCriticalWtL()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmWtL;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmDpH() <= this.getCriticalDpH()",
	 *     message="Greater Than Critical High"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="80", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmDpH;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmDpDiff() >= this.getCriticalDpDiff()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="100", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmDpDiff;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalStorageHeL() <= this.getAlarmStorageHeL()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="100", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmStorageHeL;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmStorageVpH() <= this.getCriticalStorageVpH()",
	 *     message="Greater Than Critical High"
	 * )
	 * @Assert\Expression(
	 *     "this.getAlarmStorageVpL() <= this.getAlarmStorageVpH()",
	 *     message="Less Than Alarm Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="45", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmStorageVpH;

	/**
	 * @Assert\Expression(
	 *     "this.getAlarmStorageVpL() >= this.getCriticalStorageVpL()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="45", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $alarmStorageVpL;






	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="100", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalHeL;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="50", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalHebH;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalVpL() <= this.getCriticalVpH()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="45", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalVpH;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="45", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalVpL;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalTfL() <= this.getCriticalTfH()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalTfH;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalTfL;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalRhL() <= this.getCriticalRhH()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalRhH;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalRhL;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="100", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalDpDiff;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalDpH() <= this.getCriticalDpH()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="80", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalDpH;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalWfL() <= this.getCriticalWfH()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="50", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalWfH;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="50", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalWfL;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalWtL() <= this.getCriticalWtH()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalWtH;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="110", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalWtL;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="100", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalStorageHeL;

	/**
	 * @Assert\Expression(
	 *     "this.getCriticalStorageVpL() <= this.getCriticalStorageVpH()",
	 *     message="Less Than Critical Low"
	 * )
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="45", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalStorageVpH;

	/**
	 * @Assert\NotBlank()
	 * @Assert\Range(min="0", max="45", minMessage="Min Value {{ limit }}", maxMessage="Max Value {{ limit }}")
	 * @ORM\Column(type="float")
	 */
	private $criticalStorageVpL;

	/**
	 * @ORM\Column(type="string")
	 */
	private $uid;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @ORM\Version()
	 * @var \DateTime
	 */
	private $timestamp;


	public function __get($name)
	{
		return $this->$name;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmHeL()
	{
		return $this->alarmHeL;
	}

	/**
	 * @param mixed $alarmHeL
	 */
	public function setAlarmHeL($alarmHeL)
	{
		$this->alarmHeL = $alarmHeL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmHebH()
	{
		return $this->alarmHebH;
	}

	/**
	 * @param mixed $alarmHebH
	 */
	public function setAlarmHebH($alarmHebH)
	{
		$this->alarmHebH = $alarmHebH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmVpH()
	{
		return $this->alarmVpH;
	}

	/**
	 * @param mixed $alarmVpH
	 */
	public function setAlarmVpH($alarmVpH)
	{
		$this->alarmVpH = $alarmVpH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmVpL()
	{
		return $this->alarmVpL;
	}

	/**
	 * @param mixed $alarmVpL
	 */
	public function setAlarmVpL($alarmVpL)
	{
		$this->alarmVpL = $alarmVpL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmTfH()
	{
		return $this->alarmTfH;
	}

	/**
	 * @param mixed $alarmTfH
	 */
	public function setAlarmTfH($alarmTfH)
	{
		$this->alarmTfH = $alarmTfH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmTfL()
	{
		return $this->alarmTfL;
	}

	/**
	 * @param mixed $alarmTfL
	 */
	public function setAlarmTfL($alarmTfL)
	{
		$this->alarmTfL = $alarmTfL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmRhH()
	{
		return $this->alarmRhH;
	}

	/**
	 * @param mixed $alarmRhH
	 */
	public function setAlarmRhH($alarmRhH)
	{
		$this->alarmRhH = $alarmRhH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmRhL()
	{
		return $this->alarmRhL;
	}

	/**
	 * @param mixed $alarmRhL
	 */
	public function setAlarmRhL($alarmRhL)
	{
		$this->alarmRhL = $alarmRhL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmDpH()
	{
		return $this->alarmDpH;
	}

	/**
	 * @param mixed $alarmDpH
	 */
	public function setAlarmDpH($alarmDpH)
	{
		$this->alarmDpH = $alarmDpH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmDpDiff()
	{
		return $this->alarmDpDiff;
	}

	/**
	 * @param mixed $alarmDpDiff
	 */
	public function setAlarmDpDiff($alarmDpDiff)
	{
		$this->alarmDpDiff = $alarmDpDiff;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmWfH()
	{
		return $this->alarmWfH;
	}

	/**
	 * @param mixed $alarmWfH
	 */
	public function setAlarmWfH($alarmWfH)
	{
		$this->alarmWfH = $alarmWfH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmWfL()
	{
		return $this->alarmWfL;
	}

	/**
	 * @param mixed $alarmWfL
	 */
	public function setAlarmWfL($alarmWfL)
	{
		$this->alarmWfL = $alarmWfL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmWtH()
	{
		return $this->alarmWtH;
	}

	/**
	 * @param mixed $alarmWtH
	 */
	public function setAlarmWtH($alarmWtH)
	{
		$this->alarmWtH = $alarmWtH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmWtL()
	{
		return $this->alarmWtL;
	}

	/**
	 * @param mixed $alarmWtL
	 */
	public function setAlarmWtL($alarmWtL)
	{
		$this->alarmWtL = $alarmWtL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmStorageHeL()
	{
		return $this->alarmStorageHeL;
	}

	/**
	 * @param mixed $alarmStorageHeL
	 */
	public function setAlarmStorageHeL($alarmStorageHeL)
	{
		$this->alarmStorageHeL = $alarmStorageHeL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmStorageVpH()
	{
		return $this->alarmStorageVpH;
	}

	/**
	 * @param mixed $alarmStorageVpH
	 */
	public function setAlarmStorageVpH($alarmStorageVpH)
	{
		$this->alarmStorageVpH = $alarmStorageVpH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmStorageVpL()
	{
		return $this->alarmStorageVpL;
	}

	/**
	 * @param mixed $alarmStorageVpL
	 */
	public function setAlarmStorageVpL($alarmStorageVpL)
	{
		$this->alarmStorageVpL = $alarmStorageVpL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalHeL()
	{
		return $this->criticalHeL;
	}

	/**
	 * @param mixed $criticalHeL
	 */
	public function setCriticalHeL($criticalHeL)
	{
		$this->criticalHeL = $criticalHeL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalHebH()
	{
		return $this->criticalHebH;
	}

	/**
	 * @param mixed $criticalHebH
	 */
	public function setCriticalHebH($criticalHebH)
	{
		$this->criticalHebH = $criticalHebH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalVpH()
	{
		return $this->criticalVpH;
	}

	/**
	 * @param mixed $criticalVpH
	 */
	public function setCriticalVpH($criticalVpH)
	{
		$this->criticalVpH = $criticalVpH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalVpL()
	{
		return $this->criticalVpL;
	}

	/**
	 * @param mixed $criticalVpL
	 */
	public function setCriticalVpL($criticalVpL)
	{
		$this->criticalVpL = $criticalVpL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalTfH()
	{
		return $this->criticalTfH;
	}

	/**
	 * @param mixed $criticalTfH
	 */
	public function setCriticalTfH($criticalTfH)
	{
		$this->criticalTfH = $criticalTfH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalTfL()
	{
		return $this->criticalTfL;
	}

	/**
	 * @param mixed $criticalTfL
	 */
	public function setCriticalTfL($criticalTfL)
	{
		$this->criticalTfL = $criticalTfL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalRhH()
	{
		return $this->criticalRhH;
	}

	/**
	 * @param mixed $criticalRhH
	 */
	public function setCriticalRhH($criticalRhH)
	{
		$this->criticalRhH = $criticalRhH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalRhL()
	{
		return $this->criticalRhL;
	}

	/**
	 * @param mixed $criticalRhL
	 */
	public function setCriticalRhL($criticalRhL)
	{
		$this->criticalRhL = $criticalRhL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalDpDiff()
	{
		return $this->criticalDpDiff;
	}

	/**
	 * @param mixed $criticalDpDiff
	 */
	public function setCriticalDpDiff($criticalDpDiff)
	{
		$this->criticalDpDiff = $criticalDpDiff;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalDpH()
	{
		return $this->criticalDpH;
	}

	/**
	 * @param mixed $criticalDpH
	 */
	public function setCriticalDpH($criticalDpH)
	{
		$this->criticalDpH = $criticalDpH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalWfH()
	{
		return $this->criticalWfH;
	}

	/**
	 * @param mixed $criticalWfH
	 */
	public function setCriticalWfH($criticalWfH)
	{
		$this->criticalWfH = $criticalWfH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalWfL()
	{
		return $this->criticalWfL;
	}

	/**
	 * @param mixed $criticalWfL
	 */
	public function setCriticalWfL($criticalWfL)
	{
		$this->criticalWfL = $criticalWfL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalWtH()
	{
		return $this->criticalWtH;
	}

	/**
	 * @param mixed $criticalWtH
	 */
	public function setCriticalWtH($criticalWtH)
	{
		$this->criticalWtH = $criticalWtH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalWtL()
	{
		return $this->criticalWtL;
	}

	/**
	 * @param mixed $criticalWtL
	 */
	public function setCriticalWtL($criticalWtL)
	{
		$this->criticalWtL = $criticalWtL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalStorageHeL()
	{
		return $this->criticalStorageHeL;
	}

	/**
	 * @param mixed $criticalStorageHeL
	 */
	public function setCriticalStorageHeL($criticalStorageHeL)
	{
		$this->criticalStorageHeL = $criticalStorageHeL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalStorageVpH()
	{
		return $this->criticalStorageVpH;
	}

	/**
	 * @param mixed $criticalStorageVpH
	 */
	public function setCriticalStorageVpH($criticalStorageVpH)
	{
		$this->criticalStorageVpH = $criticalStorageVpH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalStorageVpL()
	{
		return $this->criticalStorageVpL;
	}

	/**
	 * @param mixed $criticalStorageVpL
	 */
	public function setCriticalStorageVpL($criticalStorageVpL)
	{
		$this->criticalStorageVpL = $criticalStorageVpL;
	}

	/**
	 * @return mixed
	 */
	public function getUid()
	{
		return $this->uid;
	}

	/**
	 * @param mixed $uid
	 */
	public function setUid($uid)
	{
		$this->uid = $uid;
	}


}