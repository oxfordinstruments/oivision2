<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 10/11/2017
 * Time: 11:06 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Indaxia\OTR\ITransformable;
use Indaxia\OTR\Traits\Transformable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SystemsDataGeRepository")
 * @ORM\Table(name="systems_data_ge")
 */
class SystemsDataGeEntity implements ITransformable {
	use Transformable;
	/**
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SystemsEntity")
	 */
	private $system;

	/**
	 * Mac address of the RDC
	 * @Assert\NotBlank()
	 * @Assert\Regex(
	 *     pattern="/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/",
	 *     message="Valid XX-XX-XX-XX-XX-XX"
	 * )
	 * @ORM\Column(type="string", length=50)
	 */
	private $mac_rdc;

	/**
	 * Mac address of the OIV
	 * @Assert\NotBlank()
	 * @Assert\Regex(
	 *     pattern="/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/",
	 *     message="Valid XX-XX-XX-XX-XX-XX"
	 * )
	 * @ORM\Column(type="string", length=50)
	 */
	private $mac_oiv;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ApiKeyEntity")
	 */
	private $apiKey;

	/**
	 * @ORM\Column(type="float", precision=10, scale=0)
	 */
	private $tf;

	/**
	 * @ORM\Column(type="float", precision=10, scale=0)
	 */
	private $rh;

	/**
	 * @ORM\Column(type="float", precision=10, scale=0)
	 */
	private $dp;

	/**
	 * @ORM\Column(type="float", precision=10, scale=0)
	 */
	private $dpdiff;

	/**
	 * @ORM\Column(type="float", precision=10, scale=0)
	 */
	private $he;

	/**
	 * @ORM\Column(type="float", precision=10, scale=0)
	 */
	private $vp;

	/**
	 * @ORM\Column(type="float", precision=10, scale=0)
	 */
	private $wf;

	/**
	 * @ORM\Column(type="float", precision=10, scale=0)
	 */
	private $wt;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $q;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $cmp;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $tfa = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $rha = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $dpa = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $dpda = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hea = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $vpa = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $wfa = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $wta = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $tfc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $rhc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $dpc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $dpdc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hec = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $vpc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $wfc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $wtc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $qc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $cmpc = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $inStorage = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hasAlarm = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $hasCriticalAlarm = false;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $timestamp;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $alarmProcessed = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $criticalProcessed = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $pulledData = false;

	private $alarmsString;

	private $criticalsString;

	/**
	 * Triggered on insert
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		if(is_null($this->timestamp)){
			$this->timestamp = new \DateTime("now");
		}
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return SystemsEntity
	 */
	public function getSystem()
	{
		return $this->system;
	}

	/**
	 * @param SystemsEntity $system
	 */
	public function setSystem(SystemsEntity $system)
	{
		$this->system = $system;
	}

	/**
	 * @return string
	 */
	public function getMacRdc()
	{
		return $this->mac_rdc;
	}

	/**
	 * @param string $mac_rdc
	 */
	public function setMacRdc($mac_rdc)
	{
		$this->mac_rdc = $mac_rdc;
	}

	/**
	 * @return mixed
	 */
	public function getMacOiv()
	{
		return $this->mac_oiv;
	}

	/**
	 * @param mixed $mac_oiv
	 */
	public function setMacOiv($mac_oiv)
	{
		$this->mac_oiv = $mac_oiv;
	}


	/**
	 * @return ApiKeyEntity
	 */
	public function getApiKey()
	{
		return $this->apiKey;
	}

	/**
	 * @param ApiKeyEntity $apiKey
	 */
	public function setApiKey(ApiKeyEntity $apiKey)
	{
		$this->apiKey = $apiKey;
	}

	/**
	 * @return float
	 */
	public function getTf()
	{
		return $this->tf;
	}

	/**
	 * @param float $tf
	 */
	public function setTf($tf)
	{
		$this->tf = round($tf, 2);
	}

	/**
	 * @return float
	 */
	public function getRh()
	{
		return $this->rh;
	}

	/**
	 * @param float $rh
	 */
	public function setRh($rh)
	{
		$this->rh = round($rh, 2);
	}

	/**
	 * @return mixed
	 */
	public function getDp()
	{
		return $this->dp;
	}

	/**
	 * @param mixed $dp
	 */
	public function setDp($dp)
	{
		$this->dp = round($dp, 2);
	}

	/**
	 * @return mixed
	 */
	public function getDpdiff()
	{
		return $this->dpdiff;
	}

	/**
	 * @param mixed $dpdiff
	 */
	public function setDpdiff($dpdiff)
	{
		$this->dpdiff = round($dpdiff, 2);
	}

	/**
	 * @return float
	 */
	public function getHe()
	{
		return $this->he;
	}

	/**
	 * @param float $he
	 */
	public function setHe($he)
	{
		$this->he = round($he, 2);
	}

	/**
	 * @return float
	 */
	public function getVp()
	{
		return $this->vp;
	}

	/**
	 * @param float $vp
	 */
	public function setVp($vp)
	{
		$this->vp = round($vp, 2);
	}

	/**
	 * @return float
	 */
	public function getWf()
	{
		return $this->wf;
	}

	/**
	 * @param float $wf
	 */
	public function setWf($wf)
	{
		$this->wf = round($wf, 2);
	}

	/**
	 * @return float
	 */
	public function getWt()
	{
		return $this->wt;
	}

	/**
	 * @param float $wt
	 */
	public function setWt($wt)
	{
		$this->wt = round($wt, 2);
	}

	/**
	 * @return bool
	 */
	public function getQ()
	{
		return $this->q;
	}

	/**
	 * @param bool $q
	 */
	public function setQ($q)
	{
		$this->q = $q;
	}

	/**
	 * @return bool
	 */
	public function getCmp()
	{
		return $this->cmp;
	}

	/**
	 * @param bool $cmp
	 */
	public function setCmp($cmp)
	{
		$this->cmp = $cmp;
	}

	/**
	 * @return bool
	 */
	public function getInStorage()
	{
		return $this->inStorage;
	}

	/**
	 * @param bool $inStorage
	 */
	public function setInStorage($inStorage)
	{
		$this->inStorage = $inStorage;
	}

	/**
	 * @return bool
	 */
	public function getHasAlarm()
	{
		return $this->hasAlarm;
	}

	/**
	 * @param bool $hasAlarm
	 */
	public function setHasAlarm($hasAlarm)
	{
		$this->hasAlarm = $hasAlarm;
	}

	/**
	 * @return bool
	 */
	public function getHasCriticalAlarm()
	{
		return $this->hasCriticalAlarm;
	}

	/**
	 * @param bool $hasCriticalAlarm
	 */
	public function setHasCriticalAlarm($hasCriticalAlarm)
	{
		$this->hasCriticalAlarm = $hasCriticalAlarm;
	}

	/**
	 * @return bool
	 */
	public function getTfa()
	{
		return $this->tfa;
	}

	/**
	 * @param bool $tfa
	 */
	public function setTfa($tfa)
	{
		$this->tfa = $tfa;
	}

	/**
	 * @return bool
	 */
	public function getRha()
	{
		return $this->rha;
	}

	/**
	 * @param bool $rha
	 */
	public function setRha($rha)
	{
		$this->rha = $rha;
	}

	/**
	 * @return mixed
	 */
	public function getDpa()
	{
		return $this->dpa;
	}

	/**
	 * @param mixed $dpa
	 */
	public function setDpa($dpa)
	{
		$this->dpa = $dpa;
	}

	/**
	 * @return mixed
	 */
	public function getDpda()
	{
		return $this->dpda;
	}

	/**
	 * @param mixed $dpda
	 */
	public function setDpda($dpda)
	{
		$this->dpda = $dpda;
	}

	/**
	 * @return bool
	 */
	public function getHea()
	{
		return $this->hea;
	}

	/**
	 * @param bool $hea
	 */
	public function setHea($hea)
	{
		$this->hea = $hea;
	}

	/**
	 * @return bool
	 */
	public function getVpa()
	{
		return $this->vpa;
	}

	/**
	 * @param bool $vpa
	 */
	public function setVpa($vpa)
	{
		$this->vpa = $vpa;
	}

	/**
	 * @return bool
	 */
	public function getWfa()
	{
		return $this->wfa;
	}

	/**
	 * @param bool $wfa
	 */
	public function setWfa($wfa)
	{
		$this->wfa = $wfa;
	}

	/**
	 * @return bool
	 */
	public function getWta()
	{
		return $this->wta;
	}

	/**
	 * @param bool $wta
	 */
	public function setWta($wta)
	{
		$this->wta = $wta;
	}

	/**
	 * @return bool
	 */
	public function getTfc()
	{
		return $this->tfc;
	}

	/**
	 * @param bool $tfc
	 */
	public function setTfc($tfc)
	{
		$this->tfc = $tfc;
	}

	/**
	 * @return bool
	 */
	public function getRhc()
	{
		return $this->rhc;
	}

	/**
	 * @param bool $rhc
	 */
	public function setRhc($rhc)
	{
		$this->rhc = $rhc;
	}

	/**
	 * @return mixed
	 */
	public function getDpc()
	{
		return $this->dpc;
	}

	/**
	 * @param mixed $dpc
	 */
	public function setDpc($dpc)
	{
		$this->dpc = $dpc;
	}

	/**
	 * @return mixed
	 */
	public function getDpdc()
	{
		return $this->dpdc;
	}

	/**
	 * @param mixed $dpdc
	 */
	public function setDpdc($dpdc)
	{
		$this->dpdc = $dpdc;
	}

	/**
	 * @return bool
	 */
	public function getHec()
	{
		return $this->hec;
	}

	/**
	 * @param bool $hec
	 */
	public function setHec($hec)
	{
		$this->hec = $hec;
	}

	/**
	 * @return bool
	 */
	public function getVpc()
	{
		return $this->vpc;
	}

	/**
	 * @param bool $vpc
	 */
	public function setVpc($vpc)
	{
		$this->vpc = $vpc;
	}

	/**
	 * @return bool
	 */
	public function getWfc()
	{
		return $this->wfc;
	}

	/**
	 * @param bool $wfc
	 */
	public function setWfc($wfc)
	{
		$this->wfc = $wfc;
	}

	/**
	 * @return bool
	 */
	public function getWtc()
	{
		return $this->wtc;
	}

	/**
	 * @param bool $wtc
	 */
	public function setWtc($wtc)
	{
		$this->wtc = $wtc;
	}

	/**
	 * @return bool
	 */
	public function getQc()
	{
		return $this->qc;
	}

	/**
	 * @param bool $qc
	 */
	public function setQc($qc)
	{
		$this->qc = $qc;
	}

	/**
	 * @return bool
	 */
	public function getCmpc()
	{
		return $this->cmpc;
	}

	/**
	 * @param bool $cmpc
	 */
	public function setCmpc($cmpc)
	{
		$this->cmpc = $cmpc;
	}

	/**
	 * @param \DateTime $timestamp
	 */
	public function setTimestamp($timestamp)
	{
		$this->timestamp = $timestamp;
	}

	/**
	 * @return \DateTime
	 */
	public function getTimestamp()
	{
		return $this->timestamp;
	}


	/**
	 * @return boolean
	 */
	public function getAlarmProcessed()
	{
		return $this->alarmProcessed;
	}

	/**
	 * @param boolean $alarmProcessed
	 */
	public function setAlarmProcessed($alarmProcessed)
	{
		$this->alarmProcessed = $alarmProcessed;
	}

	/**
	 * @return boolean
	 */
	public function getPulledData()
	{
		return $this->pulledData;
	}

	/**
	 * @param boolean $pulledData
	 */
	public function setPulledData($pulledData)
	{
		$this->pulledData = $pulledData;
	}

	/**
	 * @return string
	 */
	public function getAlarmsString()
	{
		return $this->alarmsString;
	}

	/**
	 * @param string $alarmsString
	 */
	public function setAlarmsString($alarmsString)
	{
		$this->alarmsString = $alarmsString;
	}

	/**
	 * @return string
	 */
	public function getCriticalsString()
	{
		return $this->criticalsString;
	}

	/**
	 * @param string $criticalsString
	 */
	public function setCriticalsString($criticalsString)
	{
		$this->criticalsString = $criticalsString;
	}

	/**
	 * @return boolean
	 */
	public function getCriticalProcessed()
	{
		return $this->criticalProcessed;
	}

	/**
	 * @param boolean $criticalProcessed
	 */
	public function setCriticalProcessed($criticalProcessed)
	{
		$this->criticalProcessed = $criticalProcessed;
	}



}