<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 3:48 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class StatesEntity
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LdapGroupsRepository")
 * @ORM\Table(name="misc_ldap_groups")
 */
class LdapGroupsEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="integer", length=10)
	 * @Assert\NotBlank()
	 */
	private $gid;

	/**
	 * @ORM\Column(type="string")
	 * @Assert\NotBlank()
	 */
	private $roles;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getGid()
	{
		return $this->gid;
	}

	/**
	 * @param mixed $gid
	 */
	public function setGid($gid)
	{
		$this->gid = $gid;
	}

	/**
	 * @return mixed
	 */
	public function getRoles()
	{
		return $this->roles;
	}

	/**
	 * @param mixed $roles
	 */
	public function setRoles($roles)
	{
		$this->roles = $roles;
	}


}