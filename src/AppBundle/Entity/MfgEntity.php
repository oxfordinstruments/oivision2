<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/20/2017
 * Time: 5:11 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="misc_mfg")
 */
class MfgEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string")
	 */
	private $abv;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string")
	 */
	private $name;

	/**
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param integer $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getAbv()
	{
		return $this->abv;
	}

	/**
	 * @param string $abv
	 */
	public function setAbv($abv)
	{
		$this->abv = $abv;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}


}