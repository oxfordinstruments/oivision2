<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/19/2017
 * Time: 7:26 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HelloRepository")
 * @ORM\Table(name="hello")
 */
class HelloEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $mac_oiv;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $mac_rdc;


	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $version;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $revision;


	/**
	 * @ORM\Column(type="integer", nullable=true, options={"default": 0})
	 */
	private $system;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $timestamp;

	/**
	 * Triggered on insert
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		if(is_null($this->timestamp)){
			$this->timestamp = new \DateTime("now");
		}
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getMacOiv()
	{
		return $this->mac_oiv;
	}

	/**
	 * @param mixed $mac
	 */
	public function setMacOiv($mac)
	{
		$this->mac_oiv = $mac;
	}

	/**
	 * @return mixed
	 */
	public function getMacRdc()
	{
		return $this->mac_rdc;
	}

	/**
	 * @param mixed $mac
	 */
	public function setMacRdc($mac_rdc)
	{
		$this->mac_rdc = $mac_rdc;
	}

	/**
	 * @return mixed
	 */
	public function getVersion()
	{
		return $this->version;
	}

	/**
	 * @param mixed $version
	 */
	public function setVersion($version)
	{
		$this->version = $version;
	}

	/**
	 * @return mixed
	 */
	public function getSystem()
	{
		return $this->system;
	}

	/**
	 * @param mixed $system
	 */
	public function setSystem($system)
	{
		$this->system = $system;
	}

	/**
	 * @param \DateTime $timestamp
	 */
	public function setTimestamp($timestamp)
	{
		$this->timestamp = $timestamp;
	}

	/**
	 * @return \DateTime
	 */
	public function getTimestamp()
	{
		return $this->timestamp;
	}

	/**
	 * @return mixed
	 */
	public function getRevision()
	{
		return $this->revision;
	}

	/**
	 * @param mixed $revision
	 */
	public function setRevision($revision)
	{
		$this->revision = $revision;
	}



}