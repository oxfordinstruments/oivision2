<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 11/6/2017
 * Time: 11:53 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Indaxia\OTR\ITransformable;
use Indaxia\OTR\Traits\Transformable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CustomerRepository")
 * @ORM\Table(name="customer")
 */
class CustomerEntity implements ITransformable {
	use Transformable;
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $id;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $enabled = true;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", unique=true)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	private $address;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	private $city;

	/**
	 * @ORM\Column(type="string", length=50, nullable=true)
	 * @var string
	 */
	private $state;

	/**
	 * @ORM\Column(type="string", length=10, nullable=true)
	 * @var string
	 */
	private $zip;

	/**
	 * @ORM\Column(type="string", length=20, nullable=true)
	 * @var string
	 */
	private $phone;

	/**
	 * @Assert\Email(
	 *     message = "'{{ value }}' is not a valid email.",
	 *     checkMX = true
	 * )
	 * @ORM\Column(type="string", nullable=true)
	 * @var string
	 */
	private $email;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 * @var string
	 */
	private $comment;


	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return bool
	 */
	public function isEnabled(): bool
	{
		return $this->enabled;
	}

	/**
	 * @param bool $enabled
	 */
	public function setEnabled(bool $enabled)
	{
		$this->enabled = $enabled;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function setAddress(string $address)
	{
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity(string $city)
	{
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @param string $state
	 */
	public function setState(string $state)
	{
		$this->state = $state;
	}

	/**
	 * @return string
	 */
	public function getZip()
	{
		return $this->zip;
	}

	/**
	 * @param string $zip
	 */
	public function setZip(string $zip)
	{
		$this->zip = $zip;
	}

	/**
	 * @return string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone(string $phone)
	{
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail(string $email)
	{
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * @param string $comment
	 */
	public function setComment(string $comment)
	{
		$this->comment = $comment;
	}



}