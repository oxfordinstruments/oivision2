<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/19/2017
 * Time: 5:14 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Indaxia\OTR\ITransformable;
use Indaxia\OTR\Traits\Transformable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApiKeyRepository")
 * @ORM\Table(name="api_key")
 */
class ApiKeyEntity implements ITransformable {
	use Transformable;
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string")
	 */
	private $token;

	/**
	 * @ORM\OneToOne(targetEntity="AppBundle\Entity\CustomerEntity")
	 */
	private $customerId;

	/**
	 * @ORM\Column(type="string")
	 */
	private $created;

	/**
	 * @ORM\Column(type="string")
	 */
	private $expire;


	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getToken()
	{
		return $this->token;
	}

	/**
	 * @param mixed $token
	 */
	public function setToken($token)
	{
		$this->token = $token;
	}

	/**
	 * @return mixed
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @param mixed $created
	 */
	public function setCreated($created)
	{
		$this->created = $created;
	}

	/**
	 * @return mixed
	 */
	public function getExpire()
	{
		return $this->expire;
	}

	/**
	 * @param mixed $expire
	 */
	public function setExpire($expire)
	{
		$this->expire = $expire;
	}

	/**
	 * @return CustomerEntity
	 */
	public function getCustomerId()
	{
		return $this->customerId;
	}

	/**
	 * @param CustomerEntity $customerId
	 */
	public function setCustomerId($customerId)
	{
		$this->customerId = $customerId;
	}


}