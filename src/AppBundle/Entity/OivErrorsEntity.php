<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 2/5/2018
 * Time: 11:59 AM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
* @ORM\Entity(repositoryClass="AppBundle\Repository\OivErrorsRepository")
* @ORM\Table(name="oiv_errors")
 **/
class OivErrorsEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * Mac address of the oiv
	 * @Assert\NotBlank()
	 * @Assert\Regex(
	 *     pattern="/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/",
	 *     message="Valid XX-XX-XX-XX-XX-XX"
	 * )
	 * @ORM\Column(type="string", length=50)
	 */
	private $mac_oiv;

	/**
	 * Mac address of the oiv
	 * @Assert\NotBlank()
	 * @Assert\Regex(
	 *     pattern="/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/",
	 *     message="Valid XX-XX-XX-XX-XX-XX"
	 * )
	 * @ORM\Column(type="string", length=50)
	 */
	private $mac_rdc;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="datetime")
	 */
	private $error_timestamp;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string")
	 */
	private $error;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="datetime")
	 */
	private $timestamp;

	/**
	 * Triggered on insert
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		if(is_null($this->timestamp)){
			$this->timestamp = new \DateTime("now");
		}
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getMacOiv()
	{
		return $this->mac_oiv;
	}

	/**
	 * @param string $mac_oiv
	 */
	public function setMacOiv($mac_oiv)
	{
		$this->mac_oiv = $mac_oiv;
	}

	/**
	 * @return mixed
	 */
	public function getTimestamp()
	{
		return $this->timestamp;
	}

	/**
	 * @param mixed $timestamp
	 */
	public function setTimestamp($timestamp)
	{
		$this->timestamp = $timestamp;
	}

	/**
	 * @return string
	 */
	public function getError()
	{
		return $this->error;
	}

	/**
	 * @param string $error
	 */
	public function setError($error)
	{
		$this->error = $error;
	}

	/**
	 * @return mixed
	 */
	public function getErrorTimestamp()
	{
		return $this->error_timestamp;
	}

	/**
	 * @param mixed $error_timestamp
	 */
	public function setErrorTimestamp($error_timestamp)
	{
		$this->error_timestamp = $error_timestamp;
	}

	/**
	 * @return mixed
	 */
	public function getMacRdc()
	{
		return $this->mac_rdc;
	}

	/**
	 * @param mixed $mac_rdc
	 */
	public function setMacRdc($mac_rdc)
	{
		$this->mac_rdc = $mac_rdc;
	}



}