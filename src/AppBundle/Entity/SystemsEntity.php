<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/19/2017
 * Time: 7:19 PM
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Indaxia\OTR\ITransformable;
use Indaxia\OTR\Traits\Transformable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @UniqueEntity("mac_rdc")
 * @UniqueEntity("systemId")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SystemsRepository")
 * @ORM\Table(name="systems")
 * @ORM\HasLifecycleCallbacks()
 */
class SystemsEntity implements ITransformable {
	use Transformable;

	private $alarmSettingsUpdated_vars = [
		'monitorHe',
		'monitorHeb',
		'monitorVp',
		'monitorTf',
		'monitorRh',
		'monitorWf',
		'monitorWt',
		'monitorQ',
		'monitorCmp',
		'inStorage',
		'alarmHeL',
		'alarmHebH',
		'alarmVpH',
		'alarmVpL',
		'alarmTfH',
		'alarmTfL',
		'alarmRhH',
		'alarmRhL',
		'alarmWfH',
		'alarmWfL',
		'alarmWtH',
		'alarmWtL',
		'alarmStorageHeL',
		'alarmStorageVpH',
		'alarmStorageVpL',
		'criticalHeL',
		'criticalHebH',
		'criticalVpH',
		'criticalVpL',
		'criticalTfH',
		'criticalTfL',
		'criticalRhH',
		'criticalRhL',
		'criticalWfH',
		'criticalWfL',
		'criticalWtH',
		'criticalWtL',
		'criticalStorageHeL',
		'criticalStorageVpH',
		'criticalStorageVpL',
		'offsetHe',
		'offsetVp',
		'offsetWf',
		'offsetWtf',
		'offsetTc',
		'offsetRh'
	];

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * Mac address of the RDC
	 * @Assert\NotBlank()
	 * @Assert\Regex(
	 *     pattern="/([0-9|A-F|a-f]{2}(-|:)){5}[0-9|A-F|a-f]{2}/",
	 *     message="Valid XX-XX-XX-XX-XX-XX"
	 * )
	 * @ORM\Column(type="string", length=50, unique=true)
	 */
	private $mac_rdc;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $enabled = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $registered = true;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", length=10, unique=true)
	 */
	private $systemId;

	/**
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", unique=true)
	 */
	private $systemName;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $systemAddress;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	private $systemCity;

	/**
	 * @ORM\Column(type="string", length=50, nullable=true)
	 */
	private $systemState;

	/**
	 * @ORM\Column(type="string", length=10, nullable=true)
	 */
	private $systemZip;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CustomerEntity")
	 */
	private $customerId;

	/**
	 * @ORM\Column(type="string", length=20, nullable=true)
	 */
	private $alarmSettingsUpdated;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorHe = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorHeb = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorVp = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorTf = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorRh = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorWf = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorWt = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorQ = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorCmp = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorDp = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $monitorDpDiff = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $inStorage = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $avgHe = true;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $avgVp = true;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $notes;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $useGlobalAlarms = true;

	private $data;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmHeL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmHebH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmVpH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmVpL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmTfH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmTfL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmRhH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmRhL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmDpH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmDpDiff;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmWfH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmWfL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmWtH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmWtL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmStorageHeL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmStorageVpH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $alarmStorageVpL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalHeL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalHebH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalVpH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalVpL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalTfH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalTfL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalRhH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalRhL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalDpH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalDpDiff;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalWfH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalWfL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalWtH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalWtL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalStorageHeL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalStorageVpH;

	/**
	 * @ORM\Column(type="float")
	 */
	private $criticalStorageVpL;

	/**
	 * @ORM\Column(type="float")
	 */
	private $offsetHe;

	/**
	 * @ORM\Column(type="float")
	 */
	private $offsetVp;

	/**
	 * @ORM\Column(type="float")
	 */
	private $offsetTc;

	/**
	 * @ORM\Column(type="float")
	 */
	private $offsetRh;

	/**
	 * @ORM\Column(type="float")
	 */
	private $offsetWf;

	/**
	 * @ORM\Column(type="float")
	 */
	private $offsetWtf;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $oivOld = false;

	/**
	 * Triggered on update
	 * @ORM\PreUpdate
	 * @param PreUpdateEventArgs $eventArgs
	 */
	public function preUpdate(PreUpdateEventArgs $eventArgs){

		$arrEventArgs = (array)$eventArgs->getEntityChangeSet();
		foreach (array_keys($arrEventArgs) as $key){
			if(in_array($key, $this->alarmSettingsUpdated_vars)){
				/**
				 * @var SystemsEntity $obj
				 */
				$obj = $eventArgs->getObject();
				$obj->alarmSettingsUpdated = date_format(new \DateTime(), 'Y-m-d H:i:s');
				break;
			}
		}
	}

//	/**
//	 * Triggered on insert
//	 * @ORM\PrePersist
//	 */
//	public function prePersist(){
//		dump("PERSIST");
//	}


	public function __set($name, $value)
	{
		$this->$name = $value;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getMacRdc()
	{
		return $this->mac_rdc;
	}

	/**
	 * @param mixed $macrdc
	 */
	public function setMacRdc($mac_rdc)
	{
		$this->mac_rdc = $mac_rdc;
	}

	/**
	 * @return mixed
	 */
	public function getEnabled()
	{
		return $this->enabled;
	}

	/**
	 * @param mixed $enabled
	 */
	public function setEnabled($enabled)
	{
		$this->enabled = $enabled;
	}

	/**
	 * @return mixed
	 */
	public function getRegistered()
	{
		return $this->registered;
	}

	/**
	 * @param mixed $registered
	 */
	public function setRegistered($registered)
	{
		$this->registered = $registered;
	}

	/**
	 * @return mixed
	 */
	public function getSystemId()
	{
		return $this->systemId;
	}

	/**
	 * @param mixed $systemId
	 */
	public function setSystemId($systemId)
	{
		$this->systemId = $systemId;
	}

	/**
	 * @return mixed
	 */
	public function getSystemName()
	{
		return $this->systemName;
	}

	/**
	 * @param mixed $systemName
	 */
	public function setSystemName($systemName)
	{
		$this->systemName = $systemName;
	}

	/**
	 * @return mixed
	 */
	public function getSystemAddress()
	{
		return $this->systemAddress;
	}

	/**
	 * @param mixed $systemAddress
	 */
	public function setSystemAddress($systemAddress)
	{
		$this->systemAddress = $systemAddress;
	}

	/**
	 * @return mixed
	 */
	public function getSystemCity()
	{
		return $this->systemCity;
	}

	/**
	 * @param mixed $systemCity
	 */
	public function setSystemCity($systemCity)
	{
		$this->systemCity = $systemCity;
	}

	/**
	 * @return mixed
	 */
	public function getSystemState()
	{
		return $this->systemState;
	}

	/**
	 * @param mixed $systemState
	 */
	public function setSystemState($systemState)
	{
		$this->systemState = $systemState;
	}

	/**
	 * @return mixed
	 */
	public function getSystemZip()
	{
		return $this->systemZip;
	}

	/**
	 * @param mixed $systemZip
	 */
	public function setSystemZip($systemZip)
	{
		$this->systemZip = $systemZip;
	}

	/**
	 * @return CustomerEntity
	 */
	public function getCustomerId()
	{
		return $this->customerId;
	}

	/**
	 * @param CustomerEntity $customerId
	 */
	public function setCustomerId(CustomerEntity $customerId)
	{
		$this->customerId = $customerId;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmSettingsUpdated()
	{
		return $this->alarmSettingsUpdated;
	}

	/**
	 * @param mixed $alarmSettingsUpdated
	 */
	public function setAlarmSettingsUpdated($alarmSettingsUpdated)
	{
		$this->alarmSettingsUpdated = $alarmSettingsUpdated;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorHe()
	{
		return $this->monitorHe;
	}

	/**
	 * @param mixed $monitorHe
	 */
	public function setMonitorHe($monitorHe)
	{
		$this->monitorHe = $monitorHe;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorHeb()
	{
		return $this->monitorHeb;
	}

	/**
	 * @param mixed $monitorHeb
	 */
	public function setMonitorHeb($monitorHeb)
	{
		$this->monitorHeb = $monitorHeb;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorVp()
	{
		return $this->monitorVp;
	}

	/**
	 * @param mixed $monitorVp
	 */
	public function setMonitorVp($monitorVp)
	{
		$this->monitorVp = $monitorVp;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorTf()
	{
		return $this->monitorTf;
	}

	/**
	 * @param mixed $monitorTf
	 */
	public function setMonitorTf($monitorTf)
	{
		$this->monitorTf = $monitorTf;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorRh()
	{
		return $this->monitorRh;
	}

	/**
	 * @param mixed $monitorRh
	 */
	public function setMonitorRh($monitorRh)
	{
		$this->monitorRh = $monitorRh;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorDp()
	{
		return $this->monitorDp;
	}

	/**
	 * @param mixed $monitorDp
	 */
	public function setMonitorDp($monitorDp)
	{
		$this->monitorDp = $monitorDp;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorDpDiff()
	{
		return $this->monitorDpDiff;
	}

	/**
	 * @param mixed $monitorDpDiff
	 */
	public function setMonitorDpDiff($monitorDpDiff)
	{
		$this->monitorDpDiff = $monitorDpDiff;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorWf()
	{
		return $this->monitorWf;
	}

	/**
	 * @param mixed $monitorWf
	 */
	public function setMonitorWf($monitorWf)
	{
		$this->monitorWf = $monitorWf;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorWt()
	{
		return $this->monitorWt;
	}

	/**
	 * @param mixed $monitorWt
	 */
	public function setMonitorWt($monitorWt)
	{
		$this->monitorWt = $monitorWt;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorQ()
	{
		return $this->monitorQ;
	}

	/**
	 * @param mixed $monitorQ
	 */
	public function setMonitorQ($monitorQ)
	{
		$this->monitorQ = $monitorQ;
	}

	/**
	 * @return mixed
	 */
	public function getMonitorCmp()
	{
		return $this->monitorCmp;
	}

	/**
	 * @param mixed $monitorCmp
	 */
	public function setMonitorCmp($monitorCmp)
	{
		$this->monitorCmp = $monitorCmp;
	}

	/**
	 * @return mixed
	 */
	public function getInStorage()
	{
		return $this->inStorage;
	}

	/**
	 * @param mixed $inStorage
	 */
	public function setInStorage($inStorage)
	{
		$this->inStorage = $inStorage;
	}

	/**
	 * @return mixed
	 */
	public function getAvgHe()
	{
		return $this->avgHe;
	}

	/**
	 * @param bool $avgHe
	 */
	public function setAvgHe($avgHe)
	{
		$this->avgHe = $avgHe;
	}

	/**
	 * @return mixed
	 */
	public function getAvgVp()
	{
		return $this->avgVp;
	}

	/**
	 * @param bool $avgVp
	 */
	public function setAvgVp($avgVp)
	{
		$this->avgVp = $avgVp;
	}

	/**
	 * @return mixed
	 */
	public function getNotes()
	{
		return $this->notes;
	}

	/**
	 * @param mixed $notes
	 */
	public function setNotes($notes)
	{
		$this->notes = $notes;
	}

	/**
	 * @return mixed
	 */
	public function getUseGlobalAlarms()
	{
		return $this->useGlobalAlarms;
	}

	/**
	 * @param mixed $useGlobalAlarms
	 */
	public function setUseGlobalAlarms($useGlobalAlarms)
	{
		$this->useGlobalAlarms = $useGlobalAlarms;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmHeL()
	{
		return $this->alarmHeL;
	}

	/**
	 * @param mixed $alarmHeL
	 */
	public function setAlarmHeL($alarmHeL)
	{
		$this->alarmHeL = $alarmHeL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmHebH()
	{
		return $this->alarmHebH;
	}

	/**
	 * @param mixed $alarmHebH
	 */
	public function setAlarmHebH($alarmHebH)
	{
		$this->alarmHebH = $alarmHebH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmVpH()
	{
		return $this->alarmVpH;
	}

	/**
	 * @param mixed $alarmVpH
	 */
	public function setAlarmVpH($alarmVpH)
	{
		$this->alarmVpH = $alarmVpH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmVpL()
	{
		return $this->alarmVpL;
	}

	/**
	 * @param mixed $alarmVpL
	 */
	public function setAlarmVpL($alarmVpL)
	{
		$this->alarmVpL = $alarmVpL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmTfH()
	{
		return $this->alarmTfH;
	}

	/**
	 * @param mixed $alarmTfH
	 */
	public function setAlarmTfH($alarmTfH)
	{
		$this->alarmTfH = $alarmTfH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmTfL()
	{
		return $this->alarmTfL;
	}

	/**
	 * @param mixed $alarmTfL
	 */
	public function setAlarmTfL($alarmTfL)
	{
		$this->alarmTfL = $alarmTfL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmRhH()
	{
		return $this->alarmRhH;
	}

	/**
	 * @param mixed $alarmRhH
	 */
	public function setAlarmRhH($alarmRhH)
	{
		$this->alarmRhH = $alarmRhH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmRhL()
	{
		return $this->alarmRhL;
	}

	/**
	 * @param mixed $alarmRhL
	 */
	public function setAlarmRhL($alarmRhL)
	{
		$this->alarmRhL = $alarmRhL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmDpH()
	{
		return $this->alarmDpH;
	}

	/**
	 * @param mixed $alarmDpH
	 */
	public function setAlarmDpH($alarmDpH)
	{
		$this->alarmDpH = $alarmDpH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmDpDiff()
	{
		return $this->alarmDpDiff;
	}

	/**
	 * @param mixed $alarmDpDiff
	 */
	public function setAlarmDpDiff($alarmDpDiff)
	{
		$this->alarmDpDiff = $alarmDpDiff;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmWfH()
	{
		return $this->alarmWfH;
	}

	/**
	 * @param mixed $alarmWfH
	 */
	public function setAlarmWfH($alarmWfH)
	{
		$this->alarmWfH = $alarmWfH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmWfL()
	{
		return $this->alarmWfL;
	}

	/**
	 * @param mixed $alarmWfL
	 */
	public function setAlarmWfL($alarmWfL)
	{
		$this->alarmWfL = $alarmWfL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmWtH()
	{
		return $this->alarmWtH;
	}

	/**
	 * @param mixed $alarmWtH
	 */
	public function setAlarmWtH($alarmWtH)
	{
		$this->alarmWtH = $alarmWtH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmWtL()
	{
		return $this->alarmWtL;
	}

	/**
	 * @param mixed $alarmWtL
	 */
	public function setAlarmWtL($alarmWtL)
	{
		$this->alarmWtL = $alarmWtL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmStorageHeL()
	{
		return $this->alarmStorageHeL;
	}

	/**
	 * @param mixed $alarmStorageHeL
	 */
	public function setAlarmStorageHeL($alarmStorageHeL)
	{
		$this->alarmStorageHeL = $alarmStorageHeL;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmStorageVpH()
	{
		return $this->alarmStorageVpH;
	}

	/**
	 * @param mixed $alarmStorageVpH
	 */
	public function setAlarmStorageVpH($alarmStorageVpH)
	{
		$this->alarmStorageVpH = $alarmStorageVpH;
	}

	/**
	 * @return mixed
	 */
	public function getAlarmStorageVpL()
	{
		return $this->alarmStorageVpL;
	}

	/**
	 * @param mixed $alarmStorageVpL
	 */
	public function setAlarmStorageVpL($alarmStorageVpL)
	{
		$this->alarmStorageVpL = $alarmStorageVpL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalHeL()
	{
		return $this->criticalHeL;
	}

	/**
	 * @param mixed $criticalHeL
	 */
	public function setCriticalHeL($criticalHeL)
	{
		$this->criticalHeL = $criticalHeL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalHebH()
	{
		return $this->criticalHebH;
	}

	/**
	 * @param mixed $criticalHebH
	 */
	public function setCriticalHebH($criticalHebH)
	{
		$this->criticalHebH = $criticalHebH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalVpH()
	{
		return $this->criticalVpH;
	}

	/**
	 * @param mixed $criticalVpH
	 */
	public function setCriticalVpH($criticalVpH)
	{
		$this->criticalVpH = $criticalVpH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalVpL()
	{
		return $this->criticalVpL;
	}

	/**
	 * @param mixed $criticalVpL
	 */
	public function setCriticalVpL($criticalVpL)
	{
		$this->criticalVpL = $criticalVpL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalTfH()
	{
		return $this->criticalTfH;
	}

	/**
	 * @param mixed $criticalTfH
	 */
	public function setCriticalTfH($criticalTfH)
	{
		$this->criticalTfH = $criticalTfH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalTfL()
	{
		return $this->criticalTfL;
	}

	/**
	 * @param mixed $criticalTfL
	 */
	public function setCriticalTfL($criticalTfL)
	{
		$this->criticalTfL = $criticalTfL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalRhH()
	{
		return $this->criticalRhH;
	}

	/**
	 * @param mixed $criticalRhH
	 */
	public function setCriticalRhH($criticalRhH)
	{
		$this->criticalRhH = $criticalRhH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalRhL()
	{
		return $this->criticalRhL;
	}

	/**
	 * @param mixed $criticalRhL
	 */
	public function setCriticalRhL($criticalRhL)
	{
		$this->criticalRhL = $criticalRhL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalDpH()
	{
		return $this->criticalDpH;
	}

	/**
	 * @param mixed $criticalDpH
	 */
	public function setCriticalDpH($criticalDpH)
	{
		$this->criticalDpH = $criticalDpH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalDpDiff()
	{
		return $this->criticalDpDiff;
	}

	/**
	 * @param mixed $criticalDpDiff
	 */
	public function setCriticalDpDiff($criticalDpDiff)
	{
		$this->criticalDpDiff = $criticalDpDiff;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalWfH()
	{
		return $this->criticalWfH;
	}

	/**
	 * @param mixed $criticalWfH
	 */
	public function setCriticalWfH($criticalWfH)
	{
		$this->criticalWfH = $criticalWfH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalWfL()
	{
		return $this->criticalWfL;
	}

	/**
	 * @param mixed $criticalWfL
	 */
	public function setCriticalWfL($criticalWfL)
	{
		$this->criticalWfL = $criticalWfL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalWtH()
	{
		return $this->criticalWtH;
	}

	/**
	 * @param mixed $criticalWtH
	 */
	public function setCriticalWtH($criticalWtH)
	{
		$this->criticalWtH = $criticalWtH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalWtL()
	{
		return $this->criticalWtL;
	}

	/**
	 * @param mixed $criticalWtL
	 */
	public function setCriticalWtL($criticalWtL)
	{
		$this->criticalWtL = $criticalWtL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalStorageHeL()
	{
		return $this->criticalStorageHeL;
	}

	/**
	 * @param mixed $criticalStorageHeL
	 */
	public function setCriticalStorageHeL($criticalStorageHeL)
	{
		$this->criticalStorageHeL = $criticalStorageHeL;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalStorageVpH()
	{
		return $this->criticalStorageVpH;
	}

	/**
	 * @param mixed $criticalStorageVpH
	 */
	public function setCriticalStorageVpH($criticalStorageVpH)
	{
		$this->criticalStorageVpH = $criticalStorageVpH;
	}

	/**
	 * @return mixed
	 */
	public function getCriticalStorageVpL()
	{
		return $this->criticalStorageVpL;
	}

	/**
	 * @param mixed $criticalStorageVpL
	 */
	public function setCriticalStorageVpL($criticalStorageVpL)
	{
		$this->criticalStorageVpL = $criticalStorageVpL;
	}

	/**
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @param mixed $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}

	/**
	 * @return mixed
	 */
	public function getOffsetHe()
	{
		return $this->offsetHe;
	}

	/**
	 * @param mixed $offsetHe
	 */
	public function setOffsetHe($offsetHe)
	{
		$this->offsetHe = $offsetHe;
	}

	/**
	 * @return mixed
	 */
	public function getOffsetVp()
	{
		return $this->offsetVp;
	}

	/**
	 * @param mixed $offsetVp
	 */
	public function setOffsetVp($offsetVp)
	{
		$this->offsetVp = $offsetVp;
	}

	/**
	 * @return mixed
	 */
	public function getOffsetTc()
	{
		return $this->offsetTc;
	}

	/**
	 * @param mixed $offsetTc
	 */
	public function setOffsetTc($offsetTc)
	{
		$this->offsetTc = $offsetTc;
	}

	/**
	 * @return mixed
	 */
	public function getOffsetRh()
	{
		return $this->offsetRh;
	}

	/**
	 * @param mixed $offsetRh
	 */
	public function setOffsetRh($offsetRh)
	{
		$this->offsetRh = $offsetRh;
	}

	/**
	 * @return mixed
	 */
	public function getOffsetWf()
	{
		return $this->offsetWf;
	}

	/**
	 * @param mixed $offsetWf
	 */
	public function setOffsetWf($offsetWf)
	{
		$this->offsetWf = $offsetWf;
	}

	/**
	 * @return mixed
	 */
	public function getOffsetWtf()
	{
		return $this->offsetWtf;
	}

	/**
	 * @param mixed $offsetWtf
	 */
	public function setOffsetWtf($offsetWtf)
	{
		$this->offsetWtf = $offsetWtf;
	}

	/**
	 * @return mixed
	 */
	public function getOivOld()
	{
		return $this->oivOld;
	}

	/**
	 * @param mixed $oivOld
	 */
	public function setOivOld($oivOld): void
	{
		$this->oivOld = $oivOld;
	}



}