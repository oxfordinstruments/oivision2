<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 12:02 PM
 */

namespace AppBundle\Form;

use AppBundle\Entity\StatesEntity;
use AppBundle\Resources\UtilitiesCommon;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class UserNewEditForm extends AbstractType
{
	protected $reachableRoles;
	protected $roleChoices;
	private $transPrefix = 'security.roles.';
	/**
	 * @var UtilitiesCommon
	 */
	protected $utils;

	public function __construct(RoleHierarchy $roleHierarchy, AuthorizationChecker $authorizationChecker, UtilitiesCommon $utils)
	{
		$superAdminRole = 'ROLE_ADMIN';
		$currentPermissions = array(new Role($superAdminRole));
		$this->reachableRoles = $roleHierarchy->getReachableRoles($currentPermissions);
		$this->roleChoices = $this->getRoleChoices();
		if (!$authorizationChecker->isGranted($superAdminRole)) {
			unset($this->roleChoices[$this->transPrefix.$superAdminRole]);
		}
		$this->utils = $utils;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('uid', TextType::class, [
				'attr' => array('autocomplete' => 'email'),
				'label' => 'Username',
				'required' => true
			])

			->add('enabled', ChoiceType::class, [
				'label' => 'Enable User',
				'choices' => array('Yes' => true, 'No' => false),
//				'data' => 'Yes'
			])

			->add('registered', ChoiceType::class, [
				'label' => 'User Registered',
				'choices' => array('Yes' => true, 'No' => false),
//				'data' => 'Yes'
			]
			)
			->add('name', TextType::class, [
				'attr' => array('autocomplete' => 'name'),
				'label' => 'Full Name',
				'required' => true
			])

			->add('address', TextType::class, [

				'label' => 'Address',
				'required' => true
			])

			->add('city', TextType::class, [
				'attr' => array('autocomplete' => 'city'),
				'label' => 'City',
				'required' => true
			])

			->add('state', EntityType::class, [
				'class' => 'AppBundle\Entity\StatesEntity',
				'choice_label' => 'name',
				'required' =>true,
				'attr' => array(
					'autocomplete' => 'state',
					'class' => 'js-usersStateSelectFind'
				),
				'label' => 'State'
			])

			->add('zip',TextType::class, [
				'attr' => array('autocomplete' => 'postal-code'),
				'label' => 'Postal Code',
				'required' => true
			])

			->add('phone',TextType::class, [
				'attr' => array('autocomplete' => 'mobile'),
				'label' => 'Phone Number',
				'required' => true
			])

			->add('email', TextType::class, [
				'attr' => array('autocomplete' => 'email'),
				'label' => 'Email Address',
				'required' => true
			])

			->add('comment', TextareaType::class, [
				'attr' => array('autocomplete' => 'off', 'maxlength' => '255'),
				'label' => 'Comments',
				'required' => false
			])

			->add('roles', ChoiceType::class, [
				'choices' => $this->getRoleChoices(),
				'attr' => array('class' => 'js-usersRolesChoicesSelect'),
				'label' => 'Users Role',
				'required' => true,
				'expanded' => false,
				'multiple' => true
			])

			->add('customerId', EntityType::class, [
				'class' => 'AppBundle\Entity\CustomerEntity',
				'choice_label' => 'name',
				'required' =>false,
				'attr' => array(
					'class' => 'js-systemsCustomerSelectFind'
				),
				'label' => 'Customer associated with'
			])
			->add('timeZone', TimezoneType::class, [
				'required' =>true,
				'label' => "User's Timezone",
				'choice_loader' => null,
				'choices' => $this->utils->getTimeZonesList()
			])

		;

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\UserEntity',
		]);
	}

	private function getRoleChoices()
	{
		$roles = array();
		/** @var Role $role**/
		foreach ($this->reachableRoles as $role) {
			$roles[$this->transPrefix.$role->getRole()] = $role->getRole();
		}
		return $roles;
	}

}