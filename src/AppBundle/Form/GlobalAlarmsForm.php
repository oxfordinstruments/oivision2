<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 12:02 PM
 */

namespace AppBundle\Form;

use Doctrine\DBAL\Types\DecimalType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class GlobalAlarmsForm extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			//HE
			->add('alarm_he_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_he_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//HEB
			->add('critical_heb_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_heb_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//VP
			->add('critical_vp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_vp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_vp_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_vp_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//WF
			->add('critical_wf_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_wf_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_wf_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_wf_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//WT
			->add('critical_wt_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_wt_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_wt_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_wt_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//RH
			->add('critical_rh_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_rh_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_rh_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_rh_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//RT
			->add('critical_tf_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_tf_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_tf_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_tf_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//DP
			->add('critical_dp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_dp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_dp_diff', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_dp_diff', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//HE Storage
			->add('alarm_storage_he_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_storage_he_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.25,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//VP Storage
			->add('critical_storage_vp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_storage_vp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_storage_vp_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_storage_vp_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('uid', HiddenType::class, [])
		;

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\GlobalAlarmSettingsEntity',
		]);
	}


}