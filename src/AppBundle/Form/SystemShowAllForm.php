<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 12:02 PM
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SystemShowAllForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder

			->add('showAll', HiddenType::class, [
				'data' => 0
			])

			->add('showSubmit', SubmitType::class, [
				'attr' => array(
					'class' => 'btn btn-link'
				),
				'label' => 'Graph Year to date'
			])
		;

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
		]);
	}

}