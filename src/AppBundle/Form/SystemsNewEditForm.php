<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/21/2017
 * Time: 5:43 PM
 */

namespace AppBundle\Form;

use AppBundle\Entity\SystemsEntity;
use Doctrine\DBAL\Types\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SystemsNewEditForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('macRdc', TextType::class, [
				'attr' => array(
//					'class' => 'macFind'
				),
				'label' => 'RDC Mac Address',
				'required' => true
			])

			->add('enabled', ChoiceType::class, [
				'label' => 'Enable Monitoring',
				'choices' => array('Yes' => true, 'No' => false),
				'required' => true
			])

			->add('registered', ChoiceType::class, [
				'attr'=>[
					'class'=>'js-registeredFind',
					'style' => 'display: none;'
				],
				'label_attr' => [
					'style' => 'display: none;'
				] ,
				'choices' => array('Yes' => true, 'No' => false),
				'required' => false
			])

			->add('systemId', TextType::class, [
					'attr' => array(),
					'label' => 'System ID',
					'required' => true
				]
			)
			->add('systemName', TextType::class, [
				'attr' => array('autocomplete' => 'name'),
				'label' => 'System Nickname',
				'required' => true
			])

			->add('systemAddress', TextType::class, [

				'label' => 'Address',
				'required' => false
			])

			->add('systemCity', TextType::class, [
				'attr' => array('autocomplete' => 'city'),
				'label' => 'City',
				'required' => false
			])

			->add('systemState', EntityType::class, [
				'class' => 'AppBundle\Entity\StatesEntity',
				'choice_label' => 'name',
				'required' =>false,
				'attr' => array(
					'autocomplete' => 'state',
					'class' => 'js-systemsStateSelectFind'
				),
				'label' => 'State'
			])

			->add('systemZip',TextType::class, [
				'attr' => array('autocomplete' => 'postal-code'),
				'label' => 'Postal Code',
				'required' => false
			])

			->add('notes', TextareaType::class, [
				'attr' => array('autocomplete' => 'off', 'maxlength' => '255'),
				'label' => 'Comments',
				'required' => false
			])

			->add('customerId', EntityType::class, [
				'class' => 'AppBundle\Entity\CustomerEntity',
				'choice_label' => 'name',
				'required' =>false,
				'attr' => array(
					'class' => 'js-systemsCustomerSelectFind'
				),
				'label' => 'Customer'
			])

			->add('useGlobalAlarms', ChoiceType::class, [
				'label' => 'Use Global Alarms',
				'choices' => array('Yes' => true, 'No' => false),
				'attr' => [
					'class' => 'js-useGlobalAlarmsFind'
				],
				'required' => true
			])

			->add('inStorage', ChoiceType::class, [
				'label' => 'In Storage',
				'choices' => array('Yes' => true, 'No' => false),
				'required' => true
			])

			->add('monitorHe', CheckboxType::class, [
				'label' => 'Helium Level',
				'required' => false
			])
			->add('monitorHeb', CheckboxType::class, [
				'label' => 'Helium Burn',
				'required' => false
			])
			->add('monitorVp', CheckboxType::class, [
				'label' => 'Vessel Pressure',
				'required' => false
			])
			->add('monitorWf', CheckboxType::class, [
				'label' => 'Water Flow',
				'required' => false
			])
			->add('monitorWt', CheckboxType::class, [
				'label' => 'Water Temperature',
				'required' => false
			])
			->add('monitorRh', CheckboxType::class, [
				'label' => 'Room Humidity',
				'required' => false
			])
			->add('monitorTf', CheckboxType::class, [
				'label' => 'Room Temperature',
				'required' => false
			])
			->add('monitorDp', CheckboxType::class, [
				'label' => 'Room Dewpoint',
				'required' => false
			])
			->add('monitorDpDiff', CheckboxType::class, [
				'label' => 'Dewpoint Difference',
				'required' => false
			])
			->add('monitorCmp', CheckboxType::class, [
				'label' => 'Compressor',
				'required' => false
			])
			->add('monitorQ', CheckboxType::class, [
				'label' => 'Magnet Field',
				'required' => false
			])

			//HE
			->add('alarm_he_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_he_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('offset_he', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => -100.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Helium Percent',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('avgHe', CheckboxType::class, [
				'label' => 'Average Helium',
				'required' => false
			])
			//HEB
			->add('critical_heb_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_heb_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//VP
			->add('critical_vp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_vp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_vp_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_vp_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('offset_vp', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => -100.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Vessel Pressure',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('avgVp', CheckboxType::class, [
				'label' => 'Average Pressure',
				'required' => false
			])
			//WF
			->add('critical_wf_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_wf_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_wf_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_wf_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('offset_wf', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => -100.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Water Flow',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//WT
			->add('critical_wt_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_wt_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_wt_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_wt_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('offset_wtf', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => -100.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Water Temp (Deg F)',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//RH
			->add('critical_rh_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_rh_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_rh_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_rh_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('offset_rh', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => -100.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Room Humidity',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//DP
			->add('critical_dp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_dp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//DPDIFF
			->add('critical_dp_diff', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_dp_diff', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//RT
			->add('critical_tf_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_tf_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_tf_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_tf_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('offset_tc', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => -100.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Room Temp (Deg C)',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//HE Storage
			->add('alarm_storage_he_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_storage_he_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.25,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			//VP Storage
			->add('critical_storage_vp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_storage_vp_h', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm High',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('alarm_storage_vp_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Alarm Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('critical_storage_vp_l', Types\TelType::class, [
				'attr'=>[
					'step' => 0.1,
					'min' => 0.0,
					'max' => 100.0,
					'class' => 'number-input'
				],
				'label' => 'Critical Low',
				'label_attr' => [
					'class' => 'requiredIgnore'
				]
			])
			->add('clearData', TextType::class, [
					'attr' => array(
						'class' => 'js-clearDataFind'
					),
					'mapped' => false,
					'label' => 'Clear Data',
					'required' => false,
					'data' => "Type 'YES' here to clear all data!"
				]
			)
		;

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\SystemsEntity',
		]);
	}
}