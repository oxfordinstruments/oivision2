<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/7/2017
 * Time: 11:33 AM
 */

namespace AppBundle\Form;


use AppBundle\Resources\UtilitiesCommon;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\StatesEntity;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class UserChangeInfoForm extends AbstractType
{
	/**
	 * @var UtilitiesCommon
	 */
	protected $utils;

	public function __construct(UtilitiesCommon $utils)
	{
		$this->utils = $utils;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		//dump($options['data']);

		$builder
			->add('name', TextType::class, [
				'attr' => array('autocomplete' => 'name'),
				'label' => 'Full Name',
				'required' => true
			])

			->add('address', TextType::class, [

				'label' => 'Address',
				'required' => true
			])

			->add('city', TextType::class, [
				'attr' => array('autocomplete' => 'city'),
				'label' => 'City',
				'required' => true
			])

			->add('state', EntityType::class, [
				'class' => 'AppBundle\Entity\StatesEntity',
				'choice_label' => 'name',
				'required' =>true,
				'attr' => array(
					'autocomplete' => 'state',
					'class' => 'js-usersStateSelectFind'
				),
				'label' => 'State'
			])

			->add('zip',TextType::class, [
				'attr' => array('autocomplete' => 'postal-code'),
				'label' => 'Postal Code',
				'required' => true
			])

			->add('phone',TextType::class, [
				'attr' => array('autocomplete' => 'mobile'),
				'label' => 'Phone Number',
				'required' => true
			])

			->add('email', TextType::class, [
				'attr' => array('autocomplete' => 'email'),
				'label' => 'Email Address (Will not change your login username)',
				'required' => true
			])
			->add('timeZone', TimezoneType::class, [
				'required' =>true,
				'label' => "User's Timezone",
				'choice_loader' => null,
				'choices' => $this->utils->getTimeZonesList()
			])
			->add('updateInfo' , SubmitType::class)

		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\UserEntity'
		]);
	}



}