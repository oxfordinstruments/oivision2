<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 8/31/2017
 * Time: 12:02 PM
 */

namespace AppBundle\Form;

use AppBundle\Entity\StatesEntity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class CustomerNewEditForm extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder

			->add('enabled', ChoiceType::class, [
				'label' => 'Enable Customer',
				'choices' => array('Yes' => true, 'No' => false),
//				'data' => 'Yes'
			])

			->add('name', TextType::class, [
				'attr' => array('autocomplete' => 'name'),
				'label' => 'Customer Name',
				'required' => true
			])

			->add('address', TextType::class, [

				'label' => 'Address',
				'required' => true
			])

			->add('city', TextType::class, [
				'attr' => array('autocomplete' => 'city'),
				'label' => 'City',
				'required' => true
			])

			->add('state', EntityType::class, [
				'class' => 'AppBundle\Entity\StatesEntity',
				'choice_label' => 'name',
				'required' =>true,
				'attr' => array(
					'autocomplete' => 'state',
					'class' => 'js-customersStateSelectFind'
				),
				'label' => 'State'
			])

			->add('zip',TextType::class, [
				'attr' => array('autocomplete' => 'postal-code'),
				'label' => 'Postal Code',
				'required' => false
			])

			->add('phone',TextType::class, [
				'attr' => array('autocomplete' => 'mobile'),
				'label' => 'Phone Number',
				'required' => false
			])

			->add('email', TextType::class, [
				'attr' => array('autocomplete' => 'email'),
				'label' => 'Email Address',
				'required' => false
			])

			->add('comment', TextareaType::class, [
				'attr' => array('autocomplete' => 'off', 'maxlength' => '255'),
				'label' => 'Comments',
				'required' => false
			])



		;

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => 'AppBundle\Entity\CustomerEntity',
		]);
	}


}