<?php

namespace AppBundle\Form;

use AppBundle\Entity\SystemsEntity;
use AppBundle\Entity\UserEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Button;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserPreferencesSystemsForm extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event){
			$form = $event->getForm();
			$data = $event->getData();
			/**
			 * @var SystemsEntity $sys
			 */
			$systems = [];
			foreach ($data['systems'] as $sys){
				$systems[$sys->getSystemId().' - '.$sys->getSystemName()] = $sys->getId();
			}

			$form->add('systems', ChoiceType::class, [
				'label' => false,
				'required' => false,
				'choices' => $systems,
				'multiple' => true,
				'placeholder' => false,
				'expanded' => true,
				'data' => $data['user']->getMySystems(),
				'attr' => ["class" => "systemCheckBoxes"],

			]);


		});
		$builder->add('saveSystems', SubmitType::class);
		$builder->add('checkuncheck', ButtonType::class, [
			'label' => 'UnCheck All',
			'attr' => ['class' => 'checkuncheck']
		]);
	}

	public function configureOptions(OptionsResolver $resolver)
	{

	}

	public function getBlockPrefix()
	{
		return 'app_bundle_user_preferences_systems_form';
	}
}
