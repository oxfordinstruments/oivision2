<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\StatesEntity;
use AppBundle\Entity\TestEntity;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Nelmio\Alice\Fixtures;
use Nelmio\Alice\Fixtures\Fixture;

class LoadFixtures implements FixtureInterface
{
	private  $om;

	public function load(ObjectManager $manager)
	{
		$this->om = $manager;

		Fixtures::load(
			__DIR__.'/fixtures.yml',
			$manager,
			[
				'providers' =>[$this]
			]);
	}

	public function systems()
	{
		$systems = [
			'CT/i',
			'Lightspeed 16',
			'11.x',
			'Pace Plus',
			'Symfony',
			'NX/i',
			'Lightspeed QX/i'
		];
		$key = array_rand($systems);
		return $systems[$key];
	}


}