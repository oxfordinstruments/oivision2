<?php

namespace AppBundle\Twig;


use AppBundle\Entity\SystemsDataGeEntity;

class SortSystemData extends \Twig_Extension
{

	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		return 'data_sort';
	}

	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('sysdatasort', array($this, 'customSort'))
		);
	}

	/**
	 * @param $array
	 * @return array The Sorted array
	 */
	public function customSort($array)
	{
		usort($array, array($this, 'sortFunct'));
		return $array;
	}

	/**
	 * @param SystemsDataGeEntity $a
	 * @param SystemsDataGeEntity $b
	 * @return int
	 */
	private function sortFunct($a, $b)
	{
		return $a->getTimestamp() < $b->getTimestamp();
	}

}