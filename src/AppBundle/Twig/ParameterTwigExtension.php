<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 11/16/2017
 * Time: 10:54 AM
 */

namespace AppBundle\Twig;


use Symfony\Component\DependencyInjection\Container;
use Twig_Extension;
use Twig_SimpleFunction;

class ParameterTwigExtension extends Twig_Extension
{
	/**
	 * @var Container
	 */
	public $container;

	public function __construct(Container $container)
	{
		$this->container = $container;
	}


	public function getFunctions()
	{
		return [
			new Twig_SimpleFunction('parameter', function($name)
			{
				return $this->container->getParameter($name);
			})
		];
	}


	/**
	 * Returns the name of the extension.
	 *
	 * @return string The extension name
	 */
	public function getName()
	{
		return 'oi';
	}
}