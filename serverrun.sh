#!/usr/bin/env bash

if [ "$SYMFONY_ENV" = "prod" ]; then
php bin/console cache:clear --env=prod
fi

php bin/console server:run localhost:8000