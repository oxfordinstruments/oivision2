<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 9/18/2017
 * Time: 12:01 PM
 */

require __DIR__.'/vendor/autoload.php';

use GuzzleHttp\Client;

$url = 'http://192.168.14.102:8000/api/hello';


$client = new Client([
	'http_errors' => false,
	'headers' => [
		'content-type' => 'application/json',
		'api-key' => 'asdf'
	]
]);

//$res = $client->request('POST', $url, [
//	'json' => ['foo' => 'bar']
//]);

$res = $client->post($url,[
	'json' => [
		'mac' => '5c864a23ccfa',
		'date' => '2017-09-18 00:32:00',
		'tc' => '12.455'
	]
]);


echo $res->getStatusCode(),PHP_EOL;
echo $res->getHeader('Content-Type')[0],PHP_EOL;
echo $res->getBody(),PHP_EOL;
//echo print_r($res->getHeaders()),PHP_EOL;