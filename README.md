OiVision 2.0
=========

*A Symfony 3 project created by Evotodi.*

###To-do List:
+ Create cron jobs for each of the timezones in utils to send alarms today email
+ Dashboard list systems that haven't reported in a day.
+ Send email to admin/managers when user regs. 
+ System data page slow to load.
+ Reset password mostly works. If you fail validation then not fail the password will not be changed.




###Production Tasks:
~~~~
composer install
composer update
npm install
npm install -g gulp
gulp
bin/console doctrine:schema:create
bin/console seed:load
bin/console app:initialImportData
bin/console cache:clear --env=prod
set-perms.sh
~~~~

~~~~
clear the prod env cache
~~~~




###Api Docs:


Headers must contain the following 
~~~~
Content-Type: application/json
api-key: <your api key token>
~~~~
<br>


GET /api/datetime

~~~~
Returns: JSON
{"date":"2017-10-12 13:53:25.125034","timezone_type":3,"timezone":"UTC"}
~~~~

POST /api/hello
~~~~
Parameters: 
mac
ver

{"mac":"5c-86-4a-23-cc-2e","ver":"2.0"}

Returns: JSON
{code":202,"error":false,"datetime":"2017-10-12T13:54:00+0000","name":"NOT REGISTERED 9000","sysid":"MR9000","enabled":false,"freq":60}

Return Codes:
202 = Success
400 = Json error
401 = Unauthorized
405 = Data invalid
~~~~

POST /api/data/ge
~~~~
Parameters:
mac
mfg
data[]
data.tf
data.rh
data.wf
data.wt
data.he
data.vp
data.q
data.cmp

{"mac":"5c-86-4a-00-00-00","mfg":"Ge","data":{
"tf":71.5,
"rh":75.2,
"he":58.67,
"vp":4.03,
"wf":12.44,
"wt": 58.65,
"q":0,
"cmp":1
}
}

Returns: JSON
{"code":202,"error":false,"datetime":"2017-10-12T13:57:56+0000","name":"High Field Shelbys","sysid":"MR2037","enabled":true,"alarm":false,"critical":true,"freq":60}

Return Codes:
202 = Success
400 = Json error
401 = Unauthorized
403 = Not enabled
405 = Data invalid
~~~~


<br>
<br>

###Thanks To:
[KNP University](https://knpuniversity.com/)